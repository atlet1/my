<?php
require_once('classes/somedemo.php');
use PHPUnit\Framework\TestCase;

class DemoTest extends TestCase {
	
  public function setUp() {
    $this->demo = new SomeDemo();
  }

  public function testDiv() {
    $this->assertEquals(1, $this->demo->div(2,2));
  }

  public function testMult() {
    $this->assertEquals(4, $this->demo->mult(2,2));
  }

  public function tearDown() {
    unset($this->demo);
  }
}
?>
