<?php
use PHPUnit\Framework\TestSuite;
require_once 'tests/demotest-2.php';
require_once 'tests/demotest-3.php';

class AllTests {
	public static function suite() {
		$suite = new TestSuite('Project');
		$suite->addTestSuite('DemoTest');
		$suite->addTestSuite('SomeDemoTest');
		return $suite;
	}
}