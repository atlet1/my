<?php
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class DatabaseTest extends TestCase{
		use TestCaseTrait;
    /**
     * @return PHPUnit\DbUnit\Database\Connection
     */
    protected function getConnection(){
        $pdo = new PDO('sqlite:E:/Serv/home/my.loc/www/demo/4/phpunit/test1.db');
        return $this->createDefaultDBConnection($pdo, 'test1.db');
    }
		/**
     * @return PHPUnit\DbUnit\DataSet\IDataSet
     */
    protected function getDataSet(){
        return $this->createFlatXMLDataSet(__DIR__.'\xml\flatXml.xml');
    }
	public function testDB(){
		$this->getConnection();
		$this->getDataSet();
	}
}
?>