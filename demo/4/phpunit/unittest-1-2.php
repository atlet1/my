<?php
require_once('classes/array-test.php');
use PHPUnit\Framework\TestCase;
// Использование разных методов
class StackTest extends TestCase{

  public function testEmpty(){
    $arr = [];
    $this->assertTrue(empty($arr));
  }

  public function testPush(){
    $arr = [];
    array_push($arr, 'foo');
    $this->assertEquals('foo', $arr[count($arr)-1]);
    $this->assertFalse(empty($arr));
  }

  public function testPop(array $arr){
    $arr = [];
    $this->assertEquals('foo', array_pop($arr));
    $this->assertTrue(empty($arr));
  }
}
?>