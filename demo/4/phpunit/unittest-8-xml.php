<?php
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class DatabaseTest extends TestCase{
	use TestCaseTrait;
    protected function getConnection(){
        $pdo = new PDO('sqlite:E:/Serv/home/my.loc/www/demo/4/phpunit/test1.db');
        return $this->createDefaultDBConnection($pdo, 'test1.db');
    }

    protected function getDataSet(){
        return $this->createXMLDataSet(__DIR__.'\xml\xml.xml');
    }
	public function testDB(){
		$this->getConnection();
		$this->getDataSet();
	}
}
?>