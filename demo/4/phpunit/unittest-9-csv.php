<?php
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\CsvDataSet;

class DatabaseTest extends TestCase{
	use TestCaseTrait;
    protected function getConnection(){
     $pdo = new PDO('sqlite:E:/Serv/home/my.loc/www/demo/4/phpunit/test1.db');
       return $this->createDefaultDBConnection($pdo, 'test1.db');
    }

    protected function getDataSet(){
    $dataSet = new CsvDataSet();
    $dataSet->addTable('post', 'E:/Serv/home/my.loc/www/demo/4/phpunit/csv/post.csv');
    $dataSet->addTable('post_comment', 'E:/Serv/home/my.loc/www/demo/4/phpunit/csv/post_comment.csv');
    $dataSet->addTable('current_visitors', 'E:/Serv/home/my.loc/www/demo/4/phpunit/csv/current_visitors.csv');
        return $dataSet;
    }
	public function testDB(){
		$this->getConnection();
		$this->getDataSet();
	}
}
?> 