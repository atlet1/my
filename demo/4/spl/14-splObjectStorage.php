<?php
$start = memory_get_usage();
//$array = range(1, 100000)
$array = new SplFixedArray(100000);
for($i=0; $i<100000; ++$i)
	$array[$i] = $i;
echo memory_get_usage() - $start, ' bytes';




class Cource{
	private $_name;
	function __construct($n){$this->_name = $n;}
	function __toString(){return $this->_name;}
}
$courses = new SplObjectStorage();
$php = new Cource('php');
$xml = new Cource('xml');
$java = new Cource('java');

$courses->attach($php);
var_dump($courses->contains($php));//true
var_dump($courses->contains($java));//false
$courses->attach($java);
var_dump($courses->contains($java));//true
$courses->attach($xml);
$courses->detach($java);
var_dump($courses->contains($xml));//true

$titles = [];
foreach($courses as $course)
	$titles [] = (string)$course;

print join(', ', $titles);
exit;
$os = new SplObjectStorage();

$person = new stdClass();// Стандартный объект
$person->name = "John";
$person->age = "25";

$os->attach($person); //Добавляем объект в storage

foreach ($os as $object){
	print_r($object);
	echo "<br>";
}

$person->name = "Mike"; //Меняем имя
echo str_repeat("-",30)."<br>"; //Просто линия

foreach ($os as $object){
	print_r($object);
	echo "<br>";
}

$person2 = new stdClass();
$person2->name = "Vasya";
$person2->age = "18";

$os->attach($person2);

echo str_repeat("-",30)."<br>";

foreach ($os as $object){
	print_r($object);
	echo "<br>";
}

if($os->contains($person))
	echo "У нас имеется объект person";
else
	echo "У нас нет объекта person";

$os->rewind();
echo "<br>" . $os->current()->name;

$os->detach($person); //Удаляем объект из коллекции

echo "<br>".str_repeat("-",30)."<br>";
foreach ($os as $object){
	print_r($object);
	echo "<br>";
}
?>
<?php
/*
foreach(get_class_methods(SplObjectStorage) as $key=>$method){
	echo $key.' -> '.$method.'<br />';
}
*/
?>