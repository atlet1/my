<?php
/*$logdb = function ($str){
				Logger::log('debug', 'db', $str);
			};
						

$db = SQLite3('my.db');
$logdb('Connect to db');
$db->query('INSERT INTO...');
$logdb('Insert val1');
$db->query('INSERT INTO...');
$logdb('Insert val2');
$db->query('INSERT INTO...');
$logdb('Insert val3');*/

function setup($x){
	$i = 0;
  return function() use($x, &$i){
  	if(isset($x[$i]))
			return $x[$i++];
	};
}
$next = setup(['a', 'b', 'c']);
print($next());
print($next());
print($next());
echo '<br>';

// Обращение к функции через переменную
function Hello($name){
	echo "Hello, $name!";
}

$func = "Hello";
$func('John');
echo '<br>';

//Анонимная функции
$func = function($name){
					echo "Привет, $name\n";
				};
$func('Мир!');
echo '<br>';

//Использование
$arr = [1, 2, 3, 4, 5];
function foo($v){
	return $v * 10;
}
$new_arr = array_map('foo', $arr);
//$new_arr = array_map(create_function('$v', 'return $v * 10;'), $arr); колись так робили (deprecated)

//Самый удобный вариант
$new_arr = array_map(function($v){
											return $v * 10;
										},	$arr);
										
//Closure (замыкание)
$string = "Hello world!";
$closure = function() use ($string){
echo $string;
};
$closure();
echo '<br>';

// Переопределение значения внешней переменной
$x = 1;
$closure = function() use (&$x){
++$x;
};
echo $x;//1
echo '<br>';
$closure();
echo $x;//2	
echo '<br>';

$str = 'Hello';
$closure = function() use(&$str) {echo $str;};
$str = 'Bye';
$closure();
echo '<br>';

// Использование
$add = function ($v){
					return $v + 2;
				};
echo $add(2);//4
echo $add(3);//5
echo '<br>';

// Использование 2
$mult = function ($num){
				return function($x) use($num){
										return $x * $num;
									};
					
				};	
$mult_2 = $mult(2);//function($x) use($num){return $x * 2;};
$mult_3 = $mult(3);//function($x) use($num){return $x * 3;};
echo $mult_2(2);//4
echo '<br>';
echo $mult_2(5);//10
echo '<br>';
echo $mult_3(2);//6
echo '<br>';
echo $mult_3(5);//15
echo '<br>';

// Использование в классах
class User{
private $_name;
public function __construct($n){ $this->_name = $n;}
public function greet($greeting){
return function() use ($greeting) {
return "$greeting {$this->_name}!";//echo "$word {$this->_name}!";
};
}
}
$user = new User("John");
$en = $user->greet("Hello");//$user->sayHallo('Hello');
echo $en();
echo '<br>';
$ru = $user->greet("Привет");//$user->sayHallo('Привіт');
echo $ru();
?>