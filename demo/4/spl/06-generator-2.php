<?php
function numbers() {
echo "START\n";
for ($i = 0; $i < 5; ++$i) {
yield $i;
}
echo "FINISH\n"; 
}
​
foreach (numbers() as $value){
echo "VALUE: $value\n";
}
​
// Возврат ключей?
function gen() {
yield 'a';
yield 'b';
yield 'name' => 'John';
yield 'd';
yield 10 => 'e';
yield 'f';
}
foreach (gen() as $key => $value)
echo "$key : $value\n";
​
​// Co-routines: принимаем значение!
function echoLogger() {
while (true) {
echo 'Log: ' . yield . "<br>";
}
}
$logger = echoLogger();
$logger->send('Foo');
$logger->send('Bar');
​
// Комбинируем возврат и приём значений
function numbers() {
$i = 0;
while (true) {
$cmd = (yield $i);
++$i;
if ($cmd == 'stop')
return; // Выход из цикла
}
}
$gen = numbers();
foreach ($gen as $v) {
if ($v == 3)
$gen->send('stop');
echo $v;
}
?>