<?php
// Создание базы SQLite
$db = new Sqlite3("test1.db");
// Создание таблицы
$sql = "CREATE TABLE post(
						post_id INTEGER,
						title TEXT,
						date_created TEXT,
						contents TEXT,
						rating INTEGER
						)";
$db->exec($sql) or die ($db->LastErrorMsg());
$sql = "CREATE TABLE post_comment(
						post_comment_id INTEGER,
						post_id INTEGER,
						author TEXT,
						content TEXT,
						url TEXT
						)";
$db->exec($sql) or die ($db->LastErrorMsg());
$sql = "CREATE TABLE current_visitors(
						current_visitors_id INTEGER,
						ip TEXT
						)";
$db->exec($sql) or die ($db->LastErrorMsg());
$db->close();
?>