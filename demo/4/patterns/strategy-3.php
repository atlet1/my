<?php
function foo($a, $b){
	if($a == $b) return 0;
	return $a<$b ? -1 : 1;
}
$a = [3, 2, 5, 6, 1];
usort($a, "foo");

interface Strategy{
	public function doOperation($n1, $n2);
}
class OpAdd implements Strategy{
	public function doOperation($n1, $n2){
		return $n1 + $n2;
	}
}	
class Sub implements Strategy{
	public function doOperation($n1, $n2){
		return $n1 - $n2;
	}
}	
class Mult implements Strategy{
	public function doOperation($n1, $n2){
		return $n1 * $n2;
	}
}
class Context {
	private $s;
	function __construct($op) {
		switch($op){
			case "+": $this->s = new OpAdd(); break;
			case "-": $this->s = new Sub(); break;
			case "*": $this->s = new Mult(); break;
			default: throw new Exception("Wrong type!");
		}
	}
	function execute($n1, $n2){
		return $this->s->doOperation($n1, $n2);
	}
}
$c = new Context("+");
echo($c->execute(2,3))."\n";
?>