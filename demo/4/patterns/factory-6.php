<?php
//method
interface Shape{
	public function draw();
}
class Rectangle implements Shape{
	public function draw(){
		echo(__METHOD__."\n");
	}
}	
	class Square implements Shape{
	public function draw(){
		echo(__METHOD__."\n");
	}
}	
	class Circle implements Shape{
	public function draw(){
		echo(__METHOD__."\n");
}
} 
///////////////////////////////////
class ShapeFactory {
	function getShape($type) {
		$type = strtoupper($type);
		switch($type){
			case "R": return new Rectangle();
			case "S": return new Square();
			case "C": return new Circle();
			default: throw new Exception("Wrong type!");
		}
	}
}
//////////////////////////////////
$f = new ShapeFactory();
$r = $f->getShape("r");
$s = $f->getShape("s");
$c = $f->getShape("c");
$r->draw();
$s->draw();
$c->draw();
?>