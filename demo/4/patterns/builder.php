<?php
class Window {
	//public dialog = FALSE;
	//public modal = FALSE;
	//public visible = FALSE;
	
		function __construct($d, $m, $v) {
		$this->dialog = $d;
		$this->modal = $m;
		$this->visible = $v;
	}
}
//$w = new Window(TRUE, FALSE, TRUE);
/////////////////////////////////
class CreateWindow {
	function setDialog($flag=FALSE) {
		$this->dialog = $flag;
		return $this;
	}
	function setModal($flag=FALSE) {
		$this->modal = $flag;
		return $this;
	}
	function setVisible($flag=FALSE) {
		$this->visible = $flag;
		return $this;
	}
	function create(){
		return new Window($this->dialog, $this->modal, $this->visible);
	}
}
$c = new CreateWindow();
$w = $c->setVisible(TRUE)->setDialog(TRUE)->create();

?>