<?php
interface Shape{
	public function draw();
}
class Rectangle implements Shape{
	public function draw(){
		echo(__METHOD__."\n");
	}
}	
	class Square implements Shape{
	public function draw(){
		print(__METHOD__."\n");
	}
}	
	class Circle implements Shape{
	public function draw(){
		print(__METHOD__."\n");
}
} 
///////////////////////////////////
abstract class ShapeDecorator implements Shape {
	protected $decoratedShape;
	function __construct(Shape $decoratedShape){
		$this->decoratedShape = $decoratedShape;
	}
	public function draw(){
		$this->decoratedShape->draw();
	}
}
class RedShapeDecorator extends ShapeDecorator {
	function __construct(Shape $decoratedShape) {
		parent::__construct($decoratedShape);
	}
	private function SetRedTopBorder(){
		print("TOP BORDER COLOR RED \n");
	}
	private function SetRedBottomBorder(){
		print("BOTTOM BORDER COLOR RED\n");
	}
	public function draw(){
		$this->SetRedTopBorder();
		$this->decoratedShape->draw();
		$this->SetRedBottomBorder();
	}
}

//////////////////////////////////
$c = new Circle();
$rc = new RedShapeDecorator(new Circle);
$c->draw();
$rc->draw();
?>