<?php
class Users{

  public $id;
  public $email;
  public $name;

  public function nameToUpper(){
    return strtoupper($this->name);
  }
}


  $db = new PDO("sqlite:users.db");

  $sql = "SELECT * FROM user";

  $stmt = $db->query($sql);
	
	//Полная выборка fetchAll
	//$sql = "SELECT id, name, email FROM users";
	//$stmt = $db->query($sql);
	// Получаем массив массивов
//$arr = $stmt->fetchAll(PDO::FETCH_ASSOC); //по-умолчанию PDO::FETCH_BOTH
 ​
	// Выбираем данные только из первого поля
	//$arr = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
	
	// Группируем значения второго поля по значению первого поля
	//$arr = $stmt->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
	
	// Выбираем уникальные значения из первого поля
	//$arr = $stmt->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_UNIQUE); 
	​
	// Используем функцию обратного вызова
	//function foo($name, $email){
	//return $name . ': '. $email . "\n");}
	//$arr = $stmt->fetchAll(PDO::FETCH_FUNC, 'foo');

	// Получаем массив объектов класса User
	$obj = $stmt->fetchALL(PDO::FETCH_CLASS, 'Users');

  foreach($obj as $user){
    echo $user->nameToUpper().'<br>';
  }
  $db = null;
