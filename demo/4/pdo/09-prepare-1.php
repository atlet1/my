<?php
try {
  $db = new PDO("sqlite:users.db");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $id = 5;

  $stmt = $db->prepare("SELECT * FROM user WHERE id = :id");

  $stmt->bindParam(':id', $id, PDO::PARAM_INT);

  $stmt->execute();
	
	//$sql = 'SELECT email FROM users WHERE id = :id AND name = :name';
	//$stmt = $pdo->prepare($sql);
	//$stmt->execute([':id' => 5, ':name' => 'John']);
  //$result = $stmt->fetchAll();

  while($row = $stmt->fetch()){
    echo $row['id'].'<br>';
    echo $row['name'].'<br>';
    echo $row['email'];
  }

  $db = null;
}catch(PDOException $e){
  echo $e->getMessage();
}

?>