<?php
//Использование курсора
 $stmt = $db -> prepare($sql,
array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
$stmt->execute();
while ($row = $stmt ->
fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT))
$row = $stmt ->
fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_LAST);
do {
} while ($row = $stmt ->
fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_PRIOR));

//Константы курсора
/*
PDO::CURSOR_FWDONLY

PDO::CURSOR_SCROLL
	 PDO::FETCH_ORI_NEXT
	 PDO::FETCH_ORI_PRIOR
	 PDO::FETCH_ORI_FIRST
	 PDO::FETCH_ORI_LAST
	 PDO::FETCH_ORI_ABS
	 PDO::FETCH_ORI_REL
*/
