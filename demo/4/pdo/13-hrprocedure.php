<?php
//Хранимые процедуры
$db = new PDO('sqlite:users.db');

$id = 5; 
$name = 'John';
​
$stmt = $db->prepare('CALL getEmail(?, ?, ?)');
​
// Параметр IN
$stmt->bindParam(1, $id, PDO::PARAM_INT);
// Параметр INOUT
$stmt->bindParam(2, $name, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT);
// Параметр OUT
$stmt->bindParam(3, $email, PDO::PARAM_STR);
​
$stmt->execute();

//Хранимые процедуры MySQL
/*
$id = 5;
//Вариант 1
$db->query('CALL getEmail($id, @name, @email)');
$result = $db -> query('SELECT @name, @email');
//Вариант 2
$stmt = $db->prepare('CALL getEmail(:id, @name, @email)');
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
$stmt->execute();
$result = $db->query('SELECT @name, @email');
$arr = $result->fetch();
echo $arr[@name] . ':' . $arr[@email];
*/