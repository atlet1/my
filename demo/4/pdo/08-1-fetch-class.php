<?php
class User{

  public $id;
  public $email;
  public $name;

	/*
	public function __construct(){
		$this->id = 0;
		$this->email = "guest@gmail.com";
		$this->name = "Guest";
	}
	*/
  public function nameToUpper(){
    return strtoupper($this->name);
  }
}


  $db = new PDO("sqlite:users.db");

  $sql = "SELECT * FROM user";

  $stmt = $db->query($sql);
  
  //$obj = $stmt->fetchObject("User");//конструктор перезаписує дані
  /*
  $stmt->fetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "User");
  $obj = $stmt->fetch(PDO::FETCH_ASSOC, "User");
  */
  $obj = $stmt->fetch(PDO::FETCH_CLASS|PDO::FETCH_CLASSTYPE);

  echo $obj->id.'<br>';
  //echo $obj->nameToUpper().'<br>';//при розкометуванні верхнього, це закоментувати
  echo $obj->email.'<br>';

  $db = null;

/*
//Табл. users c полями: id, name, email 1, First, first@email.ru 2, Second, second@email.ru 
// Поиск названия класса в значении первого поля в выборке
​$sql = "SELECT id, name, email FROM users";
$stmt = $pdo->query($sql);
$obj = $stmt->fetch( PDO::FETCH_CLASS|PDO::FETCH_CLASSTYPE );//stdClass Object([name]=>First[email]=>first@email.ru) second@email.ru
​
class First {
public $id, $name, $email;
}
$sql = "SELECT name, email FROM users";
$stmt = $pdo->query($sql);
​$obj = $stmt->fetch( PDO::FETCH_CLASS|PDO::FETCH_CLASSTYPE );
//First Object([id]=>NULL[name]=>NULL[email]=>first@email.ru)stdClass Object([email]=>second@email.ru)
​
class Second {
public $id, $name, $email;
}
$sql = "SELECT name, email, id FROM users";
$stmt = $pdo->query($sql);
​$obj = $stmt->fetch( PDO::FETCH_CLASS|PDO::FETCH_CLASSTYPE );//First Object([id]=>1[name]=>NULL[email]=>first@email.ru )Second Object([id]=>2[name]=>NULL[email]=>second@email.ru)
​
// Явное указание названия класса для создания объекта
// По-умолчанию stdClass
$sql = "SELECT id, name, email FROM users";
$stmt = $pdo->query($sql);
​$obj = $stmt->fetchObject();//stdClass Object([id]=>1[name]=>First[email]=>first@email.ru)stdClass Object([id]=>2[name]=>Second[email]=>second@email.ru)
​
// Явное указание названия класса User для создания объекта​
class User{
public id, name, email; 
}
$sql = "SELECT id, name, email FROM users";
$stmt = $pdo->query($sql);
​$obj = $stmt->fetchObject("User");
//User Object([id]=>1[name]=>First[email]=>first@email.ru)User Object([id]=>2[name]=> Second[email]=>second@email.ru)
// Установка режимов выборки
// Явное указание имеющегося объекта
$user = new User();
$stmt->setFetchMode(PDO::FETCH_INTO, $user);
$obj = $stmt->fetch(PDO::FETCH_ASSOC);
//Результат используется один и тот же объект! 
//После извлечения последней записи в объекте будет: User Object([id]=>2[name]=>Second[email]=>second@email.ru)

// Явное указание класса User для создания объекта
$stmt -> setFetchMode(PDO::FETCH_CLASS, "User");
// Явное указание класса User для создания объекта
// Cвойства заполняются значениями после отработки конструктора
$stmt -> setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "User");
*/