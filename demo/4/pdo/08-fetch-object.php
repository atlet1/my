<?php
//Результат в виде объекта
$db = new PDO("sqlite:users.db");

$sql = "SELECT * FROM user";

$stmt = $db->query($sql);

$result = $stmt->fetch(PDO::FETCH_OBJ);

echo $result->id.'<br>';
echo $result->name.'<br>';
echo $result->email.'<br>';

// Ленивое приведение
//$result = $stmt->fetch(PDO::FETCH_LAZY);
​
//echo $result[0] . "\n";
//echo $result['name'] . "\n";
//echo $result->email . "\n";