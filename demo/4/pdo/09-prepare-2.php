<?php
try {
  $db = new PDO("sqlite:users.db");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $id = 6;

  $stmt = $db->prepare("SELECT * FROM user WHERE id = ?");

  $stmt->bindParam(1, $id, PDO::PARAM_INT);
	
  //$stmt->bindParam(2, $name, PDO::PARAM_STR);
	//$stmt->bindValue(':name', $name, PDO::PARAM_STR);

// Привязка возвращаемых полей к именам переменных 
/*
$sql = 'SELECT id, name, email FROM users';
$stmt = $db->prepare($sql);
$stmt->execute();
​
$stmt->bindColumn(1, $id);
$stmt->bindColumn(2, $name);
$stmt->bindColumn('email', $email); 
// имя поля регистрозависимо!
​while($stmt->fetch(PDO::FETCH_BOUND))
echo "$id : $name : $email\n";
*/

  $stmt->execute();
	
  //$result = $stmt->fetchAll();
  while($row = $stmt->fetch()){
    echo $row['id'].'<br>';
    echo $row['name'].'<br>';
    echo $row['email'];
  }

  $db = null;
}catch(PDOException $e){
  echo $e->getMessage();
}
