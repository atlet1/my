<?php
set_include_path(get_include_path()
					.PATH_SEPARATOR.'application/controllers'
					.PATH_SEPARATOR.'application/models'
					.PATH_SEPARATOR.'application/views');

/* Автозагрузчик классов */
function loadClass ($class_name) {
require_once($class_name.'.php');
}
spl_autoload_register('loadClass');

//spl_autoload_register(function ($class_name) {
 //   include $class_name . '.php';
//});

$front = FrontController::getInstance();
$front->route();
echo $front->getBody();