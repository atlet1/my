<HTML>
<HEAD>
<TITLE>in_array</TITLE>
</HEAD>
<BODY>
<h2>in_array Проверяет, присутствует ли в массиве значение</h2>
<?
	//create test data
	$colors = array("red", "green", "blue");

	//test for the presence of green
	if(in_array("green", $colors))
	{
		print("Yes, green is present!");
	}
?>
</BODY>
</HTML>