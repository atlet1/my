<HTML>
<HEAD>
<TITLE>list</TITLE>
</HEAD>
<BODY>
<h2>list Присваивает переменным из списка значения подобно массиву</h2>
<?
	$colors = array("red", "green", "blue");
//list() - мовна конструкція, для присвоєння списку змінних значення за одну операцію (як і array()) 
	list($color, $color1, $color2) = $colors;
	//list($color, , $color2) = $colors;
	print("Цвета: $color, $color1, $color2.<BR>\n");
	
list($a, list($b, $c)) = array(1, array(2, 3));	
var_dump($a, $b, $c);
echo '<BR>';
	//put first two elements of returned array
	//into key and value, respectively
	//list($key, $value) = each($colors);//each - deprecated
	//print("$key: $value<BR>\n");
?>
</BODY>
</HTML>