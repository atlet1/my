<HTML>
<HEAD>
<TITLE>array_shift</TITLE>
</HEAD>
<BODY>
<h2>array_shift Извлекает первый элемент массива</h2>
<?
	//set up an array of color names
	$colors = array("red", "blue", "green");
	
	$firstColor = array_shift($colors);
	
	//print "red"
	print($firstColor . "<BR>\n");

	//dump colors (blue, green)
	print("<PRE>");
	print_r($colors);
	print("</PRE>\n");
?>
</BODY>
</HTML>