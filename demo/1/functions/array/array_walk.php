<HTML>
<HEAD>
<TITLE>array_walk</TITLE>
</HEAD>
<BODY>
<h2>array_walk Применяет заданную пользователем функцию к каждому элементу массива</h2>
<?
	$colors = array("red", "blue", "green");

	function printElement($element)
	{
		print("$element<BR>\n");
	}

	array_walk($colors, "printElement");
?>
</BODY>
</HTML>