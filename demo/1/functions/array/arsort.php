<HTML>
<HEAD>
<TITLE>arsort</TITLE>
</HEAD>
<BODY>
<h2>arsort Сортирует массив в обратном порядке, сохраняя ключи</h2>
<?
	// build array
	$users = array("bob"=>"Robert",  
		"steve"=>"Stephen", 
		"jon"=>"Jonathon");

	// sort array
	arsort($users);

	// print out the values
	for(reset($users); $index=key($users); next($users))
	{
		print("$index : $users[$index] <BR>\n");
	}
?>
</BODY>
</HTML>