<HTML>
<HEAD>
<TITLE>call_user_func</TITLE>
</HEAD>
<BODY>
<h2>call_user_func Вызывает пользовательскую функцию, указанную в первом параметре</h2>
<?
// Пример callback-функции
function my_callback_function() {
    echo 'hello world!';
}
// Пример callback-метода
class MyClass {
    static function myCallbackMethod() {
        echo 'Hello World!';
    }
}
// Type 1: Простой callback
call_user_func('my_callback_function');
// Type 2: Вызов статического метода класса
call_user_func(array('MyClass', 'myCallbackMethod'));
// Type 3: Вызов метода класса
$obj = new MyClass();
call_user_func(array($obj, 'myCallbackMethod'));
// Type 4: Вызов статического метода класса (С PHP 5.2.3)
call_user_func('MyClass::myCallbackMethod');
// Type 5: Вызов относительного статического метода (С PHP 5.3.0)
echo '<hr>';
////////////////////////////////////////
class A {
    public static function who() {
        echo "A\n";
    }
}
class B extends A {
    public static function who() {
        echo "B\n";
    }
}
call_user_func(array('B', 'parent::who')); // A
// Type 6: Объекты реализующие __invoke могут быть использованы как callback (С PHP 5.3)
class C {
    public function __invoke($name) {
        echo 'Hello ', $name, "\n";
    }
}
$c = new C();
call_user_func($c, 'PHP!');
///////////////////////////////////////////
echo '<hr>';
// Наше замыкание
$double = function($a) {
    return $a * 2;
};
// Диапазон чисел
$numbers = range(1, 5);
// Использование замыкания в качестве callback-функции
// для удвоения каждого элемента в нашем диапазоне
$new_numbers = array_map($double, $numbers);
print implode(' ', $new_numbers);

?>
</BODY>
</HTML>