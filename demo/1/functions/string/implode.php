<HTML>
<HEAD>
<TITLE>implode</TITLE>
</HEAD>
<BODY>
<h2>implode Объединяет элементы массива в строку</h2>
<?
	/*
	** convert an array into a comma-delimited string
	*/
	$colors = array("red", "green", "blue");
	$colors = implode($colors, ",");
	print($colors);
////////////////////////////////////////////////
echo '<hr>';	
$array = array('имя', 'почта', 'телефон');
$comma_separated = implode(",", $array);

echo $comma_separated; // имя,почта,телефон
echo '<hr>';
// Пустая строка при использовании пустого массива:
var_dump(implode('hello', array())); // string(0) ""
////////////////////////////////////////////////
echo '<hr>';
$picnames = array("pic1.jpg", "pic2.jpg", "pic3.jpg", "pic4.jpg", "pic5.jpg", "pic6.jpg", "pic7.jpg");
$allpics = implode("|", array_slice($picnames, 0, 5)); 
echo $allpics; 
////////////////////////////////////////////////
echo '<hr>';
$array = array("H", "e", "l", "l", "o");
echo implode("<br/>",$array);
?>
</BODY>
</HTML>