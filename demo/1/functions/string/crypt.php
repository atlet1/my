<HTML>
<HEAD>
<TITLE>crypt</TITLE>
</HEAD>
<BODY>
<h2>crypt Необратимое хэширование строки</h2>
<?
	$password = "secret";

	if(CRYPT_MD5)
	{
		$salt = "leonatkinson";
		print("Using MD5: ");
	}
	else
	{
		$salt = "cp";
		print("Using Standard DES: ");
	}

	print(crypt($password, $salt));
////////////////////////////////////////	
echo '<hr>';	
	$hashed_password = crypt('mypassword'); // соль будет сгенерирована автоматически
/* Для проверки пароля в качестве параметра salt следует передавать результат работы crypt() целиком во избежание проблем при использовании различных алгоритмов (как уже было отмечено выше, стандартный DES-алгоритм использует 2-символьную соль, а MD5 - 12-символьную. */

$user_input = 'mypassword';
if (hash_equals($hashed_password, crypt($user_input, $hashed_password))) {
   echo "Пароль верен!";
}
////////////////////////////////////////	
echo '<hr>';	
// пароль 
$password = 'mypassword';
// получение хэша, соль генерируется автоматически
$hash = crypt($password);
echo $hash;
?>
</BODY>
</HTML>