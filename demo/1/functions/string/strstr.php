<HTML>
<HEAD>
<TITLE>strstr</TITLE>
</HEAD>
<BODY>
<h3>strstr Находит первое вхождение подстроки Возвращает подстроку строки haystack начиная с первого вхождения needle (и включая его) и до конца строки haystack</h3>
<?
	$text = "Although this is a string, it's not very long.";
	if(strstr($text, "it") != "")
	{
		print("The string contains 'it'.<BR>/n");
	}
////////////////////////////////////////////////
echo '<hr>';
$email  = 'name@example.com';
//Если установлен в TRUE, возвращает часть строки haystack до первого вхождения needle (исключая needle). 
$user = strstr($email, '@', true); // Начиная с PHP 5.3.0
echo $user; // выводит name		
?>
</BODY>
</HTML>