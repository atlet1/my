<HTML>
<HEAD>
<TITLE>explode</TITLE>
</HEAD>
<BODY>
<h2>explode Разбивает строку с помощью разделителя Возвращает массив строк</h2>
<?
$pizza  = "кусок1 кусок2 кусок3 кусок4 кусок5 кусок6";
$pieces = explode(" ", $pizza);
echo $pieces[0]; // кусок1
echo $pieces[1]; // кусок2
$sub = implode(", ", $pizza);
print_r($pizza);

/////////////////////////////////////////
echo '<hr>';
/* Строка, которая не содержит разделителя, будет просто возвращать массив с одним значением оригинальной строки.*/
$input1 = "hello";
$input2 = "hello,there";
$input3 = ',';
var_dump( explode( ',', $input1 ) );
echo '<hr>';
var_dump( explode( ',', $input2 ) );
echo '<hr>';
var_dump( explode( ',', $input3 ) );
/////////////////////////////////////////
echo '<hr>';
	/* 
	** convert tab-delimited list into an array
	*/
	$data = "red\tgreen\tblue";
	$colors = explode("\t", $data);

	// print out the values
	for($index=0; $index < count($colors); $index++)
	{
		print("$index : $colors[$index] <BR>\n");
	}
////////////////////////////////////////	
echo '<hr>';

$data = "foo:*:1023:1000::/home/foo:/bin/sh";
list($user, $pass, $uid, $gid, $gecos, $home, $shell) = explode(":", $data);
echo $user; // foo
echo $pass; // *	
?>
</BODY>
</HTML>