<HTML>
<HEAD>
<TITLE>htmlentities</TITLE>
</HEAD>
<BODY>
<h2>htmlentities Преобразует все возможные символы в соответствующие HTML-сущности (html_entity_decode() - для раскодировки)</h2>
<?
	$text = "Use <HTML> to begin a document.";
	print(htmlentities($text));
////////////////////////////////////////////////
echo '<hr>';	
$str = "A 'quote' is <b>bold</b>";
// выводит в исходном коде: A 'quote' is &lt;b&gt;bold&lt;/b&gt;
echo htmlentities($str);
echo '<br>';
// выводит в исходном коде: A &#039;quote&#039; is &lt;b&gt;bold&lt;/b&gt;
echo htmlentities($str, ENT_QUOTES);
////////////////////////////////////////////////
echo '<hr>';
$str = "\x8F!!!";
// Выводит пустую строку
echo htmlentities($str, ENT_QUOTES, "UTF-8");
echo '<br>';
// Выводит "!!!"
echo htmlentities($str, ENT_QUOTES | ENT_IGNORE, "UTF-8");//ENT_IGNORE не рекоменд.
?> 

</BODY>
</HTML>