<HTML>
<HEAD>
<TITLE>mb_strtolower</TITLE>
</HEAD>
<BODY>
<h2>mb_strtolower Многобайтовая кодировка (utf-8) Приведение строки к нижнему регистру</h2>
<?
$str = "У Мэри Был Маленький Ягненок и Она Его Очень ЛЮБИЛА";
$str = mb_strtolower($str);
echo $str; // Выведет: у мэри был маленький ягненок и она его очень любила

?>
</BODY>
</HTML>