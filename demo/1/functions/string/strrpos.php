<HTML>
<HEAD>
<TITLE>strrpos</TITLE>
</HEAD>
<BODY>
<h2>strrpos Возвращает позицию последнего вхождения подстроки в строке</h2>
<?
	//set test string
	$path = "/usr/local/apache";
	
	//find last slash
	$pos = strrpos($path, "/");
	
	//print everything after the last slash
	print(substr($path, $pos+1));
////////////////////////////////////////////////
echo '<hr>';
$foo = "0123456789a123456789b123456789c";
//Поиск происходит в обратном направлении и начинается с 5 позиции с конца: int(17)
var_dump(strrpos($foo, '7', -5));  
echo '<br>';
//Начинает поиск с 20 позиции в строке: int(27)
var_dump(strrpos($foo, '7', 20));  
echo '<br>';
// Результат: bool(false)
var_dump(strrpos($foo, '7', 28));  

?>
</BODY>
</HTML>