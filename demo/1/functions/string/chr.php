<HTML>
<HEAD>
<TITLE>chr</TITLE>
</HEAD>
<BODY>
<h2>chr Возвращает символ по его коду</h2>
<?
$str = "Эта строка заканчивается на escape: ";
$str .= chr(27); // добавляет символ escape в конец $str 
// Но обычно лучше использовать такую конструкцию 
$str1 = sprintf("The string ends in escape: %c", 27);
echo $str;
echo '<br>';
echo $str1;
/////////////////////////////////
echo '<hr>';
	//open a test file
	$fp = fopen("data.txt", "w");
		//write a couple of records that have
	//linefeeds for end markers
	fwrite($fp, "data record 1" . chr(10));
	fwrite($fp, "data record 2" . chr(10));
	
	//close file
	fclose($fp);
?>
</BODY>
</HTML>