<HTML>
<HEAD>
<TITLE>substr</TITLE>
</HEAD>
<BODY>
<h2>substr Возвращает подстроку строки string, начинающейся с start символа по счету и длиной length символов</h2>
<?
	$text = "My dog's name is Angus.";

	//print Angus
	print(substr($text, 17, 5));
/////////////////////////////////////////	
echo '<hr>';
echo substr('abcdef', 1);     // bcdef
echo '<br>';
echo substr('abcdef', 0, 8);  // abcdef
echo '<br>';
echo substr('abcdef', -1, 1); // f
$rest = substr("abcdef", 2, -1);  // возвращает "cde"
echo '<br>';
echo $rest;
/////////////////////////////////////////	
echo '<hr>';
// Получить доступ к отдельному символу в строке можно также с помощью "квадратных скобок", а лучше "фигурных" чтоб с массивом не путать 
$string = 'abcdef';
echo $string[0]; // a
echo '<br>';
echo $string{0}; // a
?>
</BODY>
</HTML>