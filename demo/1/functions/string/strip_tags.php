<HTML>
<HEAD>
<TITLE>strip_tags</TITLE>
</HEAD>
<BODY>
<h2>strip_tags Удаляет HTML и PHP-теги из строки</h2>
<?php
	//create some test text
	$text = "<P><B>Paragraph One</B><P>Paragraph Two";
	//strip out all tags except paragraph and break
	print(strip_tags($text, "<P><BR>"));
////////////////////////////////////////////////
echo '<hr>';
$text1 = '<p>Параграф.</p><!-- Комментарий --> <a href="#fragment">Еще текст</a>';
echo strip_tags($text1);
echo "\n";
$text2 = '<p>Параграф.</p><!-- Комментарий --> <a href="#fragment">Еще текст</a>';
echo strip_tags($text2, '<p><a>');// Разрешаем <p> и <a>
?>
</BODY>
</HTML>

<form action="" method="post">
	<input name="name"><br>
	<input name="age"><br>
	<input type="submit" value="Ok">
</form>

<?php
if($_SERVER['REQUEST_METHOD']=='POST'){
$name = trim(strip_tags($_POST['name']));
$age = abs((int)$_POST['age']);// $age = strip_tags($_POST['age']);
echo '<p>Ваше ім\'я: ' . $name;
echo '<p>Ваш вік: ' . $age;
}
?>