<HTML>
<HEAD>
<TITLE>mb_strpos</TITLE>
</HEAD>
<BODY>
<h2>mb_strpos Многобайтовая кодировка (utf-8) Поиск позиции первого вхождения одной строки в другую</h2>
<?
$mystring = 'abc';
$findme   = 'a';
$pos = mb_strpos($mystring, $findme);
// используется === потому что == не даст верного результата, так как 'a' находится в нулевой позиции.
if ($pos === false) {
    echo "Строка '$findme' не найдена в строке '$mystring'";
} else {
    echo "Строка '$findme' найдена в строке '$mystring'";
    echo " в позиции $pos";
}
////////////////////////////////////////////////
echo '<hr>';
// Можно искать символ, игнорируя символы до определенного смещения
$newstring = 'abcdef abcdef';
$pos = mb_strpos($newstring, 'a', 1, 'utf-8'); // $pos = 7, не 0
echo $pos;
?>
</BODY>
</HTML>