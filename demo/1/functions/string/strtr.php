<HTML>
<HEAD>
<TITLE>strtr</TITLE>
</HEAD>
<BODY>
<h2>strtr Преобразует заданные символы или заменяет подстроки</h2>
<?
	/*
	** Using strtr with 3 arguments
	** to translate characters. Побайтное преобразование, однобайтная кодировка
	*/
	$text = "Wow!  This is neat.";
	$original = "!.";
	$translated = ".?";

	// turn sincerity into sarcasm
	print(strtr($text, $original, $translated) . "<BR>\n");
////////////////////////////////////////////////
echo '<hr>';
echo strtr("baab", "ab", "01"),"\n";
$trans = array("ab" => "01");
echo '<br>';
echo strtr("baab", $trans);
////////////////////////////////////////////////
echo '<hr>';
$trans = array("h" => "-", "hello" => "hi", "hi" => "hello");
echo strtr("hi all, I said hello", $trans);
//c 2 аргументами могут быть заменены и более длинные подстроки

?>
</BODY>
</HTML>