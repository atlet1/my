<HTML>
<HEAD>
<TITLE>stripslashes</TITLE>
</HEAD>
<BODY>
<h2>stripslashes Удаляет экранирование символов</h2>
<?
	$text = "Leon\'s Test String";
	
	print("Before: $text<BR>\n");
	print("After: " . stripslashes($text) . "<BR>\n");
////////////////////////////////////////////////
echo '<hr>';
$stripped = 'this is a string with three\\\ slashes';
//$stripped = stripslashes($stripped);
echo stripslashes($stripped);
echo '<br>';
echo stripslashes(stripslashes($stripped));
////////////////////////////////////////////////
echo '<hr>';
function stripslashes_deep_array($value){
    $value = is_array($value) ?
                array_map('stripslashes_deep_array', $value) :
                stripslashes($value);
    return $value;
}
// Пример
$array = array("f\\'oo", "b\\'ar", array("fo\\'o", "b\\'ar"));
$array = stripslashes_deep_array($array);
print_r($array);// Вывод

?>
</BODY>
</HTML>