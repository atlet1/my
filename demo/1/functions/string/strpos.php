<HTML>
<HEAD>
<TITLE>strpos</TITLE>
</HEAD>
<BODY>
<h2>strpos Возвращает позицию первого вхождения подстроки</h2>
<?
	$text = "Hello, World!";

	//check for a space
	if(strpos($text, 32))
	{
		print("There is a space in '$text'<BR>\n");
	}
	
	//find where in the string World appears
	print("World is at position " . strpos($text, "World") . "<BR>\n");
////////////////////////////////////////////////
echo '<hr>';	
$mystring = 'abc';
$findme   = 'a';
$pos = strpos($mystring, $findme);
// используется === потому что == не даст верного результата, так как 'a' находится в нулевой позиции.
if ($pos === false) {
    echo "Строка '$findme' не найдена в строке '$mystring'";
} else {
    echo "Строка '$findme' найдена в строке '$mystring'";
    echo " в позиции $pos";
}
////////////////////////////////////////////////
echo '<hr>';
// Можно искать символ, игнорируя символы до определенного смещения
$newstring = 'abcdef abcdef';
$pos = strpos($newstring, 'a', 1); // $pos = 7, не 0
echo $pos;
?>
</BODY>
</HTML>