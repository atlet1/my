<HTML>
<HEAD>
<TITLE>mb_substr</TITLE>
</HEAD>
<BODY>
<h2>mb_substr Многобайтовая кодировка (utf-8) Возвращает часть строки</h2>
<?
	$text = "My dog's name is Angus.";

	//print Angus
	print(mb_substr($text, 17, 5, 'utf-8'));
/////////////////////////////////////////	
echo '<hr>';
echo mb_substr('abcdef', 1);     // bcdef
echo '<br>';
echo mb_substr('abcdef', 0, 8);  // abcdef
echo '<br>';
echo mb_substr('abcdef', -1, 1); // f
echo '<br>';
$rest = mb_substr("abcdef", 2, -1);  // возвращает "cde"
echo $rest;
/////////////////////////////////////////	
echo '<hr>';
// Получить доступ к отдельному символу в строке можно также с помощью "квадратных скобок", а лучше "фигурных" чтоб с массивом не путать 
$string = 'abcdef';
echo $string[0]; // a (в квадратных)
echo '<br>';
echo $string{0}; // a (в фігурних)
?>
</BODY>
</HTML>