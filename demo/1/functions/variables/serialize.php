<HTML>
<HEAD>
<TITLE>serialize</TITLE>
</HEAD>
<BODY>
<h3>serialize Генерирует пригодное для для хранения или передачи значений PHP между скриптами без потери их типа и структуры представление переменной (вывод функции serialize() лучше хранить в BLOB поле базы данных, а не в полях типа CHAR или TEXT)</h3>
<PRE>
<?
$colors = array("red", "blue", "green");
$s = serialize($colors);
echo $s;
echo '<hr>';
print_r (unserialize($s));
////////////////////////////////
echo '<hr>';
$var = "red";
$v = serialize($var);
echo $v;
echo '<hr>';
print_r (unserialize($v));
/////////////////////////////////
/*//$session_data содержит многомерный массив с сессионной информацией о текущем 
	//пользователе. Мы используем serialize() для сохранения этой информации в базе
	//данных в конце запроса.
echo '<hr>';
$conn = odbc_connect("webdb", "php", "chicken");
$stmt = odbc_prepare($conn,
      "UPDATE sessions SET data = ? WHERE id = ?");
$sqldata = array (serialize($session_data), $_SERVER['PHP_AUTH_USER']);
if (!odbc_execute($stmt, $sqldata)) {
    $stmt = odbc_prepare($conn,
     "INSERT INTO sessions (id, data) VALUES(?, ?)");
    if (!odbc_execute($stmt, $sqldata)) {
    // Код, выполняемый при ошибке.. 
    }
}
*/
?>
</PRE>
</BODY>
</HTML>