<HTML>
<HEAD>
<TITLE>settype</TITLE>
</HEAD>
<BODY>
<h2>settype Присваивает переменной новый тип</h2>
<?
	$myValue = 123.45;
	settype($myValue, "integer");
	//settype($myValue, "int");
	print($myValue);
	
//"boolean"-"bool", "integer"-"int", "float"-"double", "string", "array", "object", "null"
///////////////////////////////////////////
echo '<hr>';
$bar = '5';
$foo = (int) $bar;
var_dump($foo);
echo '<br>';
$foo = (float) $bar;
var_dump($foo);
echo '<br>';
$str = "$foo";        // $str - это строка
$fst = (string) $foo; // $fst - это тоже строка
var_dump($str, $fst);
echo '<br>';
$arr = [1 => 'foo'];
var_dump($arr);
echo '<br>';
var_dump(key($arr)); // outputs 'int(1)'
echo '<br>';
$obj = (object) array('1' => 'foo');
var_dump($obj);
echo '<br>';
var_dump(isset($obj->{'1'})); // outputs 'bool(true)'
echo '<br>';
var_dump(key($obj)); // outputs 'string(1)'
echo '<br>';
$obj = (object) 'ciao';
echo $obj->scalar;  // выведет 'ciao'
echo '<br>';
$test = null;//unset ($test); //не удаляет переменную и ее знач., а только возвращ NULL 
var_dump ($test);

?>
</BODY>
</HTML>