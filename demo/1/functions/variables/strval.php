<HTML>
<HEAD>
<TITLE>strval</TITLE>
</HEAD>
<BODY>
<h2>strval Возвращает строковое значение переменной</h2>
<?
	$myNumber = 13;
	print(strval($myNumber));
///////////////////////////////////////////
echo '<hr>';
//нельзя применить к массиву или объекту, которые не реализуют метод __toString() 
class StrValTest
{
    public function __toString()
    {
      return __CLASS__;
    }
}
// Выводит 'StrValTest'
echo strval(new StrValTest);

?>
</BODY>
</HTML>