<HTML>
<HEAD>
<TITLE>isset</TITLE>
</HEAD>
<BODY>
<h2>isset Определяет, была ли установлена переменная значением отличным от NULL</h2>
<?
$var = '';
// Проверка вернет TRUE, поэтому текст будет напечатан.
if (isset($var)) {
		echo "Эта переменная определена, поэтому меня и напечатали.";
}
/////////////////////////////////
echo '<hr>';
//Здесь мы используем var_dump для вывода значения, возвращаемого isset().
$a = "test";
$b = "anothertest";

var_dump(isset($a));      // TRUE
echo '<br>';
var_dump(isset($a, $b)); // TRUE
echo '<br>';

unset ($a);

var_dump(isset($a));     // FALSE
echo '<br>';
var_dump(isset($a, $b)); // FALSE
echo '<br>';

$foo = NULL;
var_dump(isset($foo));   // FALSE
/////////////////////////////////
echo '<hr>';
	if(isset($Name))
	{
		print("Your Name is $Name");
	}
	else
	{
		print("I don't know your name");
	}
	?>
</BODY>
</HTML>