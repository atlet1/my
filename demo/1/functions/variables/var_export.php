<HTML>
<HEAD>
<TITLE>var_export</TITLE>
</HEAD>
<BODY>
<h3>var_export Печатает или возвращает интерпретируемое строковое представление переменной. В отличии от var_dump возвращаемое представление является полноценным PHP кодом</h3>
<PRE>
<?
$a = array (1, 2, array ("a", "b", "c"));
var_export($a);
/////////////////////////////////
echo '<hr>';
$b = 3.1;
$v = var_export($b, true);
echo $v;
/////////////////////////////////
echo '<hr>';
class A { public $var0;} //Экспорт классов 
$a = new A;
$a->var0 = 5;
var_export($a);
/////////////////////////////////
echo '<hr>';
class B
{
    public $var1;
    public $var2;

    public static function __set_state($an_array)
    {
        $obj = new B;
        $obj->var1 = $an_array['var1'];
        $obj->var2 = $an_array['var2'];
        return $obj;
    }
}
$b = new B;
$b->var1 = 5;
$b->var2 = 'foo';
eval('$c = ' . var_export($b, true) . ';');
// $b = A::__set_state(array('var1' => 5, 'var2' => 'foo',));
var_dump($c);

?>
</PRE>
</BODY>
</HTML>