<HTML>
<HEAD>
<TITLE>empty</TITLE>
</HEAD>
<BODY>
<h2>empty Проверяет, пуста ли переменная</h2>
<?
$var = 0;
// Принимает значение true, потому что $var пусто
if (empty($var)) {
		echo '$var или 0, или пусто, или вообще не определена';
}
echo '<br>';
// Принимает значение true, потому что $var определена
if (isset($var)) {
		echo '$var определена, даже если она пустая';
}
/////////////////////////////////
echo '<hr>';
$expected_array_got_string = 'somestring';
var_dump(empty($expected_array_got_string['some_key']));
echo '<br>';
var_dump(empty($expected_array_got_string[0]));
echo '<br>';
var_dump(empty($expected_array_got_string['0']));
echo '<br>';
var_dump(empty($expected_array_got_string[0.5]));
echo '<br>';
var_dump(empty($expected_array_got_string['0.5']));
echo '<br>';
var_dump(empty($expected_array_got_string['0 Mostel']));
?>
</BODY>
</HTML>