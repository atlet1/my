<HTML>
<HEAD>
<TITLE>assert</TITLE>
</HEAD>
<BODY>
<h3>assert Проверка утверждения (равен ли результат FALSE) только в целях отладки, для тестирования каких-то условий, которые в штатных ситуациях всегда принимают значение TRUE, обратное должно указывать на программные ошибки, чтобы удостовериться в наличии каких-либо расширений или системных ограничений</h3>
<?php
// Активация утверждений и погашение вывода ошибок
assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_QUIET_EVAL, 1);

// Создание обработчика
function my_assert_handler($file, $line, $code)
{
    echo "<hr>Assertion Failed:
        File '$file'<br />
        Line '$line'<br />
        Code '$code'<br /><hr />";
}

// Подключение callback функции
assert_options(ASSERT_CALLBACK, 'my_assert_handler');

// Выполнение проверки утверждения, которое завершится неудачей
assert('mysqli_query("")');
// Выполнение проверки утверждения, которое завершится неудачей
assert('2 < 1');
assert('2 < 1', 'Два больше чем один');

///////////////////////////////////////////////////////
echo '<hr>';
class CustomError extends AssertionError {}
assert(true == false, new CustomError('True is not false!'));
echo 'Hi!';
?> 

</BODY>
</HTML>