<HTML>
<HEAD>
<TITLE>print_r</TITLE>
</HEAD>
<BODY>
<h2>print_r Выводит удобочитаемую информацию о переменной</h2>
<PRE>
<?
	//define some test variables
	$s = "a string";
	$a = array("x", "y", "z", array(1, 2, 3));
	$a1 = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));

	//print a string
	print_r($s);
	print("\n");

	echo '<hr>';
	
	//print an array
	print_r($a);
	print("\n");
	echo '<hr>';
	print_r($a1);
	print("\n");
	$results = print_r($a1, true); // $results теперь содержит вывод print_r
?>

</PRE>
</BODY>
</HTML>