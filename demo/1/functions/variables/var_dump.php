<HTML>
<HEAD>
<TITLE>var_dump</TITLE>
</HEAD>
<BODY>
<h2>var_dump Выводит информацию о переменной</h2>
<PRE>
<?
$a = true;
$b = 3;
$c = 3.1;
$d = 'String';
var_dump($a, $b, $c, $d);
/////////////////////////////////
echo '<hr>';
$e = array(1, 2, array("a", "b", "c"));
var_dump($e);
/////////////////////////////////
echo '<hr>';
class Obj{
public $name = 'John';}
$f = new Obj;
var_dump($f);
/////////////////////////////////
echo '<hr>';
$g = null;
var_dump($g);
//resource не вийде так і callable ніби теж
?>

</PRE>
</BODY>
</HTML>