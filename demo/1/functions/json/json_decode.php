<HTML>
<HEAD>
<TITLE>json_decode</TITLE>
</HEAD>
<BODY>
<h2>json_decode Декодирует JSON строку Принимает закодированную в JSON строку и преобразует ее в переменную PHP</h2>
<PRE>
<?
$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
var_dump(json_decode($json));
echo '<hr>';
var_dump(json_decode($json, true));
/////////////////////////////////
echo '<hr>';
$json1 = '{"foo-bar": 12345}';
$obj = json_decode($json1);
print $obj->{'foo-bar'}; // 12345
/////////////////////////////////
echo '<hr>';
$json3 = '{"number": 12345678901234567890}';
var_dump(json_decode($json3));
var_dump(json_decode($json3, false, 512, JSON_BIGINT_AS_STRING));
?>

</PRE>
</BODY>
</HTML>