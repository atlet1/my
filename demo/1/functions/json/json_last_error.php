<HTML>
<HEAD>
<TITLE>json_last_error</TITLE>
</HEAD>
<BODY>
<h2>json_last_error Последняя ошибка json</h2>
<PRE>
<?
// Верная json-строка
$json[] = '{"Organization": "PHP Documentation Team"}';
// Неверная json-строка, которая вызовет синтаксическую ошибку,
// здесь в качестве кавычек мы используем ' вместо "
$json1[] = "{'Organization': 'PHP Documentation Team'}";

foreach ($json1 as $string) {
    echo 'Декодируем: ' . $string;
    json_decode($string);

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - Ошибок нет';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Достигнута максимальная глубина стека';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Некорректные разряды или не совпадение режимов';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Некорректный управляющий символ';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Синтаксическая ошибка, не корректный JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Некорректные символы UTF-8, возможно неверная кодировка';
        break;
        default:
            echo ' - Неизвестная ошибка';
        break;
    }
    echo PHP_EOL;
}
?>

</PRE>
</BODY>
</HTML>