<HTML>
<HEAD>
<TITLE>json_encode</TITLE>
</HEAD>
<BODY>
<h2>json_encode Возвращает JSON-представление данных</h2>
<PRE>
<?
$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
echo json_encode($arr);
/////////////////////////////////
echo '<hr>';
$a = array('<foo>',"'bar'",'"baz"','&blong&', "\xc3\xa9");
echo "Обычно: ",     json_encode($a), "\n";
echo "Тэги: ",       json_encode($a, JSON_HEX_TAG), "\n";
echo "Апострофы: ",  json_encode($a, JSON_HEX_APOS), "\n";
echo "Кавычки: ",    json_encode($a, JSON_HEX_QUOT), "\n";
echo "Амперсанты: ", json_encode($a, JSON_HEX_AMP), "\n";
echo "Юникод: ",     json_encode($a, JSON_UNESCAPED_UNICODE), "\n";
echo "Все: ",        json_encode($a, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE), "\n\n";

echo '<hr>';
$b = array();
echo "Пустой массива как массив: ", json_encode($b), "\n";
echo "Пустой массив как объект: ", json_encode($b, JSON_FORCE_OBJECT), "\n\n";

echo '<hr>';
$c = array(array(1,2,3));
echo "Неассоциативный массив как массив: ", json_encode($c), "\n";
echo "JSON_NUMERIC_CHECK: ", json_encode($c, JSON_NUMERIC_CHECK), "\n";
echo "Неассоциативный массив как объект: ", json_encode($c, JSON_FORCE_OBJECT), "\n\n";

echo '<hr>';
$d = array('foo' => 'bar', 'baz' => 'long');
echo "Ассоциативный массив всегда отображается как объект: ", json_encode($d), "\n";
echo "Ассоциативный массив всегда отображается как объект: ", json_encode($d, JSON_FORCE_OBJECT), "\n\n";
/////////////////////////////////
echo '<hr>';
echo "Последовательный массив".PHP_EOL;
$sequential = array("foo", "bar", "baz", "blong");
var_dump(
 $sequential,
 json_encode($sequential)
);
echo PHP_EOL."Непоследовательный массив".PHP_EOL;
$nonsequential = array(1=>"foo", 2=>"bar", 3=>"baz", 4=>"blong");
var_dump(
 $nonsequential,
 json_encode($nonsequential)
);
echo PHP_EOL."Последовательный массив с одним удаленным индексом".PHP_EOL;
unset($sequential[1]);
var_dump(
 $sequential,
 json_encode($sequential)
);

?>
</PRE>
</BODY>
</HTML>