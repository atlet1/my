<?php
session_start();
$_SESSION['token'] = md5(uniqid(mt_rand(), true));
/*Этот прием помогает защититься от категории атак, называемых межсайтовой фальсификацией запросов (CSRF, Cross-Site Request Forgery)*/
?>
<form action="buy.php" method="POST">
<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
<p>Назва товару: <input type="text" name="symbol" /></p>
<p>Кількість: <input type="text" name="quantity" /></p>
<p><input type="submit" value="Придбати товар" /></p>
</form>

<?php
/*Когда вы получаете запрос, представляющий отправку данных формы, проверьте маркеры и убедитесь в том, что они совпадают:*/
session_start();
if ((! isset($_SESSION['token'])) ||
    ($_POST['token'] != $_SESSION['token'])) {
    /* Запросить пароль у пользователя. */
} else {
    /* Продолжить. */
}
?>

<?php
//Если использовался метод get, выведите форму, eсли post - обработайте форму
if ($_SERVER['REQUEST_METHOD'] == 'GET') { ?>
<form action="<?php echo htmlentities($_SERVER['SCRIPT_NAME']) ?>" method="post">
Яке Ваше ім'я?
<input type="text" name="first_name" />
<input type="submit" value="Сказати привіт" />
</form>
<?php } else {
    echo 'Hello, ' . $_POST['first_name'] . '!';
}
?>