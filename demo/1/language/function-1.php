<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Описание и вызов функции</title>
</head>

<body>
<h1>Описание и вызов функции</h1>
<?php
function foo() {
	echo "<h1>Hello, world!</h1>";
}

foo();
foo();
foo();
////////////////////////////////////////////////
echo '<hr>';
$arr = get_defined_functions();
echo '<pre>';
print_r($arr);
echo '</pre>';
?>

</body>
</html>