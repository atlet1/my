<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Массивы</title>
</head>

<body>
<h1>Массивы</h1>
<?php
//error_reporting(E_ALL);// Показываем все ошибки

$arr = array();
$arr0 = [];
/////////////////////////////////////////
$arr1 = array("Январь", "Февраль", "Март", "Апрель",);//можна кому лишити вкінці, щоб ще добаввити щось колись
print_r($arr1); 
echo '<hr>';
////////////////////////////////////////
# автозаполнение
$a[] = "Привет";
$a[] = "Еще";
$a[] = "Привет";
$a[] = "И еще";

# индексные массивы
$b[0] = 'Привет';
$b[1] = 'Еще';
$b[2] = 'Привет';
$b[3] = 'И еще';

$d[1][2] = "123";
print_r($d[1]);
echo '<hr>';
////////////////////////////////////////
# Ассоциативные массивы
$a1["Маша"] = "Привет";
$a1["Саша"] = "Еще";
$a1["Петя"] = "Привет";
$a1["Даша"] = "И еще";
print_r($a1);
echo '<hr>';
//////////////////////////////////////////
$a2 = array("Саша" => 5, "Даша" => "Привет");
print_r($a2);
echo '<hr>';
//////////////////////////////////////////
$user[0] = "John";
$user[] = "root";
$user[] = "1234";
$user[] = 25;
$user[] = true;

echo '<pre>';

print_r($user);
echo '<hr>';

echo $user[0];
echo '<hr>';

var_dump($user);
echo 'Всего ячеек в массиве '. count($user);
echo '<hr>';
echo '</pre>';
/////////////////////////////////////////////
$user1 = array(
			"John",
			"root",
			"1234",
			25,
			true);

print_r($user1);
echo '<hr>';
////////////////////////////////////////////
//Доступ к массиву внутри двойных кавычек
$foo = array('bar' => 'baz');
echo "Hello {$foo['bar']}!"; // Hello baz!
echo '<hr>';
////////////////////////////////////////////
$array = array(
         "a",
         "b",
    6 => "c",
         "d",
);
var_dump($array);
echo '<hr>';
////////////////////////////////////////////
$array1 = array(
    "foo" => "bar",
    42    => 24,
    "multi" => array(
         "dimensional" => array(
             "array" => "foo"
         )
    )
);
//Доступ к элементам массива
var_dump($array1["foo"]);
var_dump($array1[42]);
var_dump($array1["multi"]["dimensional"]["array"]);
echo "Элемент: {$array1['multi']['dimensional']["array"]}";
//при багатомірних масивах всередині рядка завжди фігурні дужки
echo '<hr>';
////////////////////////////////////////////
define('KOOLAID', 'koolaid1');//простий синтаксис без фігурних дужок
$juices = array("apple", "orange", "koolaid1" => "purple");
echo "He drank some $juices[0] juice.".PHP_EOL;
echo "He drank some $juices[1] juice.".PHP_EOL;
echo "He drank some $juices[koolaid1] juice.".PHP_EOL;// без фігурних
echo "He drank some {$juices['koolaid1']} juice.".PHP_EOL;// з фігурними

echo '<hr>';
class people {
    public $john = "John Smith";
    public $jane = "Jane Smith";
    public $robert = "Robert Paulsen";
    
    public $smith = "Smith";
}
$peop = new people();//простий синтаксис без фігурних дужок
echo "$peop->john drank some $juices[0] juice.".PHP_EOL;
echo "$peop->john then said hello to $peop->jane.".PHP_EOL;// без фігурних
echo "{$peop->john} then said hello to {$peop->jane}.".PHP_EOL;// з фігурними
//echo "Это тоже работает: {$object->values[3]->name}";
//echo "Значение переменной по имени, которое возвращает \$object->getName(): {${$object->getName()}}";
echo "$peop->john's wife greeted $peop->robert.".PHP_EOL;
echo "$peop->robert greeted the two $peop->smiths."; // Не сработает
echo '<hr>';
///////////////////////////////////////////
function getArray() {//Разыменование массива PHP 5.4 обращение к елементу массива, возвращаемого функцией
    return array(1, 2, 3);
}
$secondElement = getArray()[1];
echo $secondElement;
echo '<hr>';
////////////////////////////////////////////
$arr2 = array(5 => 1, 12 => 2);
$arr2[] = 56; //В этом месте скрипта это то же самое, что и $arr[13] = 56;
$arr2["x"] = 42; //Это добавляет к массиву новый элемент с ключом "x"
print_r($arr2);
echo '<br>';
unset($arr2[5]); // Это удаляет элемент из массива
print_r($arr2);
echo '<br>';
$arr2 = array_values($arr2);//Переиндексировать массив
print_r($arr2);
echo '<br>';
unset($arr2); // Это удаляет массив полностью
var_dump($arr2);// Масив видалений
echo '<hr>';
////////////////////////////////////////////
$array = [-3 => 'foo'];// PHP 7.1.0 мінусові числові індекси
echo "The element at index -3 is $array[-3].", PHP_EOL;
echo '<hr>';
////////////////////////////////////////////
//how to convert array to stdClass Object and how to access its value for display
$num = array("Garha","sitamarhi","canada","patna"); //create an array
$obj = (object)$num; //change array to stdClass object 
echo "<pre>";
print_r($obj); //stdClass Object created by casting of array 

$newobj = new stdClass();//create a new 
$newobj->name = "India";
$newobj->work = "Development";
$newobj->address="patna";

$new = (array)$newobj;//convert stdClass to array
echo "<pre>";
print_r($new); //print new object
echo '<hr>';

//How deals with Associative Array
$test = ['Details'=>['name','roll number','college','mobile'],'values'=>['Naman Kumar','100790310868','Pune college','9988707202']];
echo '<br>';
echo $test['Details'][0];

$val = json_decode(json_encode($test),false);//convert array into stdClass object
echo "<pre>";
print_r($val);

echo '<br>';
echo ((is_array($val) == true ?  1 : 0 ) == 1 ? "array" : "not an array" )."</br>"; // check whether it is array or not
echo ((is_object($val) == true ?  1 : 0 ) == 1 ? "object" : "not an object" );//check whether it is object or not 
echo '<hr>';
////////////////////////////////////////////
$fruits = array();
$fruits['red'][] = 'strawberry';
$fruits['red'][] = 'apple';
$fruits['yellow'][] = 'banana';
print_r($fruits);

foreach ($fruits as $color => $color_fruit) {//подвійний foreach - багатомірний масив
// $color_fruit - массив
	foreach ($color_fruit as $fruit) {
		print "$fruit is colored $color.<br>";
	}
}
echo '<hr>';
////////////////////////////////////////////
	$menu = array(
		"foreach"=>"foreach.php",
		"Page1"=>"for.php", 
		"Page2"=>"while.php"
		);
print_r($menu);	
echo '<hr>';
?>
<li><a href='<?=$menu['foreach']?>'>foreach</a></li>

</body>
</html>