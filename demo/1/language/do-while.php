<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Цикл do-while</title>
</head>

<body>
<h1>Цикл do-while</h1>

<?php
$i = 0;
do {
    echo $i;
} while ($i > 0);
//////////////////////////////////////
echo '<hr>';
$a = 1;

do {
	print $a . ", ";
	$a *= 2;
} while ($a < 1000);
////////////////////////////////////////
echo '<hr>';
do {
    if ($i < 5) {
        echo "i еще недостаточно велико";
        break;
    }
    $i *= $factor;
    if ($i < $minimum_limit) {
        break;
    }
   echo "значение i уже подходит";

/* обработка i */

} while (0);
?>
</body>
</html>
