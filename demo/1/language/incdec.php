<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Oператоры инкремента и декремента</title>
</head>

<body>
<h1>Oператоры инкремента и декремента</h1>
<?php
	$a = 1;
	echo $a++, '<br>';
	echo $a;
////////////////////////////////////	
	echo '<hr>';
	$a = 1;
	echo ++$a, '<br>';
	echo $a;
/////////////////////////////////	
	echo '<hr>';
	echo $a++, '<br>'; //то же, что и
	echo $a, '<br>'; $a = $a + 1;
	
	echo ++$a, '<br>';// то же, что и
	$a = $a + 1; echo $a, '<br>'; 
	
?>

</body>
</html>