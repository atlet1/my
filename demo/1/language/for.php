<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Цикл for</title>
</head>

<body>
<?php
for ($i = 1; $i <= 10; $i++) {
echo $i;
}
///////////////////////////////////////
echo '<hr>';
$i = 1;
for (; ; ) {
    if ($i > 15) {
        break;
    }
    echo $i;
    $i++;
}
///////////////////////////////////////
echo '<hr>';
for ($i = 1, $j = 0; $i <= 10; $j += $i, print $i, $i++);
///////////////////////////////////////	
echo '<hr>';
for ($i=0; $i<10; $i++){
		echo $i, "<br>";
	}	
////////////////////////////////////
echo '<hr>';
for($num = 1; $num <= 49; $num += 2){
			print "$num<br />";
		}
////////////////////////////////////
echo '<hr>';
	for ($j=0; $j<32; $j+=2) {
		if ($j == 8) 
			continue;
		echo $j, "<br>";
		if ($j == 30) 
			break;
	}
////////////////////////////////////
echo '<hr>';
	$a[0] = "Привет";
	$a[1] = "Еще";
	$a[2] = "Привет";
	$a[3] = "И еще";
	for ($i=0; $i < count($a); $i++){
		echo $a[$i], "<br>";
	}
///////////////////////////////////
echo '<hr>';
$people = array(
    array('name' => 'Kalle', 'salt' => 856412),
    array('name1' => 'Pierre', 'salt' => 215863)
		);
for($i = 0, $size = count($people); $i < $size; ++$i) {
    $people[$i]['salt'] = mt_rand(000000, 999999);
}
print_r ($people[0]["salt"]);
echo '<br>';
print_r ($people[1]["salt"]);
///////////////////////////////////
echo '<hr>';
echo '<table border="1" width="300">';
for($tr = 1; $tr <= 10; $tr++){
	echo '<tr>';
for($td = 1; $td <= 10; $td++){
echo "<td>". $tr * $td. "</td>";
}
echo "</tr>";
}
echo '</table>';
?>
</body>
</html>