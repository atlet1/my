<?php
	$login = "root";
	$password = "megaP@ssw0rd";
	$email = "ivan@petrov.ru";
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<title>Использование функций обработки строк</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<?php
		echo ucfirst($login), '<br />';
		echo ucfirst($password), '<br />';
//С помощью функции substr_count() проверяем e-mail на наличие символа '@'
		if(substr_count($email, '@')){
			echo ucfirst($email), '<br />';
		}
		
		/*if (!ctype_alnum($login)) {//перевірка логіна на букво-цифрове значення
    echo 'Please enter a valid username';
		}*/
	/*if (strpos($_POST['email'], '@') === false) {
    print 'There was no @ in the e-mail address!';
	}*/

////////////////////////////////////////////////
echo '<h1>Строки</h1>';

$name = "Вася Пупкин";

$str1 = "Это $name \$a100 \" \\ \n \r \t строка";

$str2 = 'Это \' $name \$a100 \" \\ \n \r \t строка';

$str3 = "

Это новая строка текста

";

$a = <<<EOF
<pre>
Много текста самого разного
Много текста самого разного
Много текста самого разного $name
Много текста самого разного
Много текста самого разного
Много текста самого разного
</pre>
EOF;

echo $a;
///////////////////////////////////////////
echo '<hr>';
$b = <<<'NOW'
<pre>
Много текста самого разного
Много текста самого разного
Много текста самого разного $name
Много текста самого разного
Много текста самого разного
Много текста самого разного
</pre>
NOW;

echo $b;
///////////////////////////////////////////
echo '<hr>';
$result = $str1 . $str2;

echo $str1, "<br>", $str2, "<br>", $a, "<br>", $result;
///////////////////////////////////////////
echo '<hr>';
$a = "Hello ";//оператор конкатенации ('.'), опер присваивания с конкат ('.=') 
$b = $a . "World!"; // $b теперь содержит строку "Hello World!"
$a .= "World!";     // $a теперь содержит строку "Hello World!"
echo $a;
///////////////////////////////////////////
echo '<hr>';
$beer = 'Heineken';
echo "$beer's taste is great<br>";//works, "'" is an invalid character for varnames
echo "He drunk some $beers<br>"; //won't work, 's' is a valid character for varnames
echo "He drunk some ${beer}s"; //works
echo "<br>";

echo strlen($name);
echo "<br>";
echo mb_strlen($name);
echo "<br>";

//$name = mb_convert_encoding($name, "CP1251");
//$name = utf8_decode($name);
echo $name{0};//краще фігурні, щоб не плутати з масивом
echo "<br>";
echo $name[0];//але і квадратні добре
echo "<br>";

$name[0] = 'V';// Изменение первого символа строки
echo $name{0};
echo "<br>";

$name[mb_strlen($name)-1] = 'N';// Изменение последнего символа строки
echo $name{11};
///////////////////////////////////////////
echo '<hr>';
class cl
{
		const drink = 'beer';
		public static $ale = 'ipa';
		
		public const BAR = <<<FOOBAR
Пример использования константы. 
FOOBAR;
    var $foo;
    public $foo1 = 'foo1';
    public $bar = <<<EOT
bar
EOT;
		public static $bar1 = <<<LABEL
Здесь ничего нет...
LABEL;
    function __construct()
    {
        $this->foo = 'Foo';
        $this->bar = array('Bar1', 'Bar2', 'Bar3');
    }
		function printHello()
    {
			echo 'Hello .';
    }
}
$cln = 'cl';
$obj = new cl();//new $cln();
$name = 'Имярек';

$beer = 'A & W';
$ipa = 'Alexander Keith\'s';
echo "Я бы хотел {${cl::drink}}(конст.)\n";// Я бы хотел A & W
echo "Я бы хотел {${cl::$ale}}(стат. св.)\n";// Я бы хотел Alexander Keith's
echo '<br>';

echo "Свойство: {$obj->foo1}. ";//C фигурн скоб возможен доступ к свойс объекта внутри строк (и без)
echo "Метод: {${$obj->printHello()}}";//метод спочатку виводить чомусь і з одними {}і з. теж
echo cl::BAR;//self::BAR; $cln::BAR
echo 'Константа: '. cl::BAR;
//echo "Константа: {${cl::BAR}}\n";//з HEREDOC не працює, хоч має, тільки з . 
echo 'Статическое свойство: ' . cl::$bar1;
//echo "Статическое свойство: {${cl::$bar1}}\n";//з HEREDOC не працює, хоч має, тільки з . 

echo <<<HERE
Меня зовут "$name". Я печатаю $obj->foo.
Теперь я вывожу {$obj->bar[1]}.
Это должно вывести заглавную букву 'A': \x41
HERE;

echo '<hr>';
var_dump(array(<<<EOD
foobar!
EOD
));

?> 

</body>
</html>