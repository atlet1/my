<?php
	define("MY_CONST", "Hello, world!");
	const MY_CONST1 = 'Hello, world1!';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<title>Константы</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
</head>
<body>
	<h1>Константы</h1>
	<?php
		echo defined("MY_CONST");//Выведет 1, если константа существует
		echo "<h2>",MY_CONST,"</h2>";
		echo "<h2>",MY_CONST1,"</h2>";
		//MY_CONST = "Какое-то другое значение";//Ошибка!!!
///////////////////////////////////////////////////////////
echo '<h1>Предопределённые константы</h1>';

function getFuncName(){
	echo 'Вызвана функция по имени '.__FUNCTION__.'<br>';
}
echo 'Это строка номер '.__LINE__.'<br>';
echo 'Это файл '.__FILE__.'<br>';
echo 'Это каталог '.__DIR__.'<br>';
echo 'Это пространство имен '.__NAMESPACE__.'<br>';
echo 'Это метод '.__METHOD__.'<br>';//OOP
echo 'Это класс '.__CLASS__.'<br>';//OOP
echo 'Это трейт '.__TRAIT__.'<br>';//OOP
getFuncName();
/////////////////////////////////////////////////////////////
echo '<hr>';
$arr = get_defined_constants();
echo '<pre>';
print_r($arr);
echo '</pre>';
?>
<hr>
Мы используем РНР версии <?= PHP_VERSION?>		
</body>
</html>