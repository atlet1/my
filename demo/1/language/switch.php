<?php
	$day = 5;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<title>Конструкция switch</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
</head>
<body>
	<h1>Конструкция switch</h1>
	<?php

	switch($day){
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			echo "Это рабочий день<br>";
			break;
		case 6:
		case 7:
			echo "Это выходной день";
			break;
		default:
			echo "Неизвестный день";
	}
/////////////////////////////////////////////////	
echo '<hr>';	
	$a = 2;
	
	switch ($a){
		case 1: 
			echo "ОДИН";
		case 2: 
			echo "ДВА ";
		case 3: 
			echo "ТРИ ";
		case 4: 
			echo "ЧЕТЫРЕ";
	}
////////////////////////////////////////////////
echo '<hr>';
$b = 1;

switch ($b){
	case 1: 
		echo "ОДИН";
		break;
	case 2: 
		echo "ДВА";
		break;
	case 3: 
		echo "ТРИ";
		break;
	case 4: 
		echo "ЧЕТЫРЕ";
		break;
	default:
		echo "МНОГО";
}
////////////////////////////////////////////////
echo '<hr>';
$i = "пирог";
switch ($i) {
    case "яблоко":
        echo "i это яблоко";
        break;
    case "шоколадка":
        echo "i это шоколадка";
        break;
    case "пирог":
        echo "i это пирог";
        break;
}
////////////////////////////////////////////////
echo '<hr>';
$beer = 'heineken';
switch($beer)
{
    case 'tuborg';
    case 'carlsberg';
    case 'heineken';
        echo 'Хороший выбор';
    break;
    default;
        echo 'Пожалуйста, сделайте новый выбор...';
    break;
}
///////////////////////////////////////////////
echo '<br>';
//if (!empty($_GET["id "])){ // проверяем параметр article на пустоту
$id = $_GET['id'];
    switch($id){
        case 'contact':	include 'contact.php'; break;
        //case 'contact': $page = 'contact.php';break;
        case 'значение2': $page = 'путь до php файла2 нашей страницы';break;
        case 'значение3': $page = 'путь до php файла3 нашей страницы';break;
        default: 'index.php';
				//...
    }
//include $page; //подключаем нужный файл, в зависимости от пришедшего параметра id
//print_r($_GET['id']);		
?>
</body>
</html>