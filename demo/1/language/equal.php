<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Oператоры сравнения</title>
</head>

<body>
<h1>Oператоры сравнения</h1>
<?php
$a = 1;
$b = "1";
$c = 2;

echo $a == $b;
echo $a === $b;
echo '<br>';
echo $a != $c;

echo '<hr>';

echo $a > $c;
echo $a < $c;
echo '<br>';
echo $a >= $c;
echo $a <= $c;

?>
</body>
</html>
