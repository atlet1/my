<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Числа</title>
</head>

<body>
<h1>Числа</h1>
<?php
	$a = 1;
	$b = 3.1415;
	$c = 0xFFFF0C;
	$d = 1.5E6;
	
	//$a = null;

	$e = $a + $b;
	$e = $a - $b;
	$e = $a * $b;
	$e = $a / $b;
	$e = $a % $b;	//деление с остатком

	$a += 2;	// $a = $a + 2;
	$a -= 2;
	$a /= 2;
	$a *= 2;
	$a %= 2;

	$str = "123 i1 23hih";
	$n = $str * 1;

	print $n;

?>

</body>
</html>
