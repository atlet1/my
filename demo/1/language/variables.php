<?php
	$name = "Игорь";
	$age = 40;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<title>Типы переменных, Переменные и вывод</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
</head>
<body>
	<h1>Типы переменных, Переменные и вывод</h1>
	<?php
		echo "Меня зовут: $name", "<br />";
		echo "Мне $age лет";
		unset($age);
//////////////////////////////////////////////
echo '<hr>';
$variable = 0;
$user_counter = 0;
echo $variable;
/////////////////////////////////////////////
echo '<hr>';
$var1 = "Hello";
$$var1 = "world";

echo $var1, ' ', $Hello .'<br>';

$abc = "var1";
print $$abc;	
/////////////////////////////////////////////
echo '<hr>';
$juice = "apple";
echo "He drank some juice made of {$juice}s.";//точний кінець змінної ${juice}s
echo "Это значение переменной по имени $name: {${$name}}";//{${'name'}}" - виводить Игорь
//echo "Значение переменной по имени, которое возвращает функция getName(): {${getName()}}";
////////////////////////////////////////////////
echo '<hr>';
$arr = get_defined_vars();
echo '<pre>';
print_r($arr);
echo '</pre>';
?>
</body>
</html>