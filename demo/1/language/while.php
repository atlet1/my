<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Цикл while</title>
</head>

<body>
<?php
$i = 1;
while ($i <= 10) {
echo $i++;
}
//////////////////////////////////
echo '<hr>';
$num = 1;
		while ($num <= 49){
			print "$num<br />";
			$num += 2;
}
//////////////////////////////////
echo '<hr>';
$a = 1;
while ($a < 1000) {
	print $a . ", ";
	$a *= 2;
}
///////////////////////////////////
echo '<hr>';
$a = 1;
while ($a < 1000):
	print $a . ", ";
	$a *= 3;
endwhile;
////////////////////////////////////
echo '<hr>';
$i = 1;
while ($i <= 10):
    echo $i;
    $i++;
endwhile;
///////////////////////////////////
echo '<hr>';
$str = 'HELLO';
$i = 0;
$len = mb_strlen($str);
while($i < $len){//while($i < mb_strlen($str)){ не робити, бо 6 раз виклик функції
 echo $str{$i},'<br/>';
    $i++;
}
echo '<br>';
$num = 0;
while ($letter = mb_substr('Hello', $num, 1)){
    echo $letter . '<br>';
    $num++;
}
///////////////////////////////////
echo '<hr>';
//while($row = mysqli_fetch_assoc($result)){}
//while($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){}
//while($name=readdir($dir)){if(($name=='.') or ($name=='..')){continue;}
//while($stmt->fetch(PDO::FETCH_BOUND))echo "$id : $name : $email\n";	
//$dir = opendir("/home/folder/files");	while ($file = readdir($dir)) {}
//$f = fopen("/home/folder/files/file.txt", "r"); while (!feof($f)) {}
//while ($queue->count() > 0){	echo $queue->dequeue()->doIt();}
?>
</body>
</html>