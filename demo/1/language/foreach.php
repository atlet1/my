<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Цикл Foreach ("Для каждого")</title>
</head>

<body>
<?php
$arr = array('a'=>'one','b'=>'two', 'c'=>'three');
foreach ($arr as $value) {
echo "$value\n";
}
echo '<hr>';
////////////////////////////////////////
foreach ($arr as $key => $value) {
echo "$key => $value\n";
}
echo '<hr>';
///////////////////////////////////////
foreach ($arr as &$val) {
$val = "_$val_";
}
echo "_$val_";
echo '<hr>';
// Индексный массив
$arr = array("Январь", "Февраль", "Март", "Апрель");
foreach ($arr as $i) {
	print $i . "<br>";
}
echo '<hr>';
// Ассоциативный массив
$a2 = array("Саша" => 5, "Даша" => "Привет");
foreach ($a2 as $key => $value) {
	print "$key: $value <br>";
}
echo '<hr>';
//////////////////////////////////////////
$menu = array(
		"foreach"=>"foreach.php",
		"Page1"=>"for.php", 
		"Page2"=>"while.php"
		);
foreach($menu as $key=>$value){
		echo "<li>";
		echo "<a href=$value>$key</a>";
		echo "</li>";
}
echo '<hr>';
//////////////////////////////////////////
class people {
    public $john = "John Smith";
    public $jane = "Jane Smith";
    public $robert = "Robert Paulsen";
}
$people = new people();
foreach($people as $key=>$value){
		print "$key: $value <br>";
}
echo '<hr>';
//////////////////////////////////////////
$fruits = array();
$fruits['red'][] = 'strawberry';
$fruits['red'][] = 'apple';
$fruits['yellow'][] = 'banana';
print_r($fruits);

foreach ($fruits as $color => $color_fruit) {//подвійний foreach - багатомірний масив
// $color_fruit - массив
	foreach ($color_fruit as $fruit) {
		print "$fruit is colored $color.<br>";
	}
}
?>
</body>
</html>