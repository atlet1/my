<?php
	$age = 25;//Возраст
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<title>Конструкции if-elseif-else</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
</head>
<body>
	<h1>Конструкции if-elseif-else</h1>
<?php
		if($age >= 18 and $age < 60){
			 echo "Вам ещё работать и работать";
		}elseif($age >= 1 and $age < 18){
			echo "Вам ещё рано работать";
		}elseif($age >= 60){
			echo "Вам пора на пенсию";
		}else{
			echo "Неизвестный возраст";
		}
//////////////////////////////////////////////
echo '<hr>';
echo '<h1>Условный оператор if</h1>';
$shop = true;
	if($shop)
		echo 'Идём в магазин';
	echo '<hr>Идём домой';
////////////////////////////////////////////
echo '<hr>';
$shop1 = false;
	if($shop1)
		echo 'Идём в магазин';
	else
		echo 'Идём в киоск';
	echo '<hr>Идём домой';
////////////////////////////////////////////	
echo '<hr>';
$shop2 = false;
	$kiosk = false;
	if($shop2)
		echo 'Идём в магазин';
	elseif($kiosk)
		echo 'Идём в ближний киоск';
	else	
		echo 'Идём в дальний киоск';
	echo '<hr>Идём домой';	
//////////////////////////////////////////////	
echo '<hr>';
$a = 0;

if ($a == 0) {
	print "УРА<br>";
}
else {
	print "Все плохо!";
}
//////////////////////////////////////////////
echo '<hr>';
if ($a === false) {
	print "УРА";
}
else {
	print "Все плохо!";
}	
//////////////////////////////////////////////
echo '<hr>';
$abc = true;

if ($abc) print "УРА<br>";
/////////////////////////////////////////////
if ($abc) {
	print "УРА";
	print "<br>";
}
/////////////////////////////////////////////
if ($abc) {
	print "УРА";
	print "<br>";
}
else {
	print "ВСЕ ПЛОХО";
	print "<br>";
}
/////////////////////////////////////////////
echo '<hr>';
$xyz = true;
if ($abc) {
	print "УРА";
	print "<br>";
}
elseif ($xyz) {
	print "ВСЕ ПЛОХО";
	print "<br>";
}
///////////////////////////////////
echo '<hr>';
if ($abc):
	print "УРА";
	print "<br>";
else:
	print "ВСЕ ПЛОХО";
	print "<br>";
endif;
?>

<hr>
<?php if ($abc): ?>
<h1>УРА</h1>
<?php else: ?>
<h3>Все плохо</h3>
<?php endif ?>

</body>
</html>