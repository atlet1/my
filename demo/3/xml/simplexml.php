<?php
header("Content-Type: text/html;charset=utf-8");	
$sxml = simplexml_load_file('catalog.xml');

	//$titles = $sxml->xPath("/catalog/book/title[@id>3 and @id<6]");
	
	//  Вывод названия 1-й книги
	echo $sxml->book[0]->title;
	//echo $sxml->book[0]->title->attributes();
	//echo $sxml->book[0]->title["id"];
	
	echo '<br>';
	/*echo "<pre>";
	var_dump($titles);
	echo "</pre>";*/
	echo $sxml->book[1]->pubyear = "2005";
	echo '<br>';
	// Сохранение содержимого
	file_put_contents("catalog.xml", $sxml->asXML());//объект в строку - asXML()
/////////////////////////////////////////////////////////////
$string = <<<XML
<html>
	<head>
		<title> Пример XML-документа </title>
	</head>
	<body background="silver">
		<h1 align="center">Добро пожаловать</h1>
		<p>Это первый абзац</p>
		<p>
			А это второй абзац, в котором используется
			<br />принудительный перевод строки
		</p>
		<p>
			В третьем абзаце присутствует ссылка на <a href="http://specialist.ru">www.specialist.ru</a>; 
			Центр Компьютерного Обучения.
		</p>
		<p>Это <a href="#">мой</a>сайт</p>
	</body>
</html>
XML;
//">www.specialist.ru</a>&mdash; 

$sxml1 = simplexml_load_string($string);//строку в объект
echo $sxml1->body[0]->p;
echo '<hr>';
print_r($sxml1);
//echo strip_tags($sxml1->body[3]->p->asXML());	//объект в строку
?>
<html>
	<head>
		<title>Каталог</title>
	</head>
	<body>
	<hr>
	<h1>Каталог книг</h1>
	<table border="1" width="100%">
		<tr>
			<th>Автор</th>
			<th>Название</th>
			<th>Год издания</th>
			<th>Цена, руб</th>
		</tr>
	<?php
		//Парсинг
		foreach ($sxml->book as $item) {//book as $book
		echo "<tr>";
		echo "<td>", $item->author, "</td>";
		echo "<td>", $item->title, "</td>";
		echo "<td>", $item->pubyear, "</td>";
		echo "<td>".$item->price."</td>";
		echo "</tr>";
	}
	?>
	</table>
	</body>
</html>