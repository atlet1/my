<?php
class minmax {

	private function min() {
		echo 'yes';
	}
	
	public function max() {
		echo 'no';
	}

	public function __call($method,$params) {
		if(!is_array($params)) {
			return FALSE;
		}
		
		$value = $params[0];
		if($method == 'min') {
			for($i = 0; $i < count($params); $i++) {
				if($params[$i] < $value) {
					$value = $params[$i];
				}
			}
		}
		
		if($method == 'max') {
			for($i = 0; $i < count($params); $i++) {
				if($params[$i] > $value) {
					$value = $params[$i];
				}
			}
		}
		
		
		return $value;
		
	}
	
}

$minmax = new minmax();

echo $minmax->min(10,20,3,4,5,1);

?>