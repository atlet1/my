<?php

class Types {
	private $vars = array();
	
	static $type = array('name' => 'string',
							'number' => 'integer');
	
	private function __set($name,$value) {
		if(array_key_exists($name,self::$type)) {
			if($this->my_foo(self::$type[$name],$value)) {
				$this->vars[$name] = $value;
			}
			else {
				exit('ERROR TYPES');
			}
		}
		
		else {
			exit('ERROR NAME');
		} 
	}
	
	private function __get($name) {
		if(array_key_exists($name,$this->vars)) {
			return $this->vars[$name];
		}
	}
	
	protected function my_foo($case,$val) {
		switch($case) {
			case 'string':
			return is_string($val);
			break;
			
			case 'integer':
			return is_integer($val);
			break;
			
			default:
			return FALSE;
			
			
		}
	}						
}


$types = new Types();

$types->number1 = 1;
echo $types->number1;


?>