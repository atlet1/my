<?php

class Page {

	private $header;
	
	public $content;
	public $footer;
	
	public function __construct($h,$c,$f) {
		$this->header = $h;
		$this->content = $c;
		$this->footer = $f;
	}
	
	public function __toString() {
		return $this->header.'<br>'.$this->content.'<br>'.$this->footer;
	}
	

/*
	///////////////////////
	public function __set($name,$value) {
		$this->$name = $value;
	}
	
	//////////////////////
	public function __get($name) {
		return $this->$name;
	}
	
	public function __call($name,$params) {
		if($name == 'print') {
			$this->view();
		}
		if($name == 'echo') {
			$this->view();
		}
	}
	*/
	
}

$page = new Page('HEADER','CONTENT','FOOTER');
/*
$page->header = 'Hello world';

echo $page->header;

$page->get_body('a','b','c');
*/

echo "$page";


?>