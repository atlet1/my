<?php
class Last implements IWidgets {
	public $last;
	
	public function init() {
		$this->last[] = 'STR1';
		$this->last[] = 'STR2';
		$this->last[] = 'STR3';
		$this->last[] = 'STR4';
	}
	
	public function get_body() {
		return $this->last;
	}
}
?>