<?php
class Render {
	public $dir;
	public $name;
	public $content;
	
	public function __construct($dir) {
		if(is_dir($dir)) {
			$this->dir = scandir($dir);
			//print_r($this->dir);
		}
		else {
			exit('Wrong dir');
		}
		
	}
	
	public function get_name() {
		foreach($this->dir as $item) {
			if($item == '.' || $item == '..' || $item == 'IWidgets.php') {
				continue;
			}
			$this->name[] = $item;
			
		}
		//print_r($this->name);
	}
	public function get_widgets() {
		$this->get_name();
		foreach($this->name as $file) {
			$class = basename($file,'.php');
			$ob = new $class;
			
			if($ob instanceof IWidgets) {
				$ob->init();
				$this->content[] = $ob->get_body();
				
				unset($ob);
			}
			else {
				unset($ob);
				continue;
			}
		}
		$this->view();
	}
	
	protected function view() {
		foreach($this->content as $item) {
			echo '<div style="border:1px solid #074776;width:200px; padding:20px">';
				foreach($item as $result) {
					echo '<p>'.$result.'</p>';
				} 
			echo '</div>';
		}
	}
}
?>