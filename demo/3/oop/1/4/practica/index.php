<?php
header("Content-Type:text/html;charset='utf-8'");

set_include_path(get_include_path().PATH_SEPARATOR.'classes');

function loadClass ($class) {
		include $class.'.php';
		}
spl_autoload_register('loadClass');

$dir = new Render('classes');
$dir->get_widgets();

?>