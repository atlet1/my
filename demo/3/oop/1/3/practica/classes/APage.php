<?php
include 'IPage.php';
abstract class APage implements IPage {
	
	protected $text;
	protected $db;//object classa Database
	
	public function get_all() {
		$this->db = $this->conn();
		$this->text = $this->db->get_all_db();
	}
	
	public function get_one($id) {
		$this->db = $this->conn();
		$this->text = $this->db->get_one_db($id);
	}
	
	protected function conn() {
		return new Database(HOST,USER,PASS,DB);
	}
	
	public function get_body($text,$file) {
		
		ob_start();
		include $file.'.php';
		
		
		return ob_get_clean();
	}
}
?>