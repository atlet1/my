<?php
include 'config.php';
header("Content-Type:text/html;charset='utf-8'");

function loadClass ($file) {
	if(file_exists('controllers/'.$file.'.php')) {
		require_once 'controllers/'.$file.'.php';
	}
	else {
		require_once 'model/'.$file.'.php';
	}
}
spl_autoload_register('loadClass');

if(isset($_GET['option'])) {
	$class = strip_tags($_GET['option']);
	
	switch ($class) {
		
		case 'view':
		$init = new View();
		break;
		
		default :
		$init = new Index();
		break;
	}
}
else {
	$init = new Index();
}

echo $init->get_body();
?>