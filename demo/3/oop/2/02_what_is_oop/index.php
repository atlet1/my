<?php
/**
 * Typical procedural call
 */
// Set a database connection
$dbname = 'eshop';
$username = 'root';
$password = '';
try {
    $conn = new PDO('mysql:host=localhost;dbname=' . $dbname, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
}
catch (Exception $e) {
    die($e->getMessage());
}
// Run a query
$sql = "SELECT * FROM `orders` WHERE datetime < NOW() ORDER BY datetime DESC";
try {
    $result = $conn->query($sql);
}
catch (Exception $e) {
    die($e->getMessage());
}
// Display post titles
if ($result->rowCount()) {
    foreach ($result as $post) {
        echo '<h1>' . $post['title'] . '</h1>';
    }
}
/**
 * Typical OOP call
 */
class PostModel{
}
$post = new PostModel();
$posts = $post->fetchAll();
foreach ($posts as $post) {
    echo '<h1>' . $post->title . '</h1>';
} 