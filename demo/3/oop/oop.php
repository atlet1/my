<?php
$x = 10;
$isValid = new Validator($x);
echo $isValid->isInt()->noEmpty()->execut();//exec// якщо 0, помилок немає, якщо 1 - помилка
//if($isValid->isInt()->isPos()->execut())
//echo 'Valid';

//$x = '';
//$isValid = new Validator($x);
//if($isValid->isStr()->noEmpty()->execut())

	class Validator{
	private $_val;
	private $_err = 0;
	
	function __construct($v){$this->_val = $v;}
	
	function execut(){//do_it (було exec)
		return !$_err;
	}
		function isInt(){
		if(!is_integer($this->_val))
			$_err++;
		return $this;
		}
	function isPos(){
		if(abs($this->_val) !== $this->_val)
			$_err++;
		return $this;
	}
	function isStr(){
		if(!is_string($this->_val))
			$_err++;
		return $this;
	}	
	function noEmpty(){
		if(empty($this->_val))
			$_err++;
		return $this;
	}	
}

echo '<br>';
class Person{
	private $_name;
	private $_age;
	function __set($n, $v){
		switch($n){
			case 'name': $this->_name = $v;break;
			case 'age': $this->_age = $v;break;
			default: throw new Exception('ERROR!');
		}
	}
	function __get($n){
		switch($n){
			case 'name': return $this->_name = $v;break;
			case 'age': return $this->_age = $v;break;
			default: throw new Exception('ERROR!');
		}
	}
	function __call($name, $params){
		echo "Вызван метод $name с параметрами";
			echo "<pre>";
			print_r($params);	
			echo "</pre>";
	}
	function __toString(){
		return "HELLO";	
	}
}
try{
$x = new Person;
$x->_name = 'John';
echo $x->_name;
$x->foo();
echo $x;
}catch(Exception $e){
	echo $e->getMessage();			
}
	
/*function foo(){}// в ін. мовах прог перезавантаження функцій PHP - ні
function foo($a){}
function foo(int $a int $b){}
function foo(int $a string $b){}*/
