<?php
class MyExceptionOne extends Exception{
	function __construct($msg){
		parent::__construct($msg);
	}
}
class MyExceptionTwo extends Exception{
	function __construct($msg){
		parent::__construct($msg);
	}
}
class Animal{
	public $name;// властивості класу
	public $age = 0;
	function sayHello($word){// методи класу і об'єкта простою функціжю описується
		echo $this->name.' сказав '.$word;	// доступ до властивостей через $this
		$this->dravBR();
	}
	function dravBR(){
		echo '<br>';
	}
	function __construct($x=0, $y=0){// function __construct($num=0) конструктор, встроєна функція, зручно для ініціалізації
	// function Animal(){	а 4-му PHP так викликався конструктор, по імені класу
		try{//винятки, що робити при некритичній помилці
			if(!$x)//if(!$num)
				throw new MyExceptionOne('Немає 1 параметра<br>');//throw new Exception('Немає номера');
			if(!$y)
				throw new MyExceptionTwo('Немає 2 параметра<br>');
			if(!$z)
				throw new Exception('Немає z параметра<br>');
		echo "Object #$x created<br>";//echo "Object #$num created<br>";
		}catch(MyExceptionOne $e){//catch(Exception $e){
			echo 'HERE '.$e->getMessage();//виводить перший, який знаходить
		}catch(MyExceptionTwo $e){
			echo $e->getMessage();
		}catch(Exception $e){
			echo $e->getMessage();			
		}
	}
	function __destruct(){//видалення об'єктів
		echo "Object deleted<br>";
	}	
	function __clone(){//конструктор для об'єкта, створеного посиланням
		echo "Object cloned<br>";
	}	
}
$cat = new Animal(1);//створили і присвоїли, $cat екземпляр класу, якщо писати: new Animal(); то створили не присвоїли, в пам'яті буде
$dog = new Animal();//$dog = new Animal
$dog2 = new Animal(1,2,3);
$cat->name = 'Мурзик';//присвоїли, перемінна в перемінній ніби
$bigCat = clone $cat;//посилання на об'єкт замість & в тілі об'єкта використовується
$dog->name = 'Тузик';
//echo $cat->name;
$cat->sayHello('Мяу');
$dog->sayHello('Гав');

///////////////////////////   MY  /////////////////////////////////////////
echo '<hr>';
class Animal1{
	public $name;
	public $age = 0;
	function SayHello($word){
		echo $this->name.' сказав '.$word;
		$this->InsBr();
	}
	function InsBr(){
		echo '<br>';
	}
	function __construct($num = 0){
		echo "Object $num created<br>";
	}
		function __destruct(){
		echo "Object {$this->name} deleted<br>";
	}
	function __clone(){
			echo "Object cloned<br>";
	}
}	
$cat = new Animal1(1);
$dog = new Animal1(2);
$bigdog = clone $dog;
$cat->name = 'Мурзик';	
$dog->name = 'Тузик';	
$bigdog->name = 'Тузь';	
//echo $cat->name;
$cat->SayHello('Мняв');
$dog->SayHello('Гав');
$bigdog->SayHello('Г-а-в');
$dog->InsBr();
////////////////////////////////////////////////////
class User {
	public $name;
	public $email;
	protected $login;
	protected $password;
	
	function __construct($name, $email, $login, $pas) {
		$this->name = $name;
		$this->email = $email;
		$this->login = $login;
		$this->password = $pas;
	}
	function showInfo(){
		echo 'Ваше ім\'я: '.$this->name.'<br>';
		echo 'Ваш емайл: '.$this->email.'<br>';
		echo "Ваш логін: {$this->login}<br>";
		echo "Ваш пароль: {$this->password}<br><br>";
	}
	function __destruct(){
		echo '<br>Користувач '.$this->name.' видалений<br>';
	}
	/*function setName($name){
	$this->name=$name;	
	}
	function getName(){
	return $this->name;	
	}*/
}		
	$user1 = new User('Вася', 'vasa@vasa.net', 'vasa', 'pass');
	$user1->showInfo();
//$user1->setName('Вася');//можна вставити дані з GET, POST, бази даних
	
	$user2 = new User('Іван', 'ivan@ivan.net', 'ivan', 'pass1');
	$user2->showInfo();

class SUser extends User {
	public $role;
	function __construct($name, $email, $login, $pas, $role) {
		parent::__construct($name, $email, $login, $pas);
		$this->role = $role;
	}
	function showInfo(){
		parent::showInfo();
		echo 'Ваша роль: '.$this->role.'<br>';	
	}	
	function __destruct(){
		echo "Користувач {$this->name} видалений";
}
function getInfo(){
		$arr = array();
		foreach($this as $k=>$v)
		$arr[$k] =$v;
		return $arr;
	}
}	
	$user3 = new SUser('Петро', 'petro@petro.net', 'petro', 'pass2', 'admin');
	$user3->showInfo();
?>