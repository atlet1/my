<?php
spl_autoload_register(function ($class_name) {
	echo "<p>Попытка создать объект несуществующего класса - ",	$class_name;
		include 'classes/' . $class_name . '.php';});
//$inc_obj = new IncClass();//несуществующий класс
 
/*function loadClass ($class_name) {
require_once($class_name.'.php');
}
spl_autoload_register('loadClass');
spl_autoload_functions();//Получение списка всех зарегистрированных функций */ 

	// Описание класса
	class MyClass {
		// Общедоступное свойство
		public $property = 1;
		function __construct() {//будет вызван при каждом создании нового объекта, например, для инициализации какого-либо состояния объекта перед его использованием 
			echo "Вызван конструктор класса MyClass<br>\n";
		}
		function __clone()
    {
			echo "Вызван конструктор клона MyClass<br>\n";
      //$this->property = clone $this->property;
    }

	/*function __construct($var1, $var2, $var3) {
			$this->property = $var1;
		}
	}
	$obj = new MyClass(1, 2, 3);
	$obj1 = new MyClass(4, 5, 6);
	echo $obj->property;*/
		
		// Деструктор
		function __destruct() {//будет вызван при освобождении всех ссылок на определенный объект или при завершении скрипта
			echo "Вызван деструктор класса MyClass<br>\n";
		}
			// Определение методов класса	
	function myMethod() {
			echo $this->property;//$this является ссылкой на вызываемый объект. Обычно это тот объект, которому принадлежит вызванный метод, но может быть и другой (static)
		}
	}
echo (new MyClass())->property;//5.4.0 введена возможность обратиться к свойству или методу только что созданного объекта в одном выражении
echo '<br>';

	// Создание экземпляра класса (объект на основе класса)
	$obj = new MyClass();
	echo (clone $obj)->property;//7.0.0 введена возможность обратиться к свойству или методу только что клонированого объекта в одном выражении (после создания объекта работает)
echo '<br>';
	// Копирование объекта
	$obj1 = clone $obj;

	// Вывод значения свойства
	echo $obj->property;//объекта
	echo '<br>';
	echo $obj1->property;//клона
	echo '<br>';
	// Вызов метода
	$obj->myMethod();
	echo '<br>';
	// Принадлежит ли объект классу
	if ($obj instanceof MyClass) {
		echo "Объект \$obj - объект класса MyCalss<br>";
	}
	
	// Изменение значения свойства
	$obj->property = 2;
	echo $obj->property;
	echo '<br>';	
	unset($obj);
	echo "<p>Конец сценария <hr>";
	/////////////////////////////////////////
	class MyClass2 {
		public $props;
		private $data = array();//Место хранения перегружаемых данных __isset __unset
		//private $_name;
		//private $_age;
		function __set($name,$value) {//будет выполнен при записи данных в недоступные свойства
			$this->props[$name] = $value;	
			switch($n){
				case "name":
				return $this->_name;
				case "age":
				return $this->_age;
				default:
				echo "ERROR!<br>";
			}	
		}
		function __get($name) {//будет выполнен при чтении данных из недоступных свойств
			return $this->props[$name];	
		}
		 public function __isset($name)//будет выполнен при использовании isset() или empty() на недоступных свойствах
    {
        echo "Установлено ли '$name'?\n";
        return isset($this->data[$name]);
    }
    public function __unset($name)//будет выполнен при вызове unset() на недоступном свойстве
    {
        echo "Уничтожение '$name'\n";
        unset($this->data[$name]);
    }

	}
	
	$obj2 = new MyClass2();
	$obj2->title = "PHP5";
	echo $obj2->title;	
	echo '<hr>';
/////////////////////////////
class MyClass3 {
		public $props;
		function __call($name, $params) {//в контексте объекта при вызове недоступных методов
			echo "Вызван метод $name с параметрами:";
			echo "<pre>";
			print_r($params);	
			echo "</pre>";
		}
	}
	
	$obj3 = new MyClass3();
	$obj3->foo(1,2,3);
	echo '<hr>';
///////////////////////////////////
class MyClass4 {
         function __toString() {//как класс реагирует при преобразовании в строку 
             return "Вызван метод __toString()<br>";
         }
    public function __invoke($x)//когда скрипт пытается выполнить объект как функцию
    {
        var_dump($x);
    }
}
     $obj4 = new MyClass4; 
     echo $obj4; // Выводит "вызван метод __toString()"
		 echo $obj4(5); // int(5)
		 echo '<br>';
		 var_dump(is_callable($obj4));
		 echo '<br>';
//Сериализация объектов		 
		$s = serialize($obj4);//page1.php
		echo $s;
// сохраняем $s где-нибудь, откуда page2.php сможет его получить.
//file_put_contents('store', $s);
//$s = file_get_contents('store');//page2.php
		$obj4 = unserialize($s);
		echo '<br>';
		var_dump($obj4);
		echo '<hr>';
/////////////////////////////////////////////
echo '__sleep, __wakeup';
class Connection
{
    protected $link;
    private $dsn, $username, $password;
    
    public function __construct($dsn, $username, $password)
    {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->connect();
    }
    
    private function connect()
    {
        $this->link = new PDO($this->dsn, $this->username, $this->password);
    }
    
    public function __sleep()//метод выполняется прежде любой операции serialize()
/*завершение работы над данными, ждущими обработки или других подобных задач очистки. Кроме того, этот метод можно выполнять, когда нет необходимости сохранять полностью очень большие объекты*/
    {
        return array('dsn', 'username', 'password');
    }
    
    public function __wakeup()//метод выполняется прежде любой операции unserialize()
/*для восстановления любых соединений с базой данных, которые могли быть потеряны во время операции сериализации и выполнения других операций повторной инициализации*/
    {
        $this->connect();
    }
}	
echo '<hr>';
////////////////////////////////////////

//Пример сравнения объектов в PHP 5		
function bool2str($bool)
{
    return (string) $bool;
}
function compareObjects(&$o1, &$o2)
{
    echo 'o1 == o2 : ' . bool2str($o1 == $o2) . "<br>\n";
    echo 'o1 != o2 : ' . bool2str($o1 != $o2) . "<br>\n";
    echo 'o1 === o2 : ' . bool2str($o1 === $o2) . "<br>\n";
    echo 'o1 !== o2 : ' . bool2str($o1 !== $o2) . "<br>\n";
}
class Flag
{
    public $flag;

    function Flag1($flag = true) {
        $this->flag = $flag;
    }
}
class OtherFlag
{
    public $flag;

    function OtherFlag1($flag = true) {
        $this->flag = $flag;
    }
}
$o = new Flag();
$p = new Flag();
$q = $o;
$r = new OtherFlag();

echo "Два экземпляра одного и того же класса: <br>\n";
compareObjects($o, $p);
echo "\nДве ссылки на один и тот же экземпляр: <br>\n";
compareObjects($o, $q);
echo "\nЭкземпляры двух разных классов: <br>\n";
compareObjects($o, $r);

/*B PHP 5 объектная переменная больше не содержит сам объект как значение, а содержит только идентификатор объекта, который позволяет найти конкретный объект при обращении к нему. Когда объект передается как аргумент функции, возвращается или присваивается другой переменной, то эти разные переменные не являются псевдонимами (алиасами): они содержат копию идентификатора, который указывает на один и тот же объект.*/
$a = $o; // $a и $o копии одного идентификатора ($a) = ($o) = <id>
var_dump ($a);
echo '<br>';
$b = &$o;  // $o и $b ссылки ($o, $b) = <id>
var_dump ($b);
echo '<hr>';
?>