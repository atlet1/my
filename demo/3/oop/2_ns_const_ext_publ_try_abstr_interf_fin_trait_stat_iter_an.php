<?php
namespace NS;//{ і вкінці простору закрити якщо в одному файлі 2 простори - }

class ClassName {
    }
echo ClassName::class;
//ClassName::class можно получить строку с абсолютным именем класса ClassName. Обычно это довольно полезно при работе с классами c пространства имен. 
echo '<hr>';
	class MyClass {
		// Константа класса
		const MYCONSTANT = 'Значение константы должно быть неизменяемым выражением <br>';
//Пример констант Heredoc со статичными данными
		const BAR = <<<'EOT'
bar
EOT;
		public const MY_PUBLIC = 'public';//PHP 7.1.0
		protected const MY_PROTECTED = 'protected';//PHP 7.1.0
		private const MY_PRIVATE = 'private';//PHP 7.1.0
		// Определение свойств класса
		// Общедоступное свойство
		public $property = 1;
		// Защищенное свойство
		protected $protected = 2;
		// Частные свойства
		private $private = 3;	
		
		static $counter = 0;
		public $id;
		function __construct() {
			$this->id = ++self::$counter;//Доступ к статич свойст может быть получен с помощью :: 
		}
/* Объявление свойств и методов класса статическими позволяет обращаться к ним без создания экземпляра класса, псевдо-переменная $this не доступна внутри метода, объявленного статическим. Атрибут класса, объявленный статическим, не может быть доступен посредством экземпляра класса (но статический метод может быть вызван).*/		
		public static function statMethod() {
			echo "Статический метод<br>";	
		}
		final public function finMethod() {
			echo "<br>Финальный метод, нельзя перегрузить, а класс - унаследовать";	
		}
		function myMethod() {
			echo $this->property;	
			echo '<br>';
			echo self::MYCONSTANT;
			echo "<p> Класс - ", __CLASS__;
			echo "<p>Метод - ", __METHOD__;	
		}
		function myMethod2() {
			echo $this->property;	
			echo $this->protected;
			echo $this->private;
		}
		// Защищенный метод
		protected function myFunc() {
        echo "<br>ProtectedMyClass::myFunc()\n";
    }
		function printMyFunc() {
         $this->myFunc();
    }
		function iterateVisible() {//Итерация простого объекта
       echo "MyClass::iterateVisible:\n";
      foreach ($this as $key => $value) {
           print "$key => $value\n";
			}
		}
	}

	$classname = 'NS\MyClass';//5.3 возможным обратиться к классу с помощью переменной. В NAMESPACE вказувати повне і'мя класу
/*Оператор разрешения области видимости - :: позволяет обращаться к статическим свойствам, константам  (MyClass::statMethod()) и перегруженным свойствам или методам класса (parent, self, static::myMethod())	*/	
	MyClass::statMethod();//вызов статического метода
	$classname::statMethod();// Начиная с версии PHP 5.3.0.
	echo MyClass::MYCONSTANT;
	echo $classname::MYCONSTANT; // Начиная с версии PHP 5.3.0.
	echo MyClass::MY_PUBLIC; // Работает
	//MyClass::MY_PROTECTED; // Fatal Error
	//MyClass::MY_PRIVATE; // Fatal Error
	echo '<br>';
	
	$obj = new MyClass();//new $classname()

	echo $obj->property;// Работает
	//echo $obj->protected;// Неисправимая ошибка
	//echo $obj->private;//// Неисправимая ошибка
	echo '<br>';
	$obj->myMethod();// Выводит property, MYCONSTANT, Класс -, Метод -
	echo '<br>';
	$obj->myMethod2();// Выводит property, protected и private
	//$obj->myFunc();// Неисправимая ошибка
	$obj->printMyFunc();//Работает общий, защищенный и закрытый
	$obj->finMethod();
	
	$obj2 = new MyClass();
	$obj3 = new MyClass();
	
	echo "<p>Объект \$obj1 имеет id=", $obj->id;
	echo "<p>Объект \$obj2 имеет id=", $obj2->id;
	echo "<p>Объект \$obj3 имеет id=", $obj3->id;
	echo "<p>Всего объектов - ", MyClass::$counter;
	echo '<br>';
	
	//Итерация простого объекта, public если не в методе итерировать
	foreach($obj as $key => $value) {
    print "$key => $value\n";
}
echo "\n";
echo '<br>';
$obj->iterateVisible();
echo '<hr>';
	// Наследование
	class NewClass extends MyClass {
		public $new_property = 2;
		protected $protected = ', protected-2, ';

		public static $my_static = 'Статическая переменная (свойство, поле)';
		
		function __construct() {
			parent::__construct();
       print "Конструктор класса класса NewClass<br>\n"; 
		}
		public static function doubleColon() {
				echo "Статический метод<br>";
        echo parent::MYCONSTANT . "\n";
        echo self::$my_static . "\n";
    }
		function myMethod() {
			parent::myMethod();
			//echo $this->new_property;//якщо інший код, ніж батьків, то перезавантаження методів	
		}
		function myMethod2() {
			echo $this->new_property;	
			echo $this->protected;
			echo $this->private;
		}	
		// Перекрываем родительское определение
    public function myFunc()
    {
        // Но все еще вызываем родительскую функцию
        parent::myFunc();
        echo "<br>PublicNewClass::myFunc()\n";
    }
		function newMethod() {
			echo $this->new_property;	
			echo $this->private;
		}
	}
	
	NewClass::doubleColon();
	echo '<br>';
	$ext = new NewClass();
	echo $ext->new_property;// Работает
	//echo $ext->protected;// Неисправимая ошибка
	//echo $ext->private;// Неопределен
	echo '<br>';
	$ext->myMethod2();// Выводит 2, protected-2, Undefined
	echo '<br>';
	$ext->myMethod();
	$ext->myFunc();	
	echo '<br>';
	$ext->newMethod();
echo '<hr>';	
///////////////////////////////////////////////	
//позднее статическое связывание сохраняет имя класса указанного в последнем "не перенаправленном вызове"
	class A {
		function __construct(){
		echo __CLASS__;
		}		
    public static function who() {
        echo __CLASS__;
    }
    public static function test() {
        static::who(); // Здесь действует позднее статическое связывание.
// Было self::who() - результат был "А"
    }
}
class B extends A {
    public static function who() {
        echo __CLASS__;
    }
}
B::test(); //Результат выполнения примера: "B"
echo '<hr>';
//////////////////////////////////////////////	
	class MathException extends \Exception {// в NAMESPACE \з глобального
		function __construct($message) {
			parent::__construct($message);	
		}
	}	
	try {
		$a = 1;
		$b = 0;
		if ($b==0) throw new MathException("Деление на 0!");
		echo $a/$b;
	} catch (MathException $e) {
		echo "Произошла математическая ошибка: ", $e->getMessage(),
		" в строке ", $e->getLine(), 
		" файла ", $e->getFile();	
	} catch (Exception $e) {
		echo "Произошла неопознанная ошибка ", $e->getMessage(),
		" в строке ", $e->getLine(), 
		" файла ", $e->getFile();	
	}finally {// всегда будет выполняться после кода в блоках try и catch, вне зависимости было ли брошено исключение или нет, перед тем как продолжится нормальное выполнение кода 
    echo "<br>Finally\n";
}
echo '<hr>';
//////////////////////////////////////////////
abstract class AbstClass {//описательный смысл без реализации, черновик. Нельзя созд екз класса
// Данный метод должен быть определён в дочернем классе
		abstract function myMethod();
// Общий метод 
    public function printOut() {
        echo 'Общий метод';
    }
}
	class NewClass1 extends AbstClass {
		function myMethod() {
			echo 'Определёный метод';
		}	
	}
	$obj4 = new NewClass1();
	$obj4->myMethod();
	echo '<br>';
	$obj4->printOut();

abstract class Db{//ескіз, зразок
	public $cnn;
	function connect(){
		//
	}
	abstract function open($a);
	abstract function query();
	abstract function close();
}
class MyDb extends Db{
	function open($a){}
	function query(){}
	function close(){}
}
$db = new MyDb;	
echo '<hr>';
////////////////////////////////
	interface Int1 {// контракт, клас, где все методы абстрактные. Нельзя созд екз класса
		const b = 'Константа интерфейса<br>';// константы перекрывать нельзя
		function myMethod1(); 
	}
	interface Int2 {
		function myMethod2(); 
	}	
	echo Int1::b;
	interface Int3 extends Int1, Int2
	{
    public function foo();
	}
	
	class NewClass2 implements Int1,Int2 {//extends NewClass1 implements Int1 - можна і наслідувати і реалізовувати одночасно
		function myMethod1() {
			echo 1;
		}
		function myMethod2() {
			echo 2;
		}			
	}
	
	$obj5 = new NewClass2();
	$obj5->myMethod1();
	echo '<br>';
	$obj5->myMethod2();
echo '<hr>';
////////////////////////////////////
//Пример использования трейта
echo 'Трейт';
trait ezcReflectionReturnInfo {//типаж, риса, механизм обеспечения повторного использования кода в языках с поддержкой единого наследования. Нельзя созд екз класса
    function getReturnType() { /*1*/ }
    function getReturnDescription() { /*2*/ }
}

class ezcReflectionMethod extends \ReflectionMethod {// наследует глобальнй класс, поєтому \
    use ezcReflectionReturnInfo;
    /* ... */
}

class ezcReflectionFunction extends \ReflectionFunction {
    use ezcReflectionReturnInfo;
    /* ... */
}
echo '<hr>';
/////////////////////////////////////
//Анонимные классы  PHP 7+ очень полезны, когда надо создать простой, одноразовый объект
//Все объекты, созданные одной и той же декларацией анонимного класса, являются объектами одного класса
echo get_class(new class {}); 
echo '<hr>';
$ano_class_obj = new class{
    public $prop1 = 'hello';
    public $prop2 = 754;
    const SETT = 'some config<br>';
    public function getValue()
    {
        // do some operation
        return 'some returned value<br>';
    }
    public function getValueWithArgu($str)
    {
        // do some operation
        return 'returned value is '.$str;
    }
};
var_dump($ano_class_obj);
echo '<p>';
echo $ano_class_obj->prop1 . '<br>';
echo $ano_class_obj->prop2 . '<br>';
echo $ano_class_obj::SETT;
echo $ano_class_obj->getValue();
echo $ano_class_obj->getValueWithArgu('OOP');
echo '<hr>';
class SomeClass {}
interface SomeInterface {}
trait SomeTrait {}

var_dump(new class(10) extends SomeClass implements SomeInterface {
    private $num;

    public function __construct($num)
    {
        $this->num = $num;
    }
    use SomeTrait;
}); 
?>