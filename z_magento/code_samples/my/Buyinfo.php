<?php

namespace Study\First\Block;

/**
 * Class Buyinfo
 * @package Study\First\Block
 */

class Buyinfo extends \Magento\Framework\View\Element\Template
{
    protected $_registry;
    private $resourceModel;
    private $model;
    private $collection = null;
    protected $orderRepository;
    protected $modelFactory;
		private $repository;
    private $catalogSess;
		protected $request;
		
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Study\First\Model\FirstTest2 $model,
        \Study\First\Model\FirstTest2Factory $modelFactory,
        \Study\First\Model\ResourceModel\FirstTest2 $resourceModel,
        \Study\First\Model\ResourceModel\FirstTest2\Collection $collection,
        \Magento\Sales\Model\OrderRepository $orderRepository,
				\Study\First\Api\FirstTestRepositoryInterface $repository,
        \Magento\Catalog\Model\Session $catalogSess,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->resourceModel = $resourceModel;
        $this->model = $model;
        $this->collection = $collection;
        $this->orderRepository = $orderRepository;
        $this->modelFactory = $modelFactory;
				$this->repository = $repository;
        $this->catalogSess = $catalogSess;
        $this->request = $request;
        //$this->request = $context->getRequest();
        parent::__construct($context, $data);
    }
    public function getIddata()
    {
			//$this->request->getParams(); // all params
        //return $this->request->getPost();
        return $this->request->getParam('id');
    }
		public function getPriceProduct()
    {
        $this->catalogSess->setPriceProduct($this->_registry->registry('current_product')->getFinalPrice());
    }
    public function getOrderById($id)
    {
        return $this->orderRepository->get($id);
    }
		
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');// //('current_product')->getFinalPrice();
    }

    public function getById($entityId, $field)// //getById($entityId)
    {
        $this->resourceModel->load($this->model, $entityId, $field);// //load($this->model, $entityId);
        return $this->model;
    }// //echo $block->getById(1561, 'product_id')->getId() //echo $block->getById(1)->getProductId();
		
		    public function getFreeEventCollection()
    {
        return $this->collection;
    }

    public function getFilterCollection($product_id)
    {
        return $this->collection->addFieldToSelect('product_id')
            ->addFieldToFilter('product_id', ['eq' => $product_id])//['like' => "%$product_id%"]
            ->getData();
    }
		
		/*public function getFilterCollection()//Eventgrid.php - FirstTest\Collection
    {
        return $this->collection->addFieldToSelect('*')
            ->addFieldToFilter('id', ['eq' => 2])
            ->setOrder('created_at', 'desc')
            ->setPageSize(200)
            ->getData();
    }*/
		/*public function getFilterCollection()
    {
        return $this->collection->addFieldToSelect('id')
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('base_total_amount')
            ->addFieldToSelect('comment')
            ->addFieldToFilter('visibility', ['eq' => 1])
            ////->setPageSize(100)
            ->getData();
    }*/
		
    public function saveModel()
    {
        //$ar = implode(",", array_unique([1, 2, 2, 3, 4, 5, 5]));//implode(", "...пробіл є
        $ar = array_unique([1, 2, 2, 3, 4, 5, 5]);
        foreach ($ar as $value) {
            $mod = $this->modelFactory->create();
            $mod->setProductId($value)->setShippingEventId(5);
            $this->resourceModel->save($mod);
        //$this->resourceModel->save($this->modelFactory->create()->setProductId($value)->setShippingEventId(5));
            /*$this->model->setProductId($value)// //1561
                ->setShippingEventId(5);
            $this->resourceModel->save($this->model);
            $this->model->unsetData();//працює тільки при foreach, один - ні */
				/*$this->model->setData('product_id', '1561');
        $this->model->setData('shipping_event_id', '2');
        $this->resourceModel->save($this->model);	*/
        }
    }
		public function repo($id)
    {
        return $this->repository->getById($id);
    }
}
