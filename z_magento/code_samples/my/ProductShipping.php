<?php
namespace Study\First\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Checkout\Model\Session\Proxy as CheckoutSession;
use Magento\Framework\App\ResourceConnection;

class ProductShipping extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $checkoutSession;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    private $quote = null;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Catalog session
     *
     * @var \Magento\Catalog\Model\Session
     */
    protected $_catalogSession;

    private $model;
    private $resourceModel;
    private $resourceConnection;
    private $productModel;
    private $productResource;

    /**
     * @param \Magento\Checkout\Model\Session\Proxy $checkoutSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Model\Session $catalogSession
     * @param \Study\First\Model\FirstTest2 $model
     * @param \Study\First\Model\ResourceModel\FirstTest2 $resourceModel
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\Product $productModel
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param array $data
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Session $catalogSession,
        \Study\First\Model\FirstTest2 $model,
        \Study\First\Model\ResourceModel\FirstTest2 $resourceModel,
        ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        array $data = []
    ) {
        parent::__construct($data);
        $this->checkoutSession = $checkoutSession;
        $this->_scopeConfig = $scopeConfig;
        $this->_catalogSession = $catalogSession;
        $this->model = $model;
        $this->resourceModel = $resourceModel;
        $this->resourceConnection = $resourceConnection;
        $this->productModel = $productModel;
        $this->productResource = $productResource;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $freeShippingAmount = $this->_scopeConfig->getValue(
            'settingStudyFirst/general/quantity_items',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($freeShippingAmount) {
            $totals = $this->getQuote()->getTotals();
            $subtotalAmount = $totals['subtotal']->getValue();
            $amountleft = $freeShippingAmount - $subtotalAmount;

            if ($this->_scopeConfig->getValue(
                'settingStudyFirst/general/first_status',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )) {
                $freeshipp = true;// //select()->distinct()->from
								$select1 = $this->resourceConnection->getConnection()->select()
                    ->from('catalog_product_flat_1', 'entity_id')
                    ->where('url_key = ?', $this->helperData->getProdId());
                $currentProductId = $this->resourceConnection->getConnection()->fetchOne($select1);
								//$currentProductId = $this->helperData->getProdId();
								//$currentProductId = 1561;
                //$currentProductId = int($_SESSION['current_product_id']);
								//$currentProductId = $_SERVER['HTTP_REFERER']//на секцію переходить звідси
								//$currentProductId = $this->toJson($_REQUEST);//$_SERVER ...
                //$currentProductId = $this->_catalogSession->getLastViewedProductId();
                //$currentProductId = $this->_catalogSession->getCurrentProductId();
                $currentProductPrice = $this->getProductById($currentProductId, 'price')->getFinalPrice();
            } else {
                $freeshipp = false;
                $currentProductId = 1;
                $currentProductPrice = 1;
            }

            if ($currentProductPrice < $amountleft) {
                $product_count = __('buy ') .
                    ceil($amountleft / $currentProductPrice) . __(' products in get free shipping');
            } else {
                if ($subtotalAmount > $freeShippingAmount) :
                    $product_count = __('Freeshipping Available: '). '<a href="/checkout/">Proceed to Checkout</a>';
                else :
                    $product_count = __('buy 1 product in get free shipping');
                endif;
            }

            if ($currentProductId) {
							 /*$select = $this->resourceConnection->getConnection()->select()->from('custom_index', 'count')
                    ->where('product_id = ?', $this->getById($currentProductId, 'product_id')->getProductId());
                $product_buyornot = $this->resourceConnection->getConnection()->fetchOne($select) .*/
               $select = $this->resourceConnection->getConnection()->select()
                    ->from('shipping_event_item', 'product_id')
                    ->where('product_id = ?', $this->getById($currentProductId, 'product_id')->getProductId());
                $product_buyornot = count($this->resourceConnection->getConnection()->fetchAll($select)) .
                __(' qty this product was buy for free shipping');
            } else {
                $product_buyornot =  __('this product was not buy for free shipping');
            }

            return [
                'freeshipp' => $freeshipp,
                'product_count' => $product_count,
                'product_buyornot' => $product_buyornot,
            ];
        }
        return false;
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    private function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    public function getById($entityId, $field)
    {
        $this->resourceModel->load($this->model, $entityId, $field);
        return $this->model;
    }
    public function getProductById($entityId, $field)
    {
        $this->productResource->load($this->productModel, $entityId, $field);
        return $this->productModel;
    }
}
