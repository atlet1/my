<?php

namespace Yura\Blog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

////use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'blog_category'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('blog_category')
        )->addColumn(
            'category_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
            'Category ID'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Category Name'
        )->addColumn(
            'status',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'unsigned' => true, 'default' => '1'],
            'Category Enabled'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            '64k',
            [],
            'Category Description'
        )->addColumn(
            'url_key',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Category URL Key'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'blog_post'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('blog_post')
        )->addColumn(
            'post_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
            'Post ID'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Post Name'
        )->addColumn(
            'image',
            Table::TYPE_TEXT,
            255,
            [],
            'Post Image'
        )->addColumn(
            'status',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'unsigned' => true, 'default' => '1'],
            'Post Enabled'
        )->addColumn(
            'short_description',
            Table::TYPE_TEXT,
            '64k',
            [],
            'Post Short Description'
        )->addColumn(
            'post_content',
            Table::TYPE_TEXT,
            '64k',
            [],
            'Post Content'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Post Created At'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Post Updated At'
        )->addColumn(
            'author',
            Table::TYPE_TEXT,
            255,
            [],
            'Post Author'
        )->addColumn(
            'cid',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'unsigned' => true],
            'Category ID'
        )->addColumn(
            'url_key',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Post URL Key'
        /*)->addIndex(
       $installer->getIdxName('blog_post', ['name']),
       ['name']
   )->addIndex(
       $installer->getIdxName('blog_post', ['name'], AdapterInterface::INDEX_TYPE_UNIQUE),
       ['name'],
       ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]*/
        )->addForeignKey(
            $installer->getFkName('blog_post', 'cid', 'blog_category', 'category_id'),
            'cid',
            $installer->getTable('blog_category'),
            'category_id',
            Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'blog_likes'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('blog_likes')
        )->addColumn(
            'pid_like',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Post ID'
        )->addColumn(
            'like',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Like'
        )->addColumn(
            'dislike',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Dislike'
        )->addForeignKey(
            $installer->getFkName('blog_likes', 'pid_like', 'blog_post', 'post_id'),
            'pid_like',
            $installer->getTable('blog_post'),
            'post_id',
            Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'blog_likes_user'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('blog_likes_user')
        )->addColumn(
            'pid_user',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Post ID'
        )->addColumn(
            'user_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'User ID'
        )->addForeignKey(
            $installer->getFkName('blog_likes_user', 'pid_user', 'blog_post', 'post_id'),
            'pid_user',
            $installer->getTable('blog_post'),
            'post_id',
            Table::ACTION_CASCADE
        /*)->addIndex(
        $installer->getIdxName('blog_user_likes', ['user_id']),
        ['user_id']*/
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
