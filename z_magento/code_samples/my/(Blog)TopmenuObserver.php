<?php

namespace Yura\Blog\Observer;

use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;

class TopmenuObserver implements ObserverInterface
{
    const NODE_ID = 'yura';

    protected $nodeFactory;

    protected $request;
    
    public function __construct(
        NodeFactory $nodeFactory,
        Http $request
    ) {
        $this->nodeFactory = $nodeFactory;
        $this->request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //$fullAction = $this->request->getFullActionName();
        //$selectedActions = ['blog_post_index', 'blog_category_index', 'blog_post_view', 'blog_category_view'];
        $identifier = trim($this->request->getPathInfo(), '/');
        $pathInfo = explode('/', $identifier);

        $menu = $observer->getMenu();
        $node = $this->nodeFactory->create([
            'data' => [
                'name' => 'Blog',
                'url' => '/blog',
                //'has_active' => ($pathInfo[0] === 'blog'),
                'is_active' => ($pathInfo[0] == 'blog'),
                //'is_active' => in_array($fullAction, $selectedActions)
            ],
            'idField' => static::NODE_ID,
            'tree' => $menu->getTree(),
            'parent' => $menu
        ]);
        $menu->addChild($node);

        return $this;
    }
}
