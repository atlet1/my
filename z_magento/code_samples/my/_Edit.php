<?php
namespace Study\First\Controller\Adminhtml\Study;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Study\First\Api\TestRepositoryInterface;

class Edit extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Study_First::ui_components';

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var TestRepositoryInterface
     */
    private $repository;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param TestRepositoryInterface $repository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        TestRepositoryInterface $repository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->repository = $repository;
        parent::__construct($context);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Study_First::ui_components');

        if ($id) {
            try {
                $item = $this->repository->getById($id);
                $resultPage->getConfig()->getTitle()->prepend($item->getId());
            } catch (\Exception $e) {
                $resultRedirect = $this->resultRedirectFactory->create();
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Item'));
        }

        return $resultPage;
    }
}
