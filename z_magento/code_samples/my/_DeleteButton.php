<?php
namespace Study\First\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Backend\Block\Widget\Context;
use Study\First\Api\TestRepositoryInterface;

class DeleteButton implements ButtonProviderInterface
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var TestRepositoryInterface
     */
    protected $repository;

    /**
     * @param Context $context
     * @param TestRepositoryInterface $repository
     */
    public function __construct(
        Context $context,
        TestRepositoryInterface $repository
    ) {
        $this->context = $context;
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($id = $this->getItemId()) {
            $data = [
                'label'      => __('Delete'),
                'class'      => 'delete',
                'on_click'   => 'deleteConfirm(\'' . __(
                    'Are you sure you want to delete this item?'
                ) . '\', \'' . $this->getDeleteUrl($id) . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * Get delete button url
     *
     * @param int $id
     * @return string
     */
    public function getDeleteUrl($id)
    {
        return $this->context->getUrlBuilder()->getUrl('*/*/delete', ['id' => $id]);
    }

    /**
     * Get item ID
     *
     * @return int|null
     */
    protected function getItemId()
    {
        try {
            return $this->repository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (\Exception $e) {
            return null;
        }
    }
}
