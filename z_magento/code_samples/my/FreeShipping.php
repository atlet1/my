<?php
namespace Study\First\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Checkout\Model\Session\Proxy as CheckoutSession;

class FreeShipping extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $checkoutSession;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    private $quote = null;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    private $checkoutHelper;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    private $catalogSess;

    private $cookie;

    private $model;
    private $resourceModel;

    /**
     * @param \Magento\Checkout\Model\Session\Proxy $checkoutSession
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Model\Session $catalogSess
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookie
     * @param \Study\First\Model\FirstTest2 $model
     * @param \Study\First\Model\ResourceModel\FirstTest2 $resourceModel
     * @param array $data
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Session $catalogSess,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookie,
        \Study\First\Model\FirstTest2 $model,
        \Study\First\Model\ResourceModel\FirstTest2 $resourceModel,
        array $data = []
    ) {
        parent::__construct($data);
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->catalogSess = $catalogSess;
        $this->cookie = $cookie;
        $this->model = $model;
        $this->resourceModel = $resourceModel;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()//Функція яка використовується для передачі даних в секцію
    {
        $freeShippingAmount = $this->_scopeConfig->getValue(
            'settingStudyFirst/general/quantity_items',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $totals             = $this->getQuote()->getTotals();
        $subtotalAmount     = $totals['subtotal']->getValue();
        $hasFreeshipping    = $subtotalAmount >= $freeShippingAmount;
        $this->checkoutSession->setFreeSippingStatus($hasFreeshipping);

        $currentProductId = $this->catalogSess->getCurrentProductId();
        $currentProductPrice = $this->catalogSess->getPriceProduct();
        // далі можна використовувати даний id продукту щоб отримати потрібні дані

        return [ //повертаємо масив який буде серелізований в json і поміщений в об'єкт CostumerData
            'has_freeshipping' => $hasFreeshipping,
            'amount_left' => $this->checkoutHelper->formatPrice($freeShippingAmount - $subtotalAmount),
            'product_id' => $currentProductId,
            // для прикладу передаю значення id продукту в javascript файл який відповідає за поточну секцію
            'product_price' => $currentProductPrice,
        ];
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    private function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    public function getById($entityId, $field)// //getById($entityId)
    {
        $this->resourceModel->load($this->model, $entityId, $field);
        return $this->model;
    }// //echo $block->getById(1561, 'product_id')->getId() //echo $block->getById(1)->getProductId();
}
