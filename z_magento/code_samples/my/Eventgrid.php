<?php

namespace Study\First\Block;

/**
 * Class Eventgrid
 * @package Study\First\Block
 */

class Eventgrid extends \Magento\Framework\View\Element\Template
{
    private $collection = null;
    private $collection2 = null;
    private $resourceConnection;
    private $context;

    /**
     * TestBlock constructor.
     * @param \Study\First\Model\ResourceModel\FirstTest\Collection $collection
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Study\First\Model\ResourceModel\FirstTest\Collection $collection,
        \Study\First\Model\ResourceModel\FirstTest2\Collection $collection2,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\View\Element\Template\Context $context,
        $data = []
    ) {
        parent::__construct($context, $data);
        $this->collection = $collection;
        $this->collection2 = $collection2;
        $this->resourceConnection = $resourceConnection;
        $this->context = $context;
    }

    public function getFilterCollection()
    {
        return $this->collection->addFieldToSelect('id')//order_id
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('base_total_amount')
            ->addFieldToSelect('comment')
            ->addFieldToFilter('visibility', ['eq' => 1])
            ////->setPageSize(100)
						////->setCurPage(1)// або номер передати як аргумент
            ->getData();
    }
		
    public function getProductFilterCollection($id)
    {
        $entity_id = $this->collection2->addFieldToSelect('product_id')
            ->addFieldToFilter('shipping_event_id', ['eq' => $id])
            ->getData();
        $select = $this->resourceConnection->getConnection()->select()
            ->from('catalog_product_flat_1', 'name')
            ->where('entity_id IN (?)', $entity_id);
        return $this->resourceConnection->getConnection()->fetchAll($select);
    }
		
    public function getGetId()
    {
        ////$this->context->getUrlBuilder()->getRedirectUrl($this->context->getUrlBuilder()->getCurrentUrl());
        return (int)$this->context->getRequest()->getParam('id');
    }
}
