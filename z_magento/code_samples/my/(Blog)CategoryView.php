<?php

namespace Yura\Blog\Block;

/**
 * Class CategoryView
 * @package Yura\Blog\Block
 */

class CategoryView extends \Magento\Framework\View\Element\Template
{
    private $repository;
    private $collection = null;
    private $collection2 = null;
    private $resourceConnection;
    private $context;
    private $model;

    /**
     * TestBlock constructor.
     * @param \Yura\Blog\Model\CategoryRepository $repository
     * @param \Yura\Blog\Model\ResourceModel\Category\Collection $collection
     * @param \Yura\Blog\Model\ResourceModel\Post\Collection $collection2
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Yura\Blog\Api\Data\CategoryInterface $model
     * @param array $data
     */
    public function __construct(
        \Yura\Blog\Model\CategoryRepository $repository,
        \Yura\Blog\Model\ResourceModel\Category\Collection $collection,
        \Yura\Blog\Model\ResourceModel\Post\Collection $collection2,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\View\Element\Template\Context $context,
        \Yura\Blog\Api\Data\CategoryInterface $model,
        $data = []
    ) {
        parent::__construct($context, $data);
        $this->repository = $repository;
        $this->collection = $collection;
        $this->collection2 = $collection2;
        $this->resourceConnection = $resourceConnection;
        $this->context = $context;
        $this->model = $model;
    }
		
    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
				/*if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb(
                'home',
                [
                    'title' => __('Home'),
                    'label' => __('Home'),
                    'link' => $this->getUrl('')
                ]
            );
						$breadcrumbs->addCrumb(
						    'category',
                [
                    'label' => __('Category'),
                    'title' => __('Category')
                ]
            );
        }*/
        if ($this->getPostFilterCollection()) {
            $pager = $this->getLayout()->createBlock(//work with empty name (alternativ - addBlock)
                \Magento\Theme\Block\Html\Pager::class
            )->setCollection(
                $this->getPostFilterCollection()
            );
            $this->setChild('pager', $pager);
            $this->getPostFilterCollection()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getCatFilterCollection()
    {
        return $this->collection->addFieldToSelect('category_id')
            ->addFieldToSelect('name')
            ->addFieldToSelect('url_key')
            ->addFieldToFilter('status', 1)
            ->setOrder('name', 'desc')
            ->getData();
    }

 /**
     * Retrieve post collection
     *
     * @return \Yura\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPostFilterCollection()
    {
        return $this->collection2->addFieldToSelect('post_id')
            ->addFieldToSelect('name')
            ->addFieldToSelect('image')
            ->addFieldToSelect('short_description')
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('author')
            ->addFieldToSelect('url_key')
            ->addFieldToFilter('cid', $this->getCategoryParam())
            ->addFieldToFilter('status', 1)
            ->setOrder('created_at', 'desc')
            ->setPageSize(5)
            ->setCurPage($this->getRequest()->getParam('p', 1));//blog/test-category?p=1
            // //->getSize();//count items
            // //->getData();//array
           // //->load();//load object, may do foreach it
    }

    public function getCategoryParam()//getCategoryId()
    {
        return (int)$this->context->getRequest()->getParam('cid');
    }

    public function getCategoryById($id)//categoryById($id)
    {
        try {
            return $this->repository->getById($id);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function categoryField($id = 'Test category', $field = 'name')
    {
        try { ////echo $block->categoryField()->getUrlKey()
            return $this->repository->getByIdField($id, $field);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getProductFilterCollection($id)
    {
        $entity_id = $this->collection2->addFieldToSelect('product_id')
            ->addFieldToFilter('shipping_event_id', ['eq' => $id])
            ->getData();
        $select = $this->resourceConnection->getConnection()->select()
            ->from('catalog_product_flat_1', 'name')
            ->where('entity_id IN (?)', $entity_id);
        return $this->resourceConnection->getConnection()->fetchAll($select);
    }

    public function saveModel()
    {
        $mod = $this->model
            ->setName('Name')
            ->setDescription('Description')
            ->setUrlKey('Url key');
        $this->repository->save($mod);
    }
}
