<?php

namespace Study\First\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const XML_PATH_STUDYFIRST = 'settingStudyFirst';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, //path: //'settingStudyFirst/general/(first_status = $code)'
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_STUDYFIRST .'/general/'. $code, $storeId);
    }

    public function getProdId()
    {
			  ////$this->_urlBuilder->getRedirectUrl($this->_urlBuilder->getCurrentUrl());
        $filter = [$this->_urlBuilder->getBaseUrl() => '', '.html' => ''];
        $prod_name = strtr($this->_httpHeader->getHttpReferer(), $filter);
        return $prod_name;
    }
		
		public function getProdId()
    {
        return $this->_request->getParam('id');
    }
}
