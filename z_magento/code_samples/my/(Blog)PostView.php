<?php

namespace Yura\Blog\Block;

use Magento\Framework\Session\SessionManagerInterface as CoreSession;

/**
 * Class PostView
 * @package Yura\Blog\Block
 */

class PostView extends \Magento\Framework\View\Element\Template
{
    private $repository;
    private $repository2;
    private $context;
    protected $_coreSession;
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * PostView constructor.
     * @param \Yura\Blog\Model\CategoryRepository $repository
     * @param \Yura\Blog\Model\PostRepository $repository2
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext,
     * @param CoreSession $coreSession,
     * @param array $data
     */
    public function __construct(
        \Yura\Blog\Model\CategoryRepository $repository,
        \Yura\Blog\Model\PostRepository $repository2,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        CoreSession $coreSession,
        $data = []
    ) {
        parent::__construct($context, $data);
        $this->repository = $repository;
        $this->repository2 = $repository2;
        $this->context = $context;
        $this->httpContext = $httpContext;
        $this->_coreSession = $coreSession;
    }

    public function init()
    {
        $data = [
            'likesdata'=>[
                'component'    => 'Yura_Blog/js/likes',
            ]
        ];
        return json_encode($data);
    }

    public function getPostById($id)
    {
        try {
            return $this->repository2->getById($id);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getPostParam()
    {
        return (int)$this->context->getRequest()->getParam('pid');
    }

    public function getCategoryName($id, $field = 'category_id')
    {
        try {
            return $this->repository->getByIdField($id, $field);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function customerLoggedIn()
    {
        return (bool)$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    public function isViewLikes()
    {
        if ($this->session->isLoggedIn()) {
            $resourceConnection = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection')->getConnection();
            $select = $resourceConnection->select()
                ->from('blog_likes_user', 'user_id')
                ->where('pid_user = ?', $this->getPostParam());
            $has_like = (int)$resourceConnection->fetchOne($select);
            if ($this->session->getCustomerId() != $has_like) {
                return true;
            }
        }
        return false;
    }

		public function isViewLikes()
    {
            $resourceConnection = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection')->getConnection();
            $select = $resourceConnection->select()
                ->from('blog_likes_user', 'user_id')
                ->where('pid_user = ?', $this->getPostParam());
            $has_like = (int)$resourceConnection->fetchOne($select);
            $customerId = $this->coreSessCustomerId();
        if ($customerId != $has_like) {
            return true;
        }
        return false;
    }

    private function coreSessCustomerId()
    {
        $this->_coreSession->start();
        return (int)$this->_coreSession->getData()['visitor_data']['customer_id'];
    }
		
		public function customerLogIn()
    {
        //return $_SESSION ['default']['visitor_data']['do_customer_login'];//boolean
        return $_SESSION ['default']['visitor_data']['customer_id'];//string
    }
}
