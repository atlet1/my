define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/model/customer'
], function (Component, customerData, customer) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Yura_Blog/likes'
        },
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.likesData = customerData.get('likes');
            customerData.reload(['likes'], true);
        },
        isCustomerLoggedIn: customer.isLoggedIn
    });
});
