<?php

namespace Study\First\Block;

use Magento\Store\Model\ScopeInterface;

/**
 * Class TestBlock
 * @package Study\First\Block
 */

class TestBlock extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;
		
		/**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
		
    /**
     * Construct
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $scopeConfig;
        $this->_currency = $currency;
        $this->_storeManager = $storeManager;
    }
    /**
     * Get  minimum shipping amount
     *
     * @return mixed
     */
    public function getMinimumShippingAmount()
    {
        return $this->_scopeConfig->getValue(
				    'settingStudyFirst/general/quantity_items',
            //'settingShippingEvent/general/minimum_shipping_amount',
            ScopeInterface::SCOPE_STORE////без юзе \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }////echo $this->getMinimumShippingAmount() . '<br />';
 
    /**
     * Get currency symbol for current locale and currency code
     *
     * @return string
     */
    public function getCurrentCurrencySymbol()
    {
        return $this->_currency->getCurrencySymbol();
    }////echo $block->getCurrentCurrencySymbol() . '<br />';
   /**
     * Get currency store
     *
     * @return string
     */
    public function getCurrencyNow()
    {
        return $this->_currency;////заглушка
        ////return $this->_storeManager->getStore()->getBaseCurrency()->getRate('USD');
        ////return $this->_storeManager->getStore()->getCurrentCurrencyCode();
        ////return $this->_storeManager->getStore()->getCurrentCurrencyRate();
        ////$this->currency_s->getStore()->getBaseCurrency()->getCurrencySymbol();
        ////$this->currency_s->getStore()->getCurrencySymbol();

    ////$currency_s = $_objectManager->create('Magento\Directory\Model\CurrencyFactory')->create()->load($currencyCode);
        ////$currencySymbol = $currency->getCurrencySymbol();
    }
    /**
     * @return string
     */
    public function getNameBlock()
    {
        return "is TestBlock";
    }
}
