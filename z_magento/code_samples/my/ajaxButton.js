define([
        'jquery',
        'uiComponent',
        'ko'
    ],
    function ($, Component, ko) {
        return Component.extend({

            initialize: function () {
                this._super();
                this.clickEvent.subscribe(this.getCurrency.bind(this));
            },

            clickEvent: ko.observable(false),
            currencyData: ko.observable('...loading'),
            getCurrency: function () {
                if (this.clickEvent()) {
                    $.getJSON(this.ajaxUrl + 'rest/V1/directory/currency', function (result) {
											  this.ordersData(result);
                        //this.currencyData(JSON.stringify(result, null, 2));
												//alert(JSON.stringify(result, null, 2));
                    }.bind(this));
                }
            }
        })
    }
);

<!-- не працює
<!--<button id="like" class="vbtn-like" type="button" data-bind="click: loadAjaxData">-->
define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Yura_Blog/likes'
        },
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.likesData = customerData.get('likes');
            customerData.reload(['likes'], true);
        },
        loadAjaxData: function () {
            var postId = 1;
            $("#post-like").hide();
            var like = 1;
            var dislike = 0;
            if($(this).hasClass('vbtn-like')){
                $("#mark").css('background-color', 'orange');
                like = 1;
            }else if($(this).hasClass('vbtn-disklike')){
                $("#mark1").css('background-color', 'orange');
                dislike = 1;
            }
            return $.ajax({
                type: "POST",
                url:this.ajaxUrl + 'blog/index/likes',
                data: { like:like, dislike:dislike, postId:postId },
                dataType: "json"
            }).done(function (data) {
                console.log(data.like + ' : ' + data.dislike );
            });
        }
    });
});
-----------------------------------
define([
        'jquery',
        'uiComponent',
        'ko'
    ],
    function ($, Component, ko) {
        return Component.extend({

            initialize: function () {
                this._super();
                this.clickEvent.subscribe(this.getLikes.bind(this));
            },

            clickEvent: ko.observable(false),
            likesData: ko.observable('...loading'),
            getLikes: function () {
                if (this.clickEvent()) {
            $.ajax({
            type: "POST",
            url:this.ajaxUrl + 'blog/index/likes',
            data: { postId:this.postId },
            dataType: "json"
        })
            .done(function(result) {
				  this.likesData(result);

                    }.bind(this));
                }
            }
        })
    }
);
 -->