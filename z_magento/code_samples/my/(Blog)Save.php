<?php
namespace Yura\Blog\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Yura\Blog\Api\PostRepositoryInterface;
use Yura\Blog\Model\PostFactory;

class Save extends Action
{
    /**
     * Authorization level of current admin session
     */
    const ADMIN_RESOURCE = 'Yura_Blog::post';

    /**
     * @var PostRepositoryInterface
     */
    private $repository;

    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @param Context $context
     * @param PostRepositoryInterface $repository
     * @param PostFactory $postFactory
     */
    public function __construct(
        Context $context,
        PostRepositoryInterface $repository,
        PostFactory $postFactory
    ) {
        $this->repository = $repository;
        $this->postFactory = $postFactory;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request        = $this->getRequest();
        $data           = $request->getPostValue();

        if ($data) {
            $id = $request->getParam('id');

            if (empty($data['post_id'])) {
                $data['post_id'] = null;
            }

            if ($id) {
                try {
                    $item = $this->repository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                    return $resultRedirect->setPath('*/*/', ['id' => $id]);
                }
            } else {
                $item = $this->postFactory->create();
            }

            if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
                // //$data['image'] = $data['image'][0]['name'];
                $image = $data['image'][0]['name'];
                /*$this->imageUploader = \Magento\Framework\App\ObjectManager::getInstance()->get(
                    'Yura\Blog\ImageUpload'
                );*/
                $imageUploader = $this->_objectManager->get(\Yura\Blog\ImageUpload::class);
                // else //$data['image'] = $data['image'][0]['image'];
                $imageUploader->moveFileFromTmp($image);// //($data['image'])
                unset($data['image']);
                //$item->setData('image', 'blog_post' . '/' . $image);// не працює
                $data['image'] = 'blog/post' . '/' . $image;
            }

            $item->setData($data);

            try {
                $this->repository->save($item);
                $this->messageManager->addSuccessMessage(__('Item was saved successfully.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving test item.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
