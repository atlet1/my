<?php

namespace Study\First\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Reload extends \Magento\Framework\App\Action\Action
{
    private function getFiltColl()
    {
           return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Study\First\Model\ResourceModel\FirstTest\Collection')
            ->addFieldToSelect('id')
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('base_total_amount')
            ->addFieldToSelect('comment')
            ->addFieldToFilter('visibility', ['eq' => 1])
            ->getData();
    }
    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /*$responseContent = [
            'success' => 'success',
            'error_message' => __('An error occurred while loading this tax rate.'),
        ];*/
        /*$responseData = [];
        foreach ($this->getFiltColl() as $resultItem) {
            $responseData[] = $resultItem;
        }*/
        // /** @var \Magento\Framework\View\Result\Layout $resultLayout */
         /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($this->getFiltColl());
        //$resultJson->setData($this->_url->getCurrentUrl());
        //$resultLayout->getLayout()->getBlock('freeshippingevents');
        return $resultJson;
    }
}
