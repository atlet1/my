<?php
namespace Yura\Blog\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Framework\App\ResourceConnection;
use Yura\Blog\Helper\Data;
use Magento\Customer\Model\Session;

class Likes extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    private $resourceConnection;
    protected $helperData;
    protected $_customerSession;

    /**
     * @param ResourceConnection $resourceConnection
     * @param Data $helperData
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        Data $helperData,
        Session $customerSession,
        array $data = []
    ) {
        parent::__construct($data);
        $this->resourceConnection = $resourceConnection;
        $this->helperData = $helperData;
        $this->_customerSession = $customerSession;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        if ($this->helperData->getGeneralConfig('blog_status')) {
            $has_like = true;

            $post_key = $this->resourceConnection->getConnection()->select()
                ->from('blog_post', 'post_id')
                ->where('url_key = ?', $this->helperData->getPostKey());
            $postId = (int)$this->resourceConnection->getConnection()->fetchOne($post_key);

            $select = $this->resourceConnection->getConnection()->select()
                ->from('blog_likes')
                ->where('pid_like = ?', $postId);
            $currentLikes = $this->resourceConnection->getConnection()->fetchRow($select);

            $like = (int)$currentLikes['like'];
            $dislike = (int)$currentLikes['dislike'];

            return [
                'has_like' => $has_like,
                'like' => $like,
                'dislike' => $dislike,
                'vote' => $this->isViewLikes($postId),
            ];
        }
        return false;
    }

    private function isViewLikes($postId)
    {
        $select1 = $this->resourceConnection->getConnection()->select()
            ->from('blog_likes_user', 'user_id')
            ->where('pid_user = ?', $postId);
        if ($this->_customerSession->getCustomerId() != (int)$this->resourceConnection
                ->getConnection()->fetchOne($select1)) {
            $vote = 1;
        } else {
            $vote = 0;
        }
        return $vote;
    }
}
