<?php

namespace Yura\Blog\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Likes extends \Magento\Framework\App\Action\Action
{
    private function saveLikes($postId, $likeId = null, $dislikeId = null)
    {
        $resourceConnection = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection')->getConnection();
        if (!$this->getLikes($postId)) {
            if ($likeId) :
                $bind = [
                    'pid_like' => $postId,
                    'like' => 1,
                ];
                $resourceConnection->insert('blog_likes', $bind);
            elseif ($dislikeId) :
                $bind = [
                    'pid_like' => $postId,
                    'dislike' => 1,
                ];
                $resourceConnection->insert('blog_likes', $bind);
            endif;
        } elseif ($likeId) {
                $bind = ['like' =>  (int)$this->getLikes($postId)['like'] + 1];
                $where = ['pid_like = ?' => $postId];
                $resourceConnection->update('blog_likes', $bind, $where);
        } elseif ($dislikeId) {
                $bind = ['dislike' => (int)$this->getLikes($postId)['dislike'] + 1];
                $where = ['pid_like = ?' => $postId];
                $resourceConnection->update('blog_likes', $bind, $where);
        }
        return true;
    }

    private function getLikes($postId)
    {
        $resourceConnection = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection')->getConnection();
        $select = $resourceConnection->select()
               ->from('blog_likes')
               ->where('pid_like = ?', $postId);
        $currentLikes = $resourceConnection->fetchRow($select);
        return $currentLikes;
    }

    private function custumerSetLikes($postId)
    {
        $sessionUserId = (int)\Magento\Framework\App\ObjectManager::getInstance()
           ->get('Magento\Customer\Model\Session')->getCustomerId();
        if (!$sessionUserId) {
            return false;
        }
        $resourceConnection = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection')->getConnection();
        $bind = [
            'pid_user' => $postId,
            'user_id' => $sessionUserId,
        ];
        $resourceConnection->insert('blog_likes_user', $bind);
        return true;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            // //$post = $this->getRequest()->getPostValue();
            // //$postId = (int)$this->getRequest()->getParam('cid');// не приходить
            $postId = (int)$this->getRequest()->getPost('postId');

            $likeId = (int)$this->getRequest()->getPost('like');
            $dislikeId = (int)$this->getRequest()->getPost('dislike');

            if ($likeId || $dislikeId) {
                $this->saveLikes($postId, $likeId, $dislikeId);
                $this->custumerSetLikes($postId);
            }
        }
         /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($this->getLikes($postId));
        return $resultJson;
    }
}
