<?php

namespace Study\First\Observer;

use Magento\Framework\Event\ObserverInterface;

class TestObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $minshipam = $this->scopeConfig->getValue(
            'settingStudyFirst/general/quantity_items',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($order->getSubtotal() >= $minshipam) {
            try {
                $this->model
                ->setTotalAmount($minshipam)
                ->setOrderId($order->getIncrementId());
                $this->resourceModel->save($this->model);

                /*$orderItems = [];
                foreach ($order->getAllVisibleItems() as $item) {
                    $orderItems[] = $item->getProductId();
                }
                $orderItems = implode(",", array_unique($orderItems));
                $mod = $this->modelFactory->create();
                $mod->setProductId($orderItems)
                    ->setShippingEventId($this->model->getId());
                $this->resourceModel2->save($mod);*/
								
								foreach ($order->getAllVisibleItems() as $item) {
                    $mod = $this->modelFactory->create();
                    $mod->setProductId($item->getProductId())
                    ->setShippingEventId($this->model->getId());//model->getOrderId()
                    $this->resourceModel2->save($mod);
                }
            } catch (\Exception $e) {
                $e->getMessage();
            }
        }
				
				/*$order->getId();
        $order->getTotalItemCount();
        $order->getItems();// // array //$item->getId(); $item->getProductId(); $item->getQtyOrdered();
        $order->getSubtotal();

        foreach ($order->getItems() as $orderItem) {
            $orderItem->getId();
            $orderItem->getProductId();
        }

        // спробувати цикл while чи for для запису кількох моделей

        $modelData = [
            'order_id' => $order->getId(),// //'order_id' => $order->getIncrementId(),
            'customer_id' => $order->getCustomerId(),
            'total' => $order->getGrandTotal(),
            'total_base' => $order->getBaseGrandTotal(),
            'item_count' => $itemCount,
        ];
        $orderModel = $this->ordersFactory->create();
        $orderModel->setData($modelData);
        $orderModel->save();

        try {
            $automation = $this->automationFactory->create()
                ->setEmail($email)
                ->setAutomationType($automationType)
                ->setTypeId($order->getIncrementId())
                ->setWebsiteId($website->getId())
                ->setStoreName($storeName)
                ->setProgramId($programId);
            $this->automationResource->save($automation);
        } catch (\Exception $e) {
            $e->getMessage();
        }
        return $this;*/
    }
}
