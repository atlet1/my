<?php
namespace Study\First\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        $table = $installer->getConnection()->getTableName('test_table');
        $data = [
            [
                'name' => "Noah",
            ],
            [
                'name' => "Jacob",
            ],
            [
                'name' => "Mason",
            ],
            [
                'name' => "William",
            ]
        ];

        $installer->getConnection()->insertMultiple($table, $data);
    }
}
