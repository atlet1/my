<?php
namespace Study\First\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class AppHttpLaunch
{
    /**
     * Catalog session
     *
     * @var \Magento\Catalog\Model\Session
     */
    protected $_catalogSession;

    private $urlFinder;

    public function __construct(
        \Magento\Catalog\Model\Session $catalogSession,
        UrlFinderInterface $urlFinder
    ) {
        $this->_catalogSession = $catalogSession;
        $this->urlFinder = $urlFinder;
    }

    public function beforeDispatch($subject, RequestInterface $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $rewriteData = $this->urlFinder
                ->findOneByData(
                    [UrlRewrite::REQUEST_PATH => ltrim($request->getRequestString(), '/'),]
                );

            if ($rewriteData) {
                $data = $rewriteData->toArray();

                if ($data && isset($data['entity_type']) && $data['entity_type'] == 'product') {
                    $productId = isset($data['entity_id']) ? $data['entity_id'] : null;

                    if ($productId) {
											  //$_SESSION['current_product_id'] = $productId;
												//$_CUSTOM = ['USERNAME' => 'john', 'USERID' => '18068416846']; // Custom global variable $_CUSTOM
                        $this->_catalogSession->setCurrentProductId($productId);
                        return [$request];
                    }
                }
            }

            $this->_catalogSession->setCurrentProductId(null);
        }

        return [$request];
    }
}
