<?php
namespace Study\First\Controller\Adminhtml\Study;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Study\First\Model\ResourceModel\FirstTest;
use Study\First\Model\FirstTestFactory;

class Save extends Action
{
    /**
     * Authorization level of current admin session
     */
    const ADMIN_RESOURCE = 'Study_First::ui_components';

    /**
     * @var FirstTest
     */
    private $repository;

    /**
     * @var FirstTestFactory
     */
    private $firstTestFactory;

    /**
     * @param Context $context
     * @param FirstTest $repository
     * @param FirstTestFactory $firstTestFactory
     */
    public function __construct(
        Context $context,
        FirstTest $repository,
        FirstTestFactory $firstTestFactory
    ) {
        $this->repository = $repository;
        $this->firstTestFactory = $firstTestFactory;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request        = $this->getRequest();
        $data           = $request->getPostValue();

        if ($data) {
            $id = $request->getParam('id');

            if (empty($data['id'])) {
                $data['id'] = null;
            }

            if ($id) {
                try {
                    $item = $this->firstTestFactory->create();
                    $this->repository->load($item, $id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                    return $resultRedirect->setPath('*/*/', ['id' => $id]);
                }
            } else {
                $item = $this->firstTestFactory->create();
            }

            $item->setData($data);

            try {
                $this->repository->save($item);
                $this->messageManager->addSuccessMessage(__('Item was saved successfully.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving test item.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
