<?php
require 'inc/jumb.inc.php';
$jb = new Jumb();	

$title = 'Jumbotron Template for Bootstrap';
$header = "Привіт, світ!";
$head_text = "This is a template for a simple marketing or informational 
website.";
$full_head_text = "It includes a large callout called a jumbotron and three 
supporting pieces of content. Use it as a starting point to create 
something more unique.";
?>
<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/jumbotron.css" rel="stylesheet">
<script src="/assets/jquery.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/ie-emulation-modes-warning.js"></script>
<script>
$(document).ready(function(){
    $("#show").click(function(){
        $("#block").show();
    });
    $("#show").click(function(){
    		$("#show").hide();
    });
    $("#hide").click(function(){
        $("#block").hide();
    });
    $("#block").click(function(){
        $("#show").show();
    });

});
</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <?php
	include 'inc/top.inc.php';
	?>
<!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1><?=$header?></h1>
				<p><?=$head_text?></p>
				<p><a id="show" class="btn btn-primary btn-lg" role="button">Показати більше »</a></p>
        <p hidden="" id="block"><?=$full_head_text?> <br> <a id="hide" class="btn btn-primary btn-lg" role="button">Приховати »</a></p>
        
      </div>
    </div>

  <?php
  if(!$_GET['one']){
	include 'inc/index.inc.php';	
	}else{
	include 'inc/details.inc.php';	
	}
	include 'inc/bottom.inc.php';
	?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="assets/jquery.js"></script>-->
    <script>window.jQuery || document.write('<script src="/assets/jquery.js"><\/script>')</script>
    <script src="/assets/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/assets/ie10-viewport-bug-workaround.js"></script>

</body></html>