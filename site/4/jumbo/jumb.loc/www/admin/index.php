<?
require_once "secure/session.inc.php";
require_once "secure/secure.inc.php";

$secure = new Secure();

if(isset($_GET['logout'])){
	$secure->logOut();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Адмінка</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>
<body>
	<h1>Адміністрування блогу</h1>
	<h3>Доступні дії:</h3>
	<ul>
		<li><a href='http://jumb.loc/'>На головну</a></li><br>
		<li><a href='add.php'>Додавання статей</a></li>
		<li><a href='resave.php'>Редагування статей</a></li>
		<li><a href='secure/create_user.php'>Додати користувача</a></li>
		<li><a href='index.php?logout'>Завершити сеанс</a></li>
	</ul>
</body>
</html>