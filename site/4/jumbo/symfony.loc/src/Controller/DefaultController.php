<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;//* @Method({"GET", "POST"})
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\Article;
use App\Repository\ArticleRepository;
//use Doctrine\DBAL\Driver\Connection;
use App\Form\UserType;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

use Symfony\Component\Validator\Validator\ValidatorInterface;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends Controller
{
	  public $title;
    public $header;
    public $message;
    public $full_message;
    function __construct(){
    	$this->title = 'Jumbotron Template for Bootstrap';
			$this->header = "Привіт, світ!";
			$this->message = "This is a template for a simple marketing or informational	website.";
			$this->full_message = "It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.";
		}
  
    /**
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function index()
    //public function index(Article $articles): Response
    /*public function index(Connection $connection)
    	$users = $connection->fetchAll('SELECT * FROM users');*/
    {
  
  $articles = $this->getDoctrine()->getRepository(Article::class)->getDescrArticles();//свій метод в Repository
  //$articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
  /*->findBy(
    ['title' => 'DESC'],
    ['descr' => 'DESC'],
    ['id' => 'DESC']
);*/

/* $entityManager = $this->getDoctrine()->getManager();
$q = $entityManager->createQuery('SELECT a FROM App\Entity\Article a WHERE a.id > 0 ORDER BY a.id DESC');
$articles = $q->getResult();*/

		//dump($articles); exit;
	
	$count = count($articles);
    return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController', 'title'=>$this->title, 'header'=>$this->header, 'message'=>$this->message, 'full_message'=>$this->full_message, 'articles'=>$articles, 'count'=>$count,
        ]);// 'session'=>$session,
    }
		
		/**
     * @Route("/article/{id}", name="show", requirements={"id"="\d+"})
     * @Method({"GET"})
     */
		public function show($id)
    {
     	$article = $this->getDoctrine()->getRepository(Article::class)->find($id);
    	//->findOneBy(['id' => $id]);
    	//dump($article); exit;
    	/*if (!$article) {
        throw $this->createNotFoundException(
            'Немає статті з id '.$id);
        }*/
        return $this->render('default/show.html.twig', [
            'controller_name' => 'DefaultController', 'article'=>$article,
        ]);
    }
  
   /**
     * @Route("/admin/delete/{article}", name="delete")
     * @Method({"POST"})
     */
     
	public function delete(Request $request, $article)
  {
   	//dump($request->request->get('token')); exit;
   	//dump($article); exit;
		if ($this->isCsrfTokenValid('delete', $request->request->get('token'))) {
			$entityManager = $this->getDoctrine()->getManager();
			$article_tmp = $entityManager->getRepository(Article::class)->findOneBy(['id' => $article]);
			//dump($article_tmp); exit;
			
			$f = $article_tmp->getImg();
			//dump($f); exit;
		if(file_exists($f)){
		unlink($f);
		}
				
		$entityManager->remove($article_tmp);
		$entityManager->flush();
		//$session->set('message', 'Стаття видалена');
	  $this->addFlash('success', 'post.deleted_successfully');
		return $this->redirectToRoute('index', ['article' => $article_tmp->getId()]);
	  }else{
		return $this->redirectToRoute('index');
    } 
	}
		
		/**
     * @Route("/admin/add", name="add")
     * @Method({"GET"})
     */
		public function add(Request $request, SessionInterface $session)
    {//@Security("has_role('ROLE_ADMIN')")
    	//$session->set('foo', 'bar');
    	//dump($request); exit;
    	//dump($session); exit;
    //if($session->get('admin')):
    $errors = NULL;
    return $this->render('default/add.html.twig', [
            'controller_name' => 'DefaultController', 'request' => $request, 'session' => $session, 'errors' => $errors,
        ]);
    /*else:
		return $this->redirectToRoute('index');
		endif;
		}*/
    }
    
    /**
     * @Route("/admin/add", name="post")
     * @Method({"POST"})
     */
    public function post(ValidatorInterface $validator, Request $request): Response
    {
  if (!$this->isCsrfTokenValid('add', $request->request->get('token'))) {
            return $this->redirectToRoute('add');
        }
      //$request = Request::createFromGlobals();
      //$request = new Request($_GET, $_POST, [], $_COOKIE, $_FILES, $_SERVER);
			//$request->files->get('image');//true - заповенне поле, null - ні
			//$request->files->has('image');// чи є поле таке
      //$content = $request->request->all();//Повертає параметри
      //$content = $request->getContent();//Отримати вихідні дані, надіслані з тілом запиту
      //$content = $request->getSession(); //Доступ до сесії
     	//$content = $request->hasPreviousSession(); //попередні запити bool
      //dump($content); exit;
      $article = new Article;
     	$file = $request->files->get('image');//obj UploadedFile
     		if($file){//if ($file->getError() === UPLOAD_ERR_OK){
			$filename = $file->getClientOriginalName();//оригінальне ім'я
			//$pubpath = $file->move('img/', $filename);
			//dump($pubpath); exit;
			$file->move('img/', $filename);
			$article->setImg('img/'. $filename);
			  }
			//dump($article->getImg()); exit;
            
				  /*$form = $this->createForm(Article::class, $article);
				    $data = $form->getData();//Дані з форми
				    $form->handleRequest($request);//Дані з форми
				    dump($form); exit;*/
        
        $article ->setTitle($request->request->get('title'));
        $article ->setDescr($request->request->get('descr'));
        $article ->setText($request->request->get('text'));
        $article ->setAlias($request->request->get('alias'));
        //$article ->setImg($request->request->get('img'));
        $article ->setKeywords($request->request->get('keywords'));
        $article ->setCreatedAt(new \DateTime ($request->request->get('created_at')));
        $article ->setUpdatedAt(new \DateTime ($request->request->get('updated_at')));
        
        $errors = $validator->validate($article);
        if (count($errors) > 0) {
        //$errorsString = (string) $errors;
        //return new Response($errorsString);
        return $this->render('default/add.html.twig', [
        'request' => $request, 'errors' => $errors,
        ]);
		    }
		    
				$entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);//заповнює модель
        $entityManager->flush();//зберігає в базу
        //$session->set('message', 'Стаття додана');
        $this->addFlash('success', 'post.created_successfully');
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/admin/edit/{id}", requirements={"id"="\d+"})
     * @Method({"GET"})
     */
    public function update($id)
    {
	//if($session->get('admin')):
	$errors = NULL;
   	$article = $this->getDoctrine()->getRepository(Article::class)->find($id);
   	//dump($article); exit;
	  return $this->render('default/update.html.twig', [
            'controller_name' => 'DefaultController', 'article' => $article, 'errors' => $errors,
        ]);
	  /*else:
		return $this->redirectToRoute('index');
		endif;*/
		}
    
    /**
     * @Route("/admin/edit/{id}", name="edit")
     * @Method({"POST"})
     */
    public function update_post(ValidatorInterface $validator, Request $request, $id)
    {
if ($this->isCsrfTokenValid('edit', $request->request->get('token'))) {
            return $this->redirect("/admin/edit/$id");
        }
  //if($request->getMethod() == 'POST') {
			
		$entityManager = $this->getDoctrine()->getManager();
    $article = $entityManager->getRepository(Article::class)->find($id);
    /*if (!$article) {
        throw $this->createNotFoundException(
            'Немає статті з id '.$id);
    }*/
    
    $file = $request->files->get('image');
     		if($file){
     			$f = $article->getImg();
     		//dump($article->getImg()); exit;
			if(file_exists($f)):
			unlink($f);
			endif;
			$filename = $file->getClientOriginalName();
			$file->move('img/', $filename);
			$article->setImg('img/'. $filename);
			  }
			//dump($article->getImg()); exit;
    
     $article ->setTitle($request->request->get('title'));
     $article ->setDescr($request->request->get('descr'));
     $article ->setText($request->request->get('text'));
     $article ->setAlias($request->request->get('alias'));
     $article ->setKeywords($request->request->get('keywords'));
     $article ->setCreatedAt(new \DateTime ($request->request->get('created_at')));
     $article ->setUpdatedAt(new \DateTime ($request->request->get('updated_at')));
     $errors = $validator->validate($article);
        if (count($errors) > 0) {
        return $this->render('default/update.html.twig', [
        'request' => $request, 'errors' => $errors, 'article' => $article,
        ]);
		    }
    //if()
    $entityManager->flush();
    //$session->set('message', 'Стаття оновлена');
		$this->addFlash('success', 'post.updated_successfully');
    return $this->redirectToRoute('index', [
        'id' => $article->getId()
    ]);
		//}
    }
    
    /**
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
		{
    // get the login error if there is one
    $error = $authenticationUtils->getLastAuthenticationError();

    // last username entered by the user
    $lastUsername = $authenticationUtils->getLastUsername();
	
	//dump($authenticationUtils); exit;
    return $this->render('admin/login.html.twig', array(
        'last_username' => $lastUsername,
        'error'         => $error,
    ));
		}

    /**
     * @Route("/logout", name="logout")
     * @Method({"GET"})
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }
    
    /**
     * @Route("/register", name="register")
     * @Method({"GET", "POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
				if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
        throw $this->createAccessDeniedException();
    		}
// 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        //dump($form); exit;
        if ($form->isSubmitted() && $form->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
				//dump($user); exit;
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
				$this->addFlash('success', 'post.register_successfully');
            return $this->redirectToRoute('index');
        }

        return $this->render(
            'admin/register.html.twig',
            array('form' => $form->createView())
        );
    }
}