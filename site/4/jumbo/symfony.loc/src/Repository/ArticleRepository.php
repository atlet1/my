<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

//    /**
//     * @return Article[] Returns an array of Article objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
		/*public function getAllArticles()
    {
    	$_em = $this->getDoctrine()->getManager();
      return $this->_em->createQuery('SELECT a FROM App\Entity\Article a WHERE a.id > 0 ORDER BY a.id DESC')
                       ->getResult();
    }*/    
    
    public function getDescrArticles()
  	{
    $conn = $this->getEntityManager()->getConnection();
    $sql = 'SELECT id, title, descr FROM articles ORDER BY id DESC';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
		}

}
