<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

//    /**
//     * @return User[] Returns an array of User objects
//     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

		/*public function getAllUsers()
    {
    	$_em = $this->getDoctrine()->getManager();
      return $this->_em->createQuery('SELECT u FROM App\Entity\User a WHERE u.id > 0 ORDER BY u.id DESC')
                       ->getResult();
    }*/    
    
    public function getUsers()
  	{
    $conn = $this->getEntityManager()->getConnection();
    $sql = 'SELECT id, username, password, email FROM users ORDER BY id DESC';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
		}

}
