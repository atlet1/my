<?php
namespace Src;//Callable \Controller does not exist

use Slim\Http\Request;
use Slim\Http\Response;


use Src\Article;
//use Slim\Views\Twig;
//use Illuminate\Database\Query\Builder;
//use Illuminate\Support\Facades\DB;// A facade root has not been set
use Illuminate\Database\Capsule\Manager as DB;
//use Illuminate\Database\Capsule\Manager as Capsule;
//use Illuminate\Validation\Validator;
//use Illuminate\Support\Facades\Validator;
//use Validator;
//use Slim\Http\UploadedFile;
//use Monolog\Logger;
//use Monolog\Handler\StreamHandler;


class Controller
{
    //
    protected $view;
    public $title;
    public $header;
    public $message;
    public $full_message;
    function __construct(\Slim\Views\Twig $view){
    	$this->view = $view;
    	$this->title = 'Jumbotron Template for Bootstrap';
			$this->header = "Привіт, світ!";
			$this->message = "This is a template for a simple marketing or informational	website.";
			$this->full_message = "It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.";
		}
		
    public function index($request, $response){
/*echo $a;
error_log(serialize(error_get_last()), 3,  __DIR__ . '/../logs/app.log');
var_dump(error_get_last());
error_clear_last();
var_dump(error_get_last());

//$this->logger->info("slim.loc '/' route");
//$log = new Logger($settings['name']);
//$log->pushHandler(new StreamHandler( __DIR__ . '/../logs/app.log', $settings['level']));
//$log->info(serialize(error_get_last()));
//error_clear_last();//очищає, але в інфо всеодно йде тут
//var_dump(error_get_last());
//$log->info("slim.loc '/' route");
//$log->warning('Foo');
//$log->error('Bar');

	/* public function index($request, $response, $args) {
	 	 $user = User::find(1); 
	 	 var_dump($user); 
	 	 die(); 
	 	 $title = "Slim Auth";
	 	 $response = $this->view->render($response, 'home.php', ["title" => $title]);
	 	 return $response; 
	 	 } */
	 	 
			//$header = "Привіт, світ!";
			//$articles = Article::all();
		$_SESSION['token'] = md5(uniqid(mt_rand(), true));
	
	$articles = Article::select(['id', 'title', 'desc'])->orderByDesc('id')->get();
//$articles = $this->db->table('articles')->get();//без use ($capsule)
//$articles = DB::table('articles')->select(['id', 'title', 'desc'])->orderByDesc('id')->get();
//$articles = Capsule::table('articles')->where('id', '>', 0)->get();
//$articles = Capsule::select('select * from articles where id = ?', array(1));
		
	$count = count($articles);
	$request = $request->withAttribute('session', $_SESSION);
	$session = $request->getAttribute('session');
	
	return $this->view->render($response, 'default/index.html.twig', ['title'=>$this->title, 'header'=>$this->header, 'message'=>$this->message, 'full_message'=>$this->full_message, 'articles'=>$articles, 'count'=>$count, 'session'=>$session]);
		}
		
	public function show($request, $response, $arg){
			//$article = Article::find($id);//один запис вибирає
		$article = Article::select(['id', 'title', 'text', 'img', 'created_at', 'updated_at'])->where('id', $arg)->first();
	//var_dump($arg['id']);
    return $this->view->render($response, 'default/show.html.twig', [
         'article'=>$article]);//'id' => $arg['id']
		}
		
	public function delete($request, $response, $article){
	//var_dump($_SESSION['token']); exit;
if ((isset($_SESSION['token'])) || ($_POST['token'] == $_SESSION['token'])){
	
	//var_dump($article);
		$article_tmp = Article::where('id', $article)->first();//один запис
	//var_dump($article_tmp);
	
	$f = $article_tmp['img'];
	//var_dump($f); exit;
	if(file_exists($f)){
	unlink($f);
	}
		
	$article_tmp->delete();
	$_SESSION['message'] = 'Стаття видалена';
	return $response->withRedirect('/', 301);//->write('Стаття видалена');
	}else{
	return $response->withRedirect('/', 301);
	}
}
		public function add($request, $response){
			 if($_SESSION['admin']):
    return $this->view->render($response, 'default/add.html.twig');
    else:
	return $response->withRedirect('/', 301);
	endif;
		}
	
		public function post($request, $response){
			//$this->validate($request, ['title'=>'required|max:255', 'alias'=>'required|unique:articles,alias', 'text'=>'required']);
			/*Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);*/
			
			$data = $request->getParsedBody();
			if(empty ($data['text']) or empty ($data['alias'])){
				return $this->view->render($response, 'default/add.html.twig', [
         'data'=>$data]);
			}
			//var_dump($data); exit;
			$files = $request->getUploadedFiles();//масив файлів
				if($_FILES['image']['name']){
			$file = $files['image'];//об'єкт один файл
			$filename = $file->getClientFilename();
			//var_dump($filename); exit;
					if ($file->getError() === UPLOAD_ERR_OK){
			$file->moveTo('img/'. $filename);// без / перед img
			$data['img'] = 'img/'. $filename;
			  	}
			  }
			//var_dump($data); exit;
			$article = new Article;
			//var_dump($article); exit;
			$article->fill($data);//заповнює модель даними з реквеста
			//var_dump($article);
			$article->save();//зберігає в базу
			$_SESSION['message'] = 'Стаття додана';
   return $response->withRedirect('/', 301);
		}
		
		public function update($request, $response, $arg){
				if($_SESSION['admin']):
   	$article = Article::find($arg['id']);
   	//var_dump($article); exit;
	  return $this->view->render($response, 'default/update.html.twig', ['article'=>$article]);
	  else:
	return $response->withRedirect('/', 301);
	endif;
		}
		
	public function update_post($request, $response, $arg){
//if($request->isPost()) {
			//$this->validate($request, ['title'=>'required|max:255', 'alias'=>'required', 'text'=>'required']);
			$article = new Article;
		$data = $request->getParsedBody();
			//var_dump($article); exit;
			
			$files = $request->getUploadedFiles();
				if($_FILES['image']['name']){
	$article = Article::select('img')->where('id', $arg)->first();// для видалення $article['img']
	$f = $article['img'];
	//var_dump($f); exit;
			if(file_exists($f)):
			unlink($f);
			endif;
		
			$file = $files['image'];
			$filename = $file->getClientFilename();
			//var_dump($filename); exit;
					if ($file->getError() === UPLOAD_ERR_OK){
			$file->moveTo('img/'. $filename);
			$data['img'] = 'img/'. $filename;
			  	}
			  }
		 //var_dump($data); exit;
					
				if($article->where('id', $arg['id'])->update($data)){
				$_SESSION['message'] = 'Стаття оновлена';
				return $response->withRedirect('/', 301);
				}
	}
//}
	public function login($request, $response){
	//$email = $_POST["email"];
	//$password = $_POST["password"];
	$email = $request->getParam('email');//getParsedBody()['email'];
	$password = $request->getParam('password');//getParsedBody()['password'];
	
	$user = DB::table('users')->select(['email', 'password'])->where('email', $email)->first();

	if(empty($email and $password)){
		$_SESSION['message'] = 'Заповніть всі поля!';
		}else{
				if ($user->email !== $email or $user->password !== md5($password)):
					$_SESSION['message'] = 'Невірний пароль чи ім\'я користувача!';
				else:
					$_SESSION['admin'] = TRUE;
					$_SESSION['message'] = 'Ви увійшли';
				return $response->withRedirect('/', 301);
				endif;
		}
return $response->withRedirect('/', 301);
}

	public function register($request, $response){
	if($_SESSION['admin']):
	if ($_SERVER['REQUEST_METHOD']=='GET'){
		return $this->view->render($response, 'admin/register.html.twig');
		}else{
	$email = $_POST["email"];//$email = 'yur@my.loc';
	$password = md5($_POST["password"]);//$password = '1234';
//$sql = "INSERT INTO users (email, password)	VALUES ('$email', '$password')";
		DB::table('users')->insert(['email' => $email, 'password' => $password]);

	if(empty($email and $password)){
		$_SESSION['message'] = 'Заповніть всі поля!';
		}else{
				$_SESSION['message'] = 'Ви зареєстровані';
				return $response->withRedirect('/', 301);
		}
	}	
	else:
	return $response->withRedirect('/', 301);
	endif;
}

	public function logout($request, $response){
	session_destroy();
	  return $response->withRedirect('/', 301);
	}
}