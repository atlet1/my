<?php
// DIC configuration

$container = $app->getContainer();

$container['Controller'] = function($c) {
    $view = $c->get("view");
// retrieve the 'view' from the container
    return new \Src\Controller($view);
};

// view renderer
/*$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};*/
//twig
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig($container['settings']['viewTemplatesDirectory'], [
        'cache' => false//'../cache'
    ]);
// Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    //$view->addGlobal('session', $_SESSION);// {{ session.username }} {{ app.session.get('name_variable') }}  {{app.object name.field name}}
     return $view;
};

// Service factory for the ORM
//$container['db'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);
	//$capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher(new \Illuminate\Container\Container));//замість bootEloquent з підключеним Events
    $capsule->setAsGlobal();
    $capsule->bootEloquent();//замість setEventDispatcher
$container['db'] = function ($container) use ($capsule) {
 return $capsule;
};


// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
