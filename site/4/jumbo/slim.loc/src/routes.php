<?php
use Src\Controller;
//use Slim\Http\Request;
//use Slim\Http\Response;

//use Slim\Views\Twig;
//use Illuminate\Database\Query\Builder;
//use Illuminate\Support\Facades\DB;// A facade root has not been set
//use Illuminate\Database\Capsule\Manager as DB;
//use Illuminate\Database\Capsule\Manager as Capsule;
//use Illuminate\Validation\Validator;
//use Illuminate\Support\Facades\Validator;
//use Validator;

// Routes

// Render Twig template in route
$app->map(['GET', 'POST'], '/', 'Controller:index')->setName('index');
//$app->map(['GET', 'POST'], '/', Controller::class . ':index')->setName('index');
$app->get('/article/{id}', 'Controller:show')->setName('show');

$app->post('/delete/{article}', 'Controller:delete')->setName('delete');

$app->get('/admin/add', 'Controller:add')->setName('add');

$app->post('/admin/add', 'Controller:post')->setName('post');

$app->get('/admin/update/{id}', 'Controller:update');

$app->post('/admin/update/{id}', 'Controller:update_post')->setName('update');

$app->post('/login', 'Controller:login')->setName('login');

$app->map(['GET', 'POST'], '/register', 'Controller:register')->setName('register');

$app->get('/logout', 'Controller:logout')->setName('logout');
