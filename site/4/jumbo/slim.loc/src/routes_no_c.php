<?php
use Src\Article;
//use Slim\Http\Request;
//use Slim\Http\Response;

//use Slim\Views\Twig;
//use Illuminate\Database\Query\Builder;
//use Illuminate\Support\Facades\DB;// A facade root has not been set
//use Illuminate\Database\Capsule\Manager as DB;
//use Illuminate\Database\Capsule\Manager as Capsule;
//use Illuminate\Validation\Validator;
//use Illuminate\Support\Facades\Validator;
//use Validator;
//use Src\Controller;

// Routes

/*$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});*/

// Render Twig template in route
$app->map(['GET', 'POST'], '/', function ($request, $response) {
	//$this->logger->info("slim.loc '/' route");
			$title = 'Jumbotron Template for Bootstrap';
			$header = "Привіт, світ!";
			$message = "This is a template for a simple marketing or informational	website.";
			$full_message = "It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.";
			$_SESSION['token'] = md5(uniqid(mt_rand(), true));
	
	$articles = Article::select(['id', 'title', 'desc'])->orderByDesc('id')->get();
//$articles = $this->db->table('articles')->get();//без use ($capsule)
//$articles = DB::table('articles')->select(['id', 'title', 'desc'])->orderByDesc('id')->get();
//$articles = Capsule::table('articles')->where('id', '>', 0)->get();
//$articles = Capsule::select('select * from articles where id = ?', array(1));
		
	$count = count($articles);
	$request = $request->withAttribute('session', $_SESSION);
	$session = $request->getAttribute('session');
	
	return $this->view->render($response, 'default/index.html.twig', ['title'=>$title, 'header'=>$header, 'message'=>$message, 'full_message'=>$full_message, 'articles'=>$articles, 'count'=>$count, 'session'=>$session]);
})->setName('index');

$app->get('/article/{id}', function ($request, $response, $arg) {
	$article = Article::select(['id', 'title', 'text', 'img', 'created_at', 'updated_at'])->where('id', $arg)->first();
	//var_dump($arg['id']);
    return $this->view->render($response, 'default/show.html.twig', [
         'article'=>$article]);//'id' => $arg['id']
})->setName('show');

$app->post('/delete/{article}', function($request, $response, $article){
	//var_dump($_SESSION['token']); exit;
if ((isset($_SESSION['token'])) || ($_POST['token'] == $_SESSION['token'])){
	
	//var_dump($article);
		$article_tmp = Article::where('id', $article)->first();//один запис
	//var_dump($article_tmp);
	
		$f = $article_tmp['img'];
	//var_dump($f); exit;
	if(file_exists($f)){
	unlink($f);
	}
		
	$article_tmp->delete();
	$_SESSION['message'] = 'Стаття видалена';
	return $response->withRedirect('/', 301);//->write('Стаття видалена');
}else{
	return $response->withRedirect('/', 301);
}
})->setName('delete');

$app->get('/admin/add', function ($request, $response) {
    if($_SESSION['admin']):
    return $this->view->render($response, 'default/add.html.twig');
    else:
	return $response->withRedirect('/', 301);
	endif;
})->setName('add');

$app->post('/admin/add', function ($request, $response) {
			$data = $request->getParsedBody();
			if(empty ($data['text']) or empty ($data['alias'])){
				return $this->view->render($response, 'default/add.html.twig', [
         'data'=>$data]);
			}
			$files = $request->getUploadedFiles();//масив файлів
				if($_FILES['image']['name']){
			$file = $files['image'];//об'єкт один файл
			$filename = $file->getClientFilename();
			//var_dump($filename); exit;
					if ($file->getError() === UPLOAD_ERR_OK){
			$file->moveTo('img/'. $filename);// без / перед img
			$data['img'] = 'img/'. $filename;
			  	}
			  }
			//var_dump($data); exit;
			$article = new Article;
			//var_dump($article); exit;
			$article->fill($data);//заповнює модель даними з реквеста
			//var_dump($article);
			$article->save();//зберігає в базу
			$_SESSION['message'] = 'Стаття додана';
   return $response->withRedirect('/', 301);
})->setName('post');

$app->get('/admin/update/{id}', function ($request, $response, $arg) {
   	if($_SESSION['admin']):
   	$article = Article::find($arg['id']);
   	//var_dump($article); exit;
	  return $this->view->render($response, 'default/update.html.twig', ['article'=>$article]);
	  else:
	return $response->withRedirect('/', 301);
	endif;
});

$app->post('/admin/update/{id}', function ($request, $response, $arg) {
		$article = new Article;
		$data = $request->getParsedBody();
	//var_dump($article); exit;
			
			$files = $request->getUploadedFiles();
				if($_FILES['image']['name']){
	$article = Article::select('img')->where('id', $arg)->first();// для видалення $article['img']
	$f = $article['img'];
	//var_dump($f); exit;
			if(file_exists($f)):
			unlink($f);
			endif;
		
			$file = $files['image'];
			$filename = $file->getClientFilename();
			//var_dump($filename); exit;
					if ($file->getError() === UPLOAD_ERR_OK){
			$file->moveTo('img/'. $filename);
			$data['img'] = 'img/'. $filename;
			  	}
			  }
		 //var_dump($data); exit;
		 
				if($article->where('id', $arg['id'])->update($data)){
				$_SESSION['message'] = 'Стаття оновлена';
				return $response->withRedirect('/', 301);
		}
})->setName('update');

$app->post('/login', function ($request, $response) {
	//$email = $_POST["email"];
	//$password = $_POST["password"];
	$email = $request->getParam('email');//getParsedBody()['email'];
	$password = $request->getParam('password');//getParsedBody()['password'];
	
		$user = $this->db->table('users')->select(['email', 'password'])->where('email', $email)->first();

	if(empty($email and $password)){
		$_SESSION['message'] = 'Заповніть всі поля!';
		}else{
				if ($user->email !== $email or $user->password !== md5($password)):
					$_SESSION['message'] = 'Невірний пароль чи ім\'я користувача!';
				else:
					$_SESSION['admin'] = TRUE;
					$_SESSION['message'] = 'Ви увійшли';
				return $response->withRedirect('/', 301);
				endif;
		}
return $response->withRedirect('/', 301);	
})->setName('login');

$app->map(['GET', 'POST'], '/register', function ($request, $response) {
	if($_SESSION['admin']):
	if ($_SERVER['REQUEST_METHOD']=='GET'){
		return $this->view->render($response, 'admin/register.html.twig');
		}else{
	$email = $_POST["email"];//$email = 'yur@my.loc';
	$password = md5($_POST["password"]);//$password = '1234';
//$sql = "INSERT INTO users (email, password)	VALUES ('$email', '$password')";
		$this->db->table('users')->insert(['email' => $email, 'password' => $password]);

	if(empty($email and $password)){
		$_SESSION['message'] = 'Заповніть всі поля!';
		}else{
				$_SESSION['message'] = 'Ви зареєстровані';
				return $response->withRedirect('/', 301);
		}
	}	
	else:
	return $response->withRedirect('/', 301);
	endif;
})->setName('register');

$app->get('/logout', function ($request, $response) {
	session_destroy();
	  return $response->withRedirect('/', 301);
})->setName('logout');
