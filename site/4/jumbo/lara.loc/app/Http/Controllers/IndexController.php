<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Storage;

class IndexController extends Controller
{
    //
    public $title;
    public $header;
    public $message;
    public $full_message;
    function __construct(){
			$this->title = 'Jumbotron Template for Bootstrap';
			$this->header = "Привіт, світ!";
			$this->message = "This is a template for a simple marketing or informational	website.";
			$this->full_message = "It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.";
		}
		
    public function index(){
			//$header = "Привіт, світ!";
			//$articles = Article::all();
			$articles = Article::select(['id', 'title', 'desc'])->orderByDesc('id')->get();
			
			//dump($articles);					
			return view('index')->with(['title'=>$this->title, 'header'=>$this->header, 'message'=>$this->message, 'full_message'=>$this->full_message, 'articles'=>$articles]);
		}
		
		public function show($id){
			//$article = Article::find($id);//один запис вибирає
			$article = Article::select(['id', 'title', 'text', 'img', 'created_at', 'updated_at'])->where('id', $id)->first();
			
			//dump($articles);				
			//dump($id);				
			return view('show')->with(['title'=>$this->title, 'header'=>$this->header, 'message'=>$this->message, 'full_message'=>$this->full_message, 'article'=>$article]);
		}
		
		public function add(){
			return view('add');
		}
		
		public function post(Request $request){
			//dump($request->all());
			$trans = ['required'=>'Поле :attribute обов\'язкове', 'unique'=>'Поле :attribute має бути унікальним'];
			$this->validate($request, ['title'=>'required|max:255', 'alias'=>'required|unique:articles,alias', 'text'=>'required'], $trans);
			$data = $request->all();
			
			  if($request->hasFile('image')){
			//$file = $request->file('image');// те, що і $data['image']
			$file = $data['image'];//obj UploadedFile
			$filename = $file->getClientOriginalName();//оригінальне ім'я
			$file->move(public_path().'/img/', $filename);//$data['img'].'.jpg');// назва з поля img Зображення: <input...
//$file = $request->image->storeAs('public', $filename);//storage/app/public
			$data['img'] = '/img/'. $filename;
			  }
			
			//dd($file);
			//dd($filename);
			$article = new Article;//new Article($data)заповнює модель
			//dump($article);
			$article->fill($data);//заповнює модель даними з реквеста
			//dump($article);
			$article->save();//зберігає в базу
			return redirect('/')->with('message','Стаття додана');
		}
		public function update($id){
			//$article = Article::select(['id', 'title', 'desc', 'text', 'alias', 'img', 'keywords', 'created_at', 'updated_at'])->where('id', $id)->first();
			$article = Article::find($id);
			return view('update')->with(['article'=>$article]);
		}
		public function update_post(Article $article, Request $request, $id){
			//dump($request->all());
			
			if($request->isMethod('post')) {
				$this->validate($request, ['title'=>'required|max:255', 'alias'=>'required', 'text'=>'required']);
			//$data = $request->all();
			$data = $request->except('_token');
			//$article = new Article;
			//dump($article);

				if($request->hasFile('image')){
$article = Article::select('img')->where('id', $id)->first();// для видалення $article['img']
$f = public_path(). $article['img'];
//dd($f);
			if(file_exists($f)):
			unlink($f);
			endif;
						//Storage::disk('pubpath')->delete($f);
    	$file = $data['image'];
			$filename = $file->getClientOriginalName();
			$file->move(public_path().'/img/', $filename);
			$data['img'] = '/img/'. $filename;
			  }
			//dd($file);
			unset($data['image']);//немає цього поля в таблиці
			//DB::table('articles')->where('id', $id)->update($data);
			if($article->where('id', $id)->update($data)){
				return redirect('/')->with('message','Стаття оновлена');
			}
		}
	}
}