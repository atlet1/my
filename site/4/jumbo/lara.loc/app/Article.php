<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    //protected $table = 'articles';
    protected $fillable = ['title', 'desc', 'text', 'alias', 'img', 'keywords', 'created_at', 'updated_at'];//дозволяє масове заповнення полів
 }
