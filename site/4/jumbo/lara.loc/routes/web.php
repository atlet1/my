<?php
//use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'IndexController@index');
Route::get('article/{id}', 'IndexController@show')->name('show');

Route::group(['prefix'=>'admin','middleware'=>'auth'], function() {
//Route::get('admin/add', 'Index@add');
Route::get('add', 'IndexController@add');
Route::post('add', 'IndexController@post')->name('post');

Route::delete('delete/{article}', function(\App\Article $article){//function($article)
		//dump($article);
//$article_tmp = \App\Article::where('id', $article)->first();//один запис
		//dump($article_tmp);
//$article_tmp->delete();

//Storage::delete($article['img']);//файл в папці storage (налаштовано)
//$files = Storage::files();// файли в папці (тут в корені)
$f = public_path(). $article['img'];
//dd($f);
if(file_exists($f)){
	unlink($f);
}
	
	$article->delete();
	return redirect('/')->with('message','Стаття видалена');
})->name('delete');

Route::get('update/{id}', 'IndexController@update');
Route::post('update/{id}', 'IndexController@update_post')->name('update');

});
//Route::auth();
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@index')->name('admin');
