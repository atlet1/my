<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title>Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.css')}}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ asset('css/ie10-viewport-bug-workaround.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/jumbotron.css')}}" rel="stylesheet">
<script src="{{ asset('js/jquery.js')}}"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{{ asset('css/ie-emulation-modes-warning.js')}}"></script>
<script>
$(document).ready(function(){
    $("#show").click(function(){
        $("#block").show();
    });
    $("#show").click(function(){
    		$("#show").hide();
    });
    $("#hide").click(function(){
        $("#block").hide();
    });
    $("#block").click(function(){
        $("#show").show();
    });

});
</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  
<!--	
	<nav class="navbar navbar-default navbar-fixed-main">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Мій сайт:</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Додому</a></li>
            <li><a href="#">Про нас</a></li>
            <li><a href="#">Сторінки</a></li>
            <li><a href="#">Сторінка</a></li>
            <li><a href="#">Контакти</a></li>
          </ul>
        </div>/ 
      </div>
    </nav>
    -->
      <div class="flex-center position-ref full-height">
@if (session('message'))
	    <div class="alert alert-success">
	        {{ session('message') }}
	    </div>
@endif
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Ім'я проекту</a>
        </div>
				        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" action="{{ route('login') }}" method="POST">
          @csrf
            <div class="form-group">
              <input placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" name="email" value="{{ old('email') }}" required autofocus>
            </div>
            <div class="form-group">
              <input placeholder="Пароль" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required>
            </div>
            
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <p style="color: #ff0000"><strong>{{ $errors->first('email') }}</strong></p>
                                    </span>
                                @endif
          @guest                      
            <button type="submit" class="btn btn-success">Увійти</button>                
          @endguest
            @if (Route::has('admin'))
                    @auth
                        <a href="{{ url('/admin') }}">&nbsp;<b>Admin</b></a>
                    @else
                        <a href="{{ route('login') }}">&nbsp;<b>Login</b></a>
                    @endauth
              @endif      
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav><!--
    <div id="container" class="container nav">
     <ul id="navbar" class="nav navbar-nav" style="font-size: 20px">
            <li class="active"><a href="#">Додому</a></li>
            <li><a href="#">Про нас</a></li>
            <li><a href="#">Сторінки</a></li>
            <li><a href="#">Сторінка</a></li>
            <li><a href="#">Контакти</a></li>
          </ul> 
		</div>-->
    <!-- Main jumbotron for a primary marketing message or call to action -->
    
@yield('content')
        
      </div>
      <hr>

      <footer>
       <p>© 2018-{{date('Y')}} Yura, Inc.</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="assets/jquery.js"></script>-->
    <script>window.jQuery || document.write('<script src="{{ asset('assets/jquery.js')}}"><\/script>')</script>
    <script src="{{ asset('assets/bootstrap.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('assets/ie10-viewport-bug-workaround.js')}}"></script>
  

</body></html>