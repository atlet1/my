@extends('base')
@section('head')
    <div class="jumbotron">
      <div class="container">
        <h1>{{$header}}</h1>
        <p>{{$message}}</p>
       <p><a id="show" class="btn btn-primary btn-lg" role="button">Показати більше »</a></p>
       <p hidden="" id="block">{{$full_message}} <br> <a id="hide" class="btn btn-primary btn-lg" role="button">Приховати »</a></p>
      </div>
    </div>
@show

@section('content')
    <div class="container">
    Всього статей: {{count($articles)}}
      <!-- Example row of columns -->
      <div class="row">
@foreach($articles as $article)
        <div class="col-md-4">
          <h2>{{ $article->title }}</h2>
          <p>{!! $article->desc !!}</p>
          <p><a class="btn btn-default" href="{{route ('show', $article->id) }}" role="button">Показати деталі »</a> 
@auth
          <a class="btn btn-default" href="{{route ('update', $article->id) }}" role="button">Редагувати »</a></p>
    
    <form action="{{route('delete', ['article'=>$article->id])}}" method="post">
<!-- <input type="hidden" name="_method" value="DELETE"/> -->
    {{ method_field('DELETE') }}
<!-- <input type="hidden" name="_token" value="sfsdjgdgfdfgjyio"/> --> 
    	{{csrf_field()}}

    	<button type="submit" class="btn btn-danger">Видалити</button>
    </form>
@endauth   
        </div>
        
@endforeach  
@endsection     