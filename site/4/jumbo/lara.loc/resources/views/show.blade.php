@extends('base')
@section('content')
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
@if($article)
        <div class="col-md-4">
        <img src="{{asset($article->img)}}" style="margin-top: 50pt"/>
          <h2>{{ $article->title }}</h2>
          <p>{!! $article->text !!}</p>
          <p>Створено: {{$article->created_at}} <br> Змінено: {{$article->updated_at}}</p>
          <p><a class="btn btn-default" href="{{ url('/') }}" role="button">Назад »</a></p>
        </div>
@endif
@endsection     