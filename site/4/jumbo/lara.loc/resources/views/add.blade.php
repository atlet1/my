<!DOCTYPE html>
<html><head>
<title>Додавання статей</title>
<link href="{{ asset('css/bootstrap.css')}}" rel="stylesheet">
<script src="{{ asset('js/ckeditor/ckeditor.js')}}"></script>
</head><body>
   <div class="col-md-9">

	<div class="">
		<h2>Додавання статті</h2>
    </div>
	
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	@if (session('message'))
	    <div class="alert alert-success">
	        {{ session('message') }}
	    </div>
	@endif

<form method="post" enctype="multipart/form-data" action="{{ route('post') }}">
	  <div class="form-group">
	    <label for=title">Заголовок</label>
	    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" placeholder="Назва статті">
	  </div>
	  	  <div class="form-group">
	    <b>Короткий опис:</b><p> <textarea class="form-control" name="desc"  cols="130" rows="5">{{ old('desc') }}</textarea></p>
    </div>
	  <div class="form-group">
	    <label for="text">Текст статті *:</label>
	    <textarea class="form-control" id="text" name="text">{{ old('text') }}</textarea>
	  </div>
	  <div class="form-group">
	    <label>Псевдонім:</label>
	    <input type="text" class="form-control" id="alias" value="{{ old('alias') }}" name="alias" placeholder="Alias">
	  </div>
		<!--<p>Зображення: <input type="text" name="img" size="52" value="/img/"></p>-->
		<p>Файл зображення: <input type="file" name="image" value=""><!-- enctype='multipart/form-data' --></p>
		<p>Ключі: <input type="text" name="keywords" size="60" value="{{ old('keywords') }}" placeholder="Keywords"></p>
		<p>Дата створення: <input type="text" name="created_at" size="20" value="<?=time()?>"></p>
				<p>Дата редагування: <input type="text" name="updated_at" size="18" value="<?=time()?>"></p>
		<p><button type="submit" class="btn btn-primary">Додати</button></p>
	{{csrf_field()}}
</form>   
   </div>
<!-- <div class="wrapper container-fluid"> 
	   {!! Form::open(['url' => route('post'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}
	  </div> 
	   <div class="form-group">
	     {!! Form::label('text', 'Текст:',['class'=>'col-xs-2 control-label']) !!}
	     <div class="col-xs-8">
		 	{!! Form::textarea('text', old('text'), ['id'=>'text','class' => 'form-control','placeholder'=>'Текст статті']) !!}
		 </div></div>
	  <div class="form-group">
	    <div class="col-xs-8">
		 	{!! Form::file('images', ['class' => 'filestyle','data-buttonText'=>'Виберіть зображення','data-buttonName'=>"btn-primary",'data-placeholder'=>"Немає файла"]) !!} </div></div>
		 {!! Form::close() !!}
		 <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
	<script>
		CKEDITOR.replace('text');
	</script>
</body></html>  