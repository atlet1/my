<!DOCTYPE html>
<html>
<head>
<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="/assets/css/base.css">  
   <link rel="stylesheet" href="/assets/css/main.css">
      
<!-- script
   ================================================== -->
	<script src="/assets/js/modernizr.js"></script>
	<script src="/assets/js/jquery-1.11.3.min.js"></script>
	<script src="/assets/ckeditor/ckeditor.js"></script>
	<title>Редагування секції Services</title>
</head>
<body>
<section id="pricing">
<div class="row">
<p align="center"><a class="button" style="color: #ff0000; font-size:20px ;" href="/admin" role="button">Назад</a></p><br>
	<!--<form action="" method="get">-->
<?php		
	foreach($this->getServices() as $item){
		/*echo <<<SRV
		<button type="submit" name="id" value="{$item['id']}" class="btn btn-danger">{$item['id']}-{$item['head']}</button>
SRV;*/
echo <<<SRV
		<a class="button" href="/admin/servicesedit/id/{$item['id']}" role="button">{$item['id']}-{$item['head']}</a>
SRV;
}
/*if(isset($_GET['id'])){//був пост, але з get-параметрами краще
			$id = $_GET['id'] * 1;*/
			
//echo $this->id;
if(isset($this->id)){
$id = $this->id;
?>
	<!--</form>-->
	<form method="post" enctype="" action="">
		<p>Заголовок: <input type="text" name="head" size="80" value="<?=$this->getServicesH()['head']?>"></p>
			<p>Короткий опис: <input type="text" name="desc" size="120" value="<?=$this->getServicesH()['desc']?>"></p>
			
	
		<p>Заголовок сервіса: <input type="text" name="headsv" size="50" value="<?=$this->getOneService($id)['head']?>"></p>
		<p>Текст сервіса:<textarea name="text" cols="100" rows="20"><?=$this->getOneService($id)['text']?></textarea></p>
		<p><input type="submit" value="Змінити"></p>
		</form>
	
    	<script>
		CKEDITOR.replace('text');
	</script>	
<?php	
	}
?>
		</div>
	</section>
	
</body>
</html>
