<!DOCTYPE html>
<html>
<head>
<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="/assets/css/base.css">  
   <link rel="stylesheet" href="/assets/css/main.css">
      
<!-- script
   ================================================== -->
	<script src="/assets/js/modernizr.js"></script>
	<script src="/assets/js/jquery-1.11.3.min.js"></script>
	<script src="/assets/ckeditor/ckeditor.js"></script>
	<title>Редагування секції AboutUs</title>
</head>
<body>
<section id="pricing">
<div class="row">
	<form method="post" enctype="multipart/form-data" action="">
	<p align="center"><a class="button" style="color: #ff0000; font-size:20px ;" href="/admin"role="button">Назад</a></p><br>
		<p>Заголовок: <input type="text" name="head" size="100" value="<?=$this->getAboutUs()[0]['head']?>"></p>
		<p>Текст:<textarea name="text" cols="100" rows="20"><?=$this->getAboutUs()[0]['text']?></textarea></p>
		<input type="hidden" name="MAX_FILE_SIZE" value="300000" />
		<input type="hidden" name="img_old" value="<?=$this->getAboutUs()[0]['img']?>" />
		<p>Файл зображення: <img src="/assets/images/<?=$this->getAboutUs()[0]['img']?>" width="10%" height="10%"/>
		<input type="file" name="image" value=""></p>
		<p><input type="submit" value="Змінити"></p>
		</form>
	
    	<script>
		CKEDITOR.replace('text');
	</script>	
		</div>
	</section>
	
</body>
</html>
