<!DOCTYPE html>
<html>
<head>
<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="/assets/css/base.css">  
   <link rel="stylesheet" href="/assets/css/main.css">
      
<!-- script
   ================================================== -->
	<script src="/assets/js/modernizr.js"></script>
	<script src="/assets/js/jquery-1.11.3.min.js"></script>
	<script src="/assets/ckeditor/ckeditor.js"></script>
	<title>Редагування секції Home</title>
</head>
<body>
<section id="pricing">
<div class="row">
	<form method="post" enctype="" action="">
	<p align="center"><a class="button" style="color: #ff0000; font-size:20px ;" href="/admin"role="button">Назад</a></p><br>
		<p>Заголовок: <input type="text" name="head" size="100" value="<?=$this->getIntro()[0]['head']?>"></p>
		<p>Текст:<textarea name="text" cols="100" rows="20"><?=$this->getIntro()[0]['text']?></textarea></p>
		<p><input type="submit" value="Змінити"></p>
		</form>
	
    	<script>
		CKEDITOR.replace('text');
	</script>	
		</div>
	</section>
	
</body>
</html>
