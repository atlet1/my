<?php
class AdminController implements IController {

  public function indexAction() {
    $fc = FrontController::getInstance();
    $model = new AdminModel();
        
    $view = $model->render('../views/admin.php');

    $fc->setBody($view);
  }
    public function homeeditAction() {
    $fc = FrontController::getInstance();
    $model = new AdminModel();
  session_start();
	header("HTTP/1.0 401 Unauthorized");
	if(isset($_SESSION['admin'])){  
        
    $view = $model->render('../views/homeedit.php');

    $fc->setBody($view);
    if($_SERVER["REQUEST_METHOD"]=="POST"):
    //var_dump($_POST); exit;
    $head = $model->clearStr($_POST['head']);
    $text = $_POST['text'];
    $model->updateIntro($head, $text);
    header('Location: /admin');
    endif;
  }else{
	header('Location: /admin');
	}
  	}
      public function aboutuseditAction() {
    $fc = FrontController::getInstance();
    $model = new AdminModel();
  session_start();
	header("HTTP/1.0 401 Unauthorized");
	if(isset($_SESSION['admin'])){  
        
    $view = $model->render('../views/aboutusedit.php');

    $fc->setBody($view);
    if($_SERVER["REQUEST_METHOD"]=="POST"):
    //var_dump($_POST); exit;
    $head = $model->clearStr($_POST['head']);
    $text = $_POST['text'];
    if($_FILES['image']['name']){
			$image = $_FILES['image']['name'];
		}else{
			//$image = $view->getAboutUs()[0]['img'];
			$image = $_POST['img_old'];
		}
    //var_dump($image);exit;
     $model->updateAboutUs($head, $text, $image);
    header('Location: /admin');
    endif;
  }else{
	header('Location: /admin');
	}
  	}
      public function serviceseditAction() {
    $fc = FrontController::getInstance();
    $model = new AdminModel();
 	session_start();
	header("HTTP/1.0 401 Unauthorized");
	if(isset($_SESSION['admin'])){  
    $model->id = $fc->getParams()["id"];  
    $view = $model->render('../views/servicesedit.php');

    $fc->setBody($view);
    if($_SERVER["REQUEST_METHOD"]=="POST"):
    //var_dump($_POST); exit;
    $head = $model->clearStr($_POST['head']);
    $desc = $model->clearStr($_POST['desc']);
    //$id = $_GET['id'] * 1;
    //$id = $model->id;
    $headsv = $model->clearStr($_POST['headsv']);
    $text = $_POST['text'];
    
    $model->updateServices($head, $desc, $model->id, $headsv, $text);
    header('Location: /admin');
    endif;
  }else{
	header('Location: /admin');
	}
  	}
}
