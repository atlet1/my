<?php
class Model{
	//const DB_NAME = 'E:\Serv\home\jumb.loc\www\inc\jumbo.db';
	const DB_NAME = '/home/land.loc/www/application/land.db';
	protected $_pdo;
	function __construct() {
	if(is_file(self::DB_NAME)){
	$this->_pdo = new PDO('sqlite:'.self::DB_NAME);
	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}else{
			try{
	$this->_pdo = new PDO('sqlite:'.self::DB_NAME);
	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "CREATE TABLE intro(
	id INTEGER PRIMARY KEY,
	head TEXT,
	text TEXT NOT NULL,
	UNION 
	CREATE TABLE aboutus(
	id INTEGER PRIMARY KEY,
	head TEXT,
	text TEXT NOT NULL,
	img TEXT,
	UNION 
	CREATE TABLE users(
	id INTEGER PRIMARY KEY,
	email TEXT NOT NULL UNIQUE,
	password TEXT NOT NULL UNIQUE)";
	$this->_pdo->exec($sql);
	if($this->_pdo->exec($sql))
	return true;
			}catch(PDOException $e){
				echo $e->getMessage();
				return false;
			}
		}
	}
	function __destruct(){
		unset ($this->_pdo);
	}
	
	public function getIntro(){
		try{
			$sql = "SELECT id, head, text
							FROM intro ORDER BY id ASC";
			if($stmt = $this->_pdo->query($sql))
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
		public function getAboutUs(){
		try{
			$sql = "SELECT id, head, text, img
							FROM aboutus ORDER BY id ASC";
			if($stmt = $this->_pdo->query($sql))
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
		public function getServicesH(){
		try{
			$sql = "SELECT id, head, desc
							FROM services_h";
			$stmt = $this->_pdo->query($sql);
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				unset($stmt);
				return $row;
		}catch(PDOException $e){
			return false;
		}
	}
	public function getServices(){
		try{
			$sql = "SELECT id, head, text
							FROM services ORDER BY id ASC";
			if($stmt = $this->_pdo->query($sql))
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
	protected function db2Arr($data){//$items
	$arr = [];
	while ($row = $data->fetch(PDO::FETCH_ASSOC))
	$arr[] = $row;
  return $arr;
	}
	
  public function render($file) {
    ob_start();
    include(dirname(__FILE__) . '/' . $file);
    return ob_get_clean();
  }
}