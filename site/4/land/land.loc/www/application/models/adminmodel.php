<?php
class AdminModel{
	
public $id = '';//admin/servicesedit/id/
	const DB_NAME = '/home/land.loc/www/application/land.db';
	protected $_pdo;
	function __construct() {
	if(is_file(self::DB_NAME)){
	$this->_pdo = new PDO('sqlite:'.self::DB_NAME);
	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}else{
			try{
	$this->_pdo = new PDO('sqlite:'.self::DB_NAME);
	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "CREATE TABLE intro(
	id INTEGER PRIMARY KEY,
	head TEXT,
	text TEXT NOT NULL,
	UNION 
	CREATE TABLE aboutus(
	id INTEGER PRIMARY KEY,
	head TEXT,
	text TEXT NOT NULL,
	img TEXT,
	UNION 
	CREATE TABLE users(
	id INTEGER PRIMARY KEY,
	email TEXT NOT NULL UNIQUE,
	password TEXT NOT NULL UNIQUE)";
	$this->_pdo->exec($sql);
	if($this->_pdo->exec($sql))
	return true;
			}catch(PDOException $e){
				echo $e->getMessage();
				return false;
			}
		}
	}
	function __destruct(){
		unset ($this->_pdo);
	}
	
	/*function saveUser($email, $password){
	$sql = "INSERT INTO users (email, password)
				VALUES ('$email', '$password')";
	$result = $this->_pdo->exec($sql);
	if(!$result)
	return FALSE;
	return TRUE;							
	}	*/
	function getUser($email, $password){
		try{
			$sql = "SELECT email, password
							FROM users WHERE email = :email AND password = :password";
			if($stmt = $this->_pdo->prepare($sql))
			$stmt->bindParam(':email', $email, PDO::PARAM_STR);
			$stmt->bindParam(':password', $password, PDO::PARAM_STR);
			$stmt->execute();
			$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
	public function clearStr($data){
		return $this->_pdo->quote(trim(strip_tags($data)));
	}
	protected function db2Arr($data){
	$arr = [];
	while ($row = $data->fetch(PDO::FETCH_ASSOC))
	$arr[] = $row;
  return $arr;
	}
	
	function logOut(){
	session_destroy();
	header('Location: /');
	exit;
	}


	public function getIntro(){
		try{
			$sql = "SELECT id, head, text
							FROM intro ORDER BY id ASC";
			if($stmt = $this->_pdo->query($sql))
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
	function updateIntro($head, $text){
		$sql = "UPDATE OR IGNORE intro SET head=$head, text='$text'";
			
	$result = $this->_pdo->exec($sql);
	if(!$result)
	return FALSE;
	return TRUE;							
	}
		public function getAboutUs(){
		try{
			$sql = "SELECT id, head, text, img
							FROM aboutus ORDER BY id ASC";
			if($stmt = $this->_pdo->query($sql))
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
		function updateAboutUs($head, $text, $image){
		$sql = "UPDATE OR IGNORE aboutus SET head=$head, text='$text', img='$image'";
					if($_FILES['image']['tmp_name']){
		$nimg = $this->getAboutUs()[0]['img'];
		$f = __DIR__ . "/../../assets/images/$nimg";
		//var_dump($f); exit;
		if(file_exists($f)){
		unlink($f);
		}
		$file = $_FILES['image']['name'];
		move_uploaded_file($_FILES['image']['tmp_name'], __DIR__ . "/../../assets/images/$file");
		}
		$result = $this->_pdo->exec($sql);		
	if(!$result)
	return FALSE;
	return TRUE;							
	}
	
	public function getServicesH(){
		try{
			$sql = "SELECT id, head, desc
							FROM services_h";
			$stmt = $this->_pdo->query($sql);
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				unset($stmt);
				return $row;
		}catch(PDOException $e){
			return false;
		}
	}
	
	public function getServices(){
		try{
			$sql = "SELECT id, head, text
							FROM services ORDER BY id ASC";
			if($stmt = $this->_pdo->query($sql))
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}

	public function getOneService($id){
		try{
			$sql = "SELECT *
							FROM services WHERE id = $id";
			if($stmt = $this->_pdo->query($sql))
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				unset($stmt);
				return $row;
		}catch(PDOException $e){
			return false;
		}
	}
	
	function updateServices($head, $desc, $id, $headsv, $text){
		$sql = "UPDATE OR IGNORE services_h SET head=$head, desc=$desc"; 
		$sql1 = "UPDATE OR IGNORE services SET head=$headsv, text='$text' WHERE id = '$id'";
			
	$res = $this->_pdo->exec($sql);
	$res1 = $this->_pdo->exec($sql1);
	
	//var_dump($res, $res1); exit;
	if(!$res || !$res1)
	return FALSE;
	return TRUE;							
	}
	
  public function render($file) {
    ob_start();
    include(dirname(__FILE__) . '/' . $file);
    return ob_get_clean();
  }
}