<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'viewTemplatesDirectory' => '../templates',//twig
         'determineRouteBeforeAppMiddleware' => false,
         'db' => [
            'driver' => 'sqlite',
            'host' => 'localhost',
            'database' => '/home/sland.loc/src/sland.sqlite',
            'username' => '',
            'password' => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
                
        // Renderer settings PHP view
        /*'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],*/

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
