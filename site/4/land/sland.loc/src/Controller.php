<?php
namespace Src;

use Slim\Http\Request;
use Slim\Http\Response;

use Src\Service;
use Illuminate\Database\Capsule\Manager as DB;
//use Slim\Http\UploadedFile;

class Controller
{
    //
    public $name;
    protected $view;
    function __construct(\Slim\Views\Twig $view){
    	$this->view = $view;
    	$this->name = 'Guest';
    }
		
    public function index($request, $response){
		
	$intro = DB::table('intro')->select(['head', 'text'])->first();
	$aboutus = DB::table('aboutus')->select(['head', 'text', 'img'])->first();
	$services_h = DB::table('services_h')->select(['head', 'desc'])->first();
	$services = Service::select(['id', 'head', 'text'])->orderBy('id')->get();

//var_dump($intro); exit;
//$serviceses = DB::select('select * from articles where id = ?', array(1));
	 
if($_SERVER['REQUEST_METHOD']=='POST'){
	$result = '';
	//$yourname = trim(strip_tags($_POST['yourname']));
	if(empty($_POST['yourname']) || empty($_POST['youremail']) || empty($_POST['body'])):
	$result = 'Require all field';//необ., в html є require"
	else:
	if(mail('admin@my.loc', $_POST['yourname'], $_POST['youremail'], $_POST['body'])){
			$result = 'Message sends';
		unset($_POST['yourname'], $_POST['youremail'], $_POST['body']);
	}else{
	$result = 'Error send message';	
	}
	endif;
}

	return $this->view->render($response, 'default/index.html.twig', ['name'=>$this->name, 'intro'=>$intro, 'aboutus'=>$aboutus, 'services_h'=>$services_h, 'services'=>$services, 'result'=>$result]);
		}

public function admin($request, $response){
	
		$_SESSION['token'] = md5(uniqid(mt_rand(), true));
	
	$request = $request->withAttribute('session', $_SESSION);
	$session = $request->getAttribute('session');
	
	return $this->view->render($response, '/default/admin.html.twig', ['session'=>$session]);
		}
		
	public function login($request, $response){
	//$login = $_POST["login"];
	//$password = $_POST["password"];
	if ((isset($_SESSION['token'])) || ($_POST['token'] == $_SESSION['token'])){
	$login = $request->getParam('login');//getParsedBody()['login'];
	$password = $request->getParam('password');//getParsedBody()['password'];
	
	$user = DB::table('users')->select(['email', 'password'])->where('email', $login)->first();

	if(empty($login and $password)){
		$_SESSION['message'] = 'Заповніть всі поля!';
		}else{
				if ($user->email !== $login or $user->password !== md5($password)):
					$_SESSION['message'] = 'Невірний пароль чи ім\'я користувача!';
				else:
					$_SESSION['admin'] = TRUE;
					$_SESSION['message'] = 'Ви увійшли';
				return $response->withRedirect('/admin', 301);
				endif;
		}
return $response->withRedirect('/admin', 301);
}
	}

	public function register($request, $response){
	if($_SESSION['admin']):
	if ($_SERVER['REQUEST_METHOD']=='GET'){
		return $this->view->render($response, 'admin/register.html.twig');
		}else{
	$login = $_POST["login"];//$login = 'root';
	$password = md5($_POST["password"]);//$password = '1234';
//$sql = "INSERT INTO users (login, password)	VALUES ('$login', '$password')";
		DB::table('users')->insert(['email' => $login, 'password' => $password]);

	if(empty($login and $password)){
		$_SESSION['message'] = 'Заповніть всі поля!';
		}else{
				$_SESSION['message'] = 'Ви зареєстровані';
				return $response->withRedirect('/admin', 301);
		}
	}	
	else:
	return $response->withRedirect('/register', 301);
	endif;
}

	public function logout($request, $response){
	session_destroy();
	  return $response->withRedirect('/', 301);
	}
	
	public function homeedit($request, $response){
		if($_SESSION['admin']):
		$intro = DB::table('intro')->select(['head', 'text'])->first();
	if($_SERVER['REQUEST_METHOD']=='POST'){
	$data = $request->getParsedBody();
		//var_dump($data); exit;
			if(DB::table('intro')->update($data)){
				$_SESSION['message'] = 'Дані оновлено';
				return $response->withRedirect('/admin', 301);
			}	
	}
    return $this->view->render($response, 'default/homeedit.html.twig', [
         'intro'=>$intro]);
    else:
		return $response->withRedirect('/admin', 301);
		endif;     
	}
	
	public function aboutusedit($request, $response){
		if($_SESSION['admin']):
		$aboutus = DB::table('aboutus')->select(['head', 'text', 'img'])->first();
	if($_SERVER['REQUEST_METHOD']=='POST'){
	//$data = $request->getParsedBody();
	$head = $request->getParam('head');
	$text = $request->getParam('text');
	$filename = $request->getParam('img_old');
		//var_dump($head); exit;
		
		$files = $request->getUploadedFiles();
				if($_FILES['image']['name']){
			if(file_exists('assets/images/'.$filename))://$data['img_old'])):
			unlink('assets/images/'.$filename);
			endif;
		
			$file = $files['image'];
			$filename = $file->getClientFilename();
			//var_dump($filename); exit;
					if ($file->getError() === UPLOAD_ERR_OK){
			$file->moveTo('assets/images/'. $filename);
			  	}
			  }
			  $data = ['head'=>$head, 'text'=>$text, 'img'=>$filename];
			  //var_dump($data); exit;
			if(DB::table('aboutus')->update($data)){
				$_SESSION['message'] = 'Дані оновлено';
				return $response->withRedirect('/admin', 301);
			}		
	}
    return $this->view->render($response, 'default/aboutusedit.html.twig', [
         'aboutus'=>$aboutus]);
    else:
		return $response->withRedirect('/admin', 301);
		endif;     
	}
	
	public function servicesedit($request, $response, $arg){
		if($_SESSION['admin']):	
		$services_h = DB::table('services_h')->select(['head', 'desc'])->first();
		$services = Service::select(['id', 'head', 'text'])->orderBy('id')->get();
		$service = Service::select(['id', 'head', 'text'])->where('id', $arg)->first();
		//$service = Service::find($arg['id']);
		//var_dump($arg['id']); exit;
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$serv = new Service;
			//var_dump($serv); exit;
			//$data = $request->getParsedBody();
	$head_h = $request->getParam('head');
	$desc_h = $request->getParam('desc');
	$data = ['head'=>$head_h, 'desc'=>$desc_h];
	$head = $request->getParam('headsv');
	$text = $request->getParam('text');
	$data1 = ['head'=>$head, 'text'=>$text];
	//var_dump($data1); exit;
			if(DB::table('services_h')->update($data) && $serv->where('id', $arg['id'])->update($data1)){
				$_SESSION['message'] = 'Дані оновлено';
				return $response->withRedirect('/admin', 301);
			}	
		}
	  return $this->view->render($response, 'default/servicesedit.html.twig', [
	        'services_h'=>$services_h, 'services'=>$services, 'service'=>$service, 'id'=>(int)$arg['id']]);
	  else:
		return $response->withRedirect('/admin', 301);
		endif;       
	}
}