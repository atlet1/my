<?php
use Src\Controller;
//use Slim\Http\Request;
//use Slim\Http\Response;

$app->map(['GET', 'POST'], '/', 'Controller:index')->setName('index');

$app->map(['GET', 'POST'], '/admin', 'Controller:admin')->setName('admin');

$app->map(['GET', 'POST'], '/admin/homeedit', 'Controller:homeedit')->setName('homeedit');

$app->map(['GET', 'POST'], '/admin/aboutusedit', 'Controller:aboutusedit')->setName('aboutusedit');

$app->map(['GET', 'POST'], '/admin/servicesedit[/{id}]', 'Controller:servicesedit')->setName('servicesedit');


$app->post('/login', 'Controller:login')->setName('login');

$app->map(['GET', 'POST'], '/register', 'Controller:register')->setName('register');

$app->get('/logout', 'Controller:logout')->setName('logout');
