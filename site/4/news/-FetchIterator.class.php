<?php
class FetchIterator implements Iterator{
    
    public function __construct(){
      $this->fetchCallback = $fetchCallback;
			$this->count = 0;
    }
		public function current() {
      $this->count || $this->next();
			return $this->current;
    }
		public function key() {
      $this->count || $this->next();
			return $this->count - 1;
		}	
		 public function valid() {
      $this->count || $this->next();
			return $this->validate();
    }
    public function validate() {
     if (!$this->current || is_string($this->current))
      return false ;
      //return false != $this->current || is_string($this->current);
    }
	  public function next() {
			if ($this->count && ! $this->validate()) {
			return;
		}
			$this->fetch();
			$this->count++;
    }
		public function fetch() {
      $func = $this->fetchCallback;
			$this->current = $func();
		}
		public function rewind() {
        $this->current = $this->current;
    }
}
?>