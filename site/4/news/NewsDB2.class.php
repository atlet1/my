<?php
include "INewsDB.class.php";
class NewsDB implements INewsDB, IteratorAggregate{
  const DB_NAME = 'news.db';
  protected $_pdo;
  protected $_items = [];
  function __construct(){
  	$this->_pdo = new PDO("sqlite:".self::DB_NAME);
  	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    if(filesize(self::DB_NAME)<=0){
    	try{
    		$this->_pdo->beginTransaction();
				$sql = "CREATE TABLE msgs(
	                              id INTEGER PRIMARY KEY AUTOINCREMENT,
	                              title TEXT,
	                              category INTEGER,
	                              description TEXT,
	                              source TEXT,
	                              datetime INTEGER
	                          )";
	      $this->_pdo->exec($sql);// or die $this->_pdo->errorCode();
	      $sql = "CREATE TABLE category(
	                                  id INTEGER PRIMARY KEY AUTOINCREMENT,
	                                  name TEXT
	                              )";
	      $this->_pdo->exec($sql); //or die $this->_pdo->errorInfo();
	      $sql = "INSERT INTO category(id, name)
	                  SELECT 1 as id, 'Политика' as name
	                  UNION SELECT 2 as id, 'Культура' as name
	                  UNION SELECT 3 as id, 'Спорт' as name";
	      $this->_pdo->exec($sql);// or die $this->_pdo->lastErrorMsg();
	      $this->_pdo->commit();	
    	}catch(PDOException $e){
    		$this->_pdo->rollBack();
				echo "ERROR!";
				exit;
			}
		}
	$this->getCategory();
	}
	function getIterator(){
		return new ArrayIterator($this->_items);
	}
	protected function getCategory(){
		$sql ="SELECT id, name FROM category";
		$stmt = $this->_pdo->query($sql);
		while($r = $stmt->fetch(PDO::FETCH_ASSOC))
			$this->_items[$r['id']] = $r['name'];
	}	
  function __destruct(){
    unset($this->_pdo);
  }
  function saveNews($title, $category, $description, $source){
    $dt = time();
    $sql = "INSERT INTO msgs(title, category, description, source, datetime)
                VALUES($title, $category, $description, $source, $dt)";
    $ret = $this->_pdo->exec($sql);
    if(!$ret)
      return false;
  return true;	
  }
 	protected function db2Arr($data){
		$arr = [];
		while($row = $data->fetch(PDO::FETCH_ASSOC))
			$arr[] = $row;
		return $arr;	
	}
  public function getNews(){
    try{
      $sql = "SELECT msgs.id as id, title, category.name as category, description, source, datetime 
              FROM msgs, category
              WHERE category.id = msgs.category
              ORDER BY msgs.id DESC";
    $stmt = $this->_pdo->query($sql);
		if (is_object($stmt)) 
		return $this->db2Arr($stmt);
			}catch(PDOException $e){
			echo $e->getMessage();
      return false;
			}
	}	
  public function deleteNews($id){
    try{
      $sql = "DELETE FROM msgs WHERE id = $id";
      $result = $this->_pdo->exec($sql);
      if ($result)
      return true;
    }catch(PDOException $e){
      echo $e->getMessage();
      return false;
    }
  }
  function clearData($data){
			$data = trim(strip_tags($data));
      return $this->_pdo->quote($data); 
  }	
}