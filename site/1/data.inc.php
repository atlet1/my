<?php
	/*
1G
50M
1024K
4096
*/
$size = ini_get('post_max_size'); //50M в настройках сервера може бути
$letter = $size{strlen($size)-1}; // M
$size = (int)$size; //50 привів до типу цифр
switch(strtoupper($letter)){
	case 'G': $size *= 1024; //break; і в кінці можна default: echo "НВІДОМО СКІЛЬКИ БАЙТ"; break;
	case 'M': $size *= 1024;
	case 'K': $size *= 1024;
}
	define('ERR_DRAW_ON_LEFT_MENU', 'Вибачте, меню не доступне. Повідомте веб-мастеру');
	define('ERR_DRAW_ON_BOTTOM_MENU', '');
	/* Меню  Багатомірний масив - масив в масиві */
	$leftMenu = array(
					array('link'=>'Домой', 'href'=>'index.php'),
					array('link'=>'О нас', 'href'=>'index.php?id=about'),
					array('link'=>'Контакты', 'href'=>'index.php?id=contact'),
					array('link'=>'Таблица умножения', 'href'=>'index.php?id=table'),
					array('link'=>'Калькулятор', 'href'=>'index.php?id=calc')
				);
	/* Конструкції */
	$hour = (int)strftime('%H');
	$welcome = '';
	if ($hour>0 and $hour<6){
	$welcome = 'Доброї ночі';
	}	elseif ($hour > 0 and $hour < 6){
	$welcome = 'Доброї ночі';
	}	elseif ($hour == 0){
	/* щоб не переплутати оператор рівності з присвоєнням можна: elseif (0 == $hour) */
	$welcome = 'З початком ночі';
	}	elseif ($hour >= 6 and $hour < 12){
		$welcome = 'Добрий ранок';
	}	elseif ($hour >= 12 and $hour < 18){
		$welcome = 'Добрий день';
	}	elseif ($hour >= 18 and $hour < 23){
		$welcome = 'Добрий вечір';
	}	
	/* альтернатива без фігурних дужок (дужку можна закрити і після великого куска HTML і т. д. коду), якщо більше одної умови (якщо одна, можна без дужок, але не варто)
	$shop = 'close'; $kiosk = false
	if($shop == 'open'):
		echo 'Йду в магазин';
	elseif($kiosk):
		echo 'Йду в кіоск-1';
		else:
		echo 'Йду в кіоск-2';
	endif;
	*/
	/* Оголошення констант */
	define ('COPYRIGHT', 'Супер Мега Веб-мастер');
	// Вибираємо дату
	$locale = setlocale(LC_ALL, 'ukrainian');
	$day = strftime('%d');
	$mon = strftime('%B');
	if (stripos($locale, "1251") !== false) {
    $mon = iconv("windows-1251","utf-8", $mon);
	}
	$year = strftime('%Y');
?>