<?php
//error_reporting(0) 
require 'lib.inc.php';
require 'data.inc.php';
$title = 'Сайт нашої школи';
$header = "$welcome, Гість";
$id = strtolower(cleanStr($_GET['id'])); //strtolower(trim(strip_tags($_GET['id'])));
switch($id){
	case 'about':
		$title = 'Про сайт';
		$header= 'Про наш сайт'; break;
	case 'contact':
		$title = 'Контакти';
		$header= 'Зворотній зв\'язок'; break;
	case 'table':
		$title = 'Таблиця множення';
		$header= 'Таблиця множення'; break;
	case 'calc':
		$title = 'Он-лайн калькулятор';
		$header= 'Калькулятор'; break;
}

?>
<!-- Меню  $leftMenu = array - пішло в файл, що підключаємо  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="uk" lang="uk">
	<head>
		<title><?=$title?></title>
		<meta http-equiv="content-type"
			content="text/html; charset=windows-1251" />
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<body>

		<div id="header">
			<!-- Верхняя часть страницы -->
			<?php
				include 'top.inc.php';
			?>
			<!-- Верхняя часть страницы -->
		</div>

		<div id="content">
			<!-- Заголовок -->
			<h1><?=$header?></h1>
			<!-- <h1>?php=$welcome?, Гість</h1> -->
			<!-- Заголовок -->
			<!-- Область основного контента -->
			<?php
				//include 'index.inc.php';
				switch($id){
					case 'about':
						include 'about.php'; break;
					case 'contact':
						include 'contact.php'; break;
					case 'table':
						include 'table.php'; break;
					case 'calc':
						include 'calc.php'; break;
					default: include 'index.inc.php';
}
			?>
			<!-- Область основного контента -->
		</div>
		<div id="nav">
			<!-- Навигация -->
			<?php
				require 'menu.inc.php';
			?>
			<!-- Меню було
			// було з функцією
			drawMenu($leftMenu); 
			// З масивом
			echo "<ul>";
			foreach($leftMenu as $item){
				echo "<li>";
				echo "<a href='{$item['href']}'>{$item['link']}</a>";
				echo "</li>";
			}
			echo "<ul>";  -->
						<!-- Меню було, тепер зверху міняти
						<ul><?php /*
				<li><a href='<?=$leftMenu[0]['href']?>'><?=$leftMenu[0]['link']?></a></li>
				<li><a href='<?=$leftMenu[1]['href']?>'><?=$leftMenu[1]['link']?></a></li>
				<li><a href='<?=$leftMenu[2]['href']?>'><?=$leftMenu[2]['link']?></a></li>
				<li><a href='<?=$leftMenu[3]['href']?>'><?=$leftMenu[3]['link']?></a></li>
				<li><a href='<?=$leftMenu[4]['href']?>'><?=$leftMenu[4]['link']?></a></li>
			</ul> -->
			<!-- Простий масив <>
				  
				$leftMenu = array(
			 						'home'=>'index.php',
			 						'about'=>'about.php',
			 						'contact'=>'contact.php',
			 						"table"=>"table.php",
			 						'calc'=>"calc.php"); 
			 
			 <ul>
				<li><a href='<?=$leftMenu['home']?>'>Домой</a></li>
				<li><a href='<?=$leftMenu['about']?>'>О нас</a></li>
				<li><a href='<?=$leftMenu['contact']?>'>Контакты</a></li>
				<li><a href='<?=$leftMenu['table']?>'>Таблица умножения</a></li>
				<li><a href='<?=$leftMenu['calc']?>'>Калькулятор</a></li>
			</ul> --> */?>
			<!-- Навигация -->
		</div>
		<div id="footer">
			<!-- Нижняя часть страницы 
			було drawMenu($leftMenu, false); -->
			<?php
				include 'bottom.inc.php';
			?>
			<!-- Нижняя часть страницы  -->
		</div>
	</body>
</html>