<?php
	// подключение библиотек
	require "inc/EshopDB.class.php";
?>
<html>
<head>
	<title>Кошик покупця</title>
</head>
<body>
	<h1>Ваш кошик</h1>
<?php
$goods = $eshop->myBasket();
	if(!is_array($goods)){ // if($goods===false) 
	echo 'Виникла помилка при виведенні товарів';
	exit;
}
if($goods)
	echo '<p>Повернутись в <a href="catalog.php">каталог</a></p>';
else
	echo '<p>Кошик порожній! Поверніться в <a href="catalog.php">каталог</a></p>';
?>
<table border="1" cellpadding="5" cellspacing="0" width="100%">
<tr>
	<th>N п/п</th>
	<th>Назва</th>
	<th>Автор</th>
	<th>Рік видання</th>
	<th>Ціна, грн.</th>
	<th>Кількість</th>
	<th>Видалити</th>
</tr>
<?php
	$i = 1; $sum = 0;
	foreach($goods as $item){
?>
	<tr>
		<td><?= $i?></td>
		<td><?= $item['title']?></td>
		<td><?= $item['author']?></td>
		<td><?= $item['pubyear']?></td>
		<td><?= $item['price']?></td>
		<td><?= $item['quantity']?></td>
		<td><a href="delete_from_basket.php?id=<?= $item['id']?>">Видалити</a></td>
	</tr>
<?
	$i++;
	$sum += $item['price'] * $item['quantity'];
}
?>
</table>

<p>Всього товарів у кошику на суму: <?= $sum?> грн.</p>

<div align="center">
	<input type="button" value="Оформити замовлення!"
                      onClick="location.href='orderform.php'" />
</div>

</body>
</html>