<?php
	require "secure/session.inc.php";
	require "../inc/EshopDB.class.php";
?>
<html>
<head>
	<title>Отримані замовлення</title>
</head>
<body>
<h1>Отримані замовлення:</h1>
<?php
 $orders = $eshop->getOrders();
 //print_r($orders);
	foreach($orders as $order){
?>
<hr>
<h2>Замовлення номер: <?= $order['orderid']?></h2>
<p><b>Замовник</b>: <?= $order["name"]?></p>
<p><b>Email</b>: <?= $order["email"]?></p>
<p><b>Телефон</b>: <?= $order["phone"]?></p>
<p><b>Адреса доставки</b>: <?= $order["address"]?></p>
<p><b>Дата розміщення замовлення</b>: <?= date('d-m-Y H:i:s', $order["date"])?></p>

<h3>Куплені товари:</h3>
<table border="1" cellpadding="5" cellspacing="0" width="90%">
<tr>
	<th>N п/п</th>
	<th>Назва</th>
	<th>Автор</th>
	<th>Рік видання</th>
	<th>Ціна, грн.</th>
	<th>Кількість</th>
</tr>
<?php
	$i = 1; $sum = 0;
	foreach($order['goods'] as $item){
?>
	<tr>
		<td><?= $i?></td>
		<td><?= $item['title']?></td>
		<td><?= $item['author']?></td>
		<td><?= $item['pubyear']?></td>
		<td><?= $item['price']?></td>
		<td><?= $item['quantity']?></td>
	</tr>
<?
	$i++;
	$sum += $item['price'] * $item['quantity'];
}
?>

</table>
<p>Всього товарів у кошику на суму: <?= $sum?> грн.</p>
<?php
 } // end of big foreach
?>
</body>
</html>