<?
require_once "secure/session.inc.php";
require_once "secure/secure.inc.php";

$secure = new Secure();

if(isset($_GET['logout'])){
	$secure->logOut();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Адмінка</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>
<body>
	<h1>Адміністрування магазину</h1>
	<h3>Доступні дії:</h3>
	<ul>
		<li><a href='add2cat.php'>Додавання товару в каталог</a></li>
		<li><a href='orders.php'>Перегляд готових замовлень</a></li>
		<li><a href='secure/create_user.php'>Додати користувача</a></li>
		<li><a href='index.php?logout'>Завершити сеанс</a></li>
	</ul>
</body>
</html>