<?
require_once "session.inc.php";
require_once "secure.inc.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Хешування SHA-1</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>

<body>
<h1>Хешування SHA-1</h1>
<?
$secure = new Secure();
//$secure->createUser();

$user = 'root';
$string = '1234';
$salt = '';
$iterationCount = 100;
$result = '';

if (!$salt)
	$salt = str_replace('=', '', base64_encode(md5(microtime() . '1FD37EAA5ED9425683326EA68DCD0E59')));

if ($_SERVER['REQUEST_METHOD']=='POST'){
	$user = $_POST['user'] ?: $user;
	if(!$secure->userExists($user)){
		$string = $_POST['string'] ?: $string;
		$salt = $_POST['salt'] ?: $salt;
		$iterationCount = (int) $_POST['n'] ?: $iterationCount;
		$result = $secure->getHash($string, $salt, $iterationCount);
		if($secure->saveHash($user, $result, $salt, $iterationCount))
			$result = 'Хеш '. $result. ' успішно додано у файл';
		else
			$result = 'При запису хеша '. $result. ' виникла помилка';
	}else{
		$result = "Користувач $user вже існує. Виберіть інше і\'мя.";
	}
}
?>
<h3><?= $result?></h3>
<form action="<?= $_SERVER['PHP_SELF']?>" method="post">
	<div>
		<label for="txtUser">Логін</label>
		<input id="txtUser" type="text" name="user" value="<?= $user?>" style="width:40em"/>
	</div>
	<div>
		<label for="txtString">Пароль</label>
		<input id="txtString" type="text" name="string" value="<?= $string?>" style="width:40em"/>
	</div>
	<div>
		<label for="txtSalt">Сіль</label>
		<input id="txtSalt" type="text" name="salt" value="<?= $salt?>"  style="width:40em"/>
	</div>	
	<div>
		<label for="txtIterationCount">Число ітерацій</label>
		<input id="txtIterationCount" type="text" name="n" value="<?= $iterationCount?>"  style="width:4em"/>
	</div>		
	<div>
		<button type="submit">Створити</button>
	</div>	
</form>
</body>
</html>