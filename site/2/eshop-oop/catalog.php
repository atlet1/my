<?php
	// подключение библиотек
include "inc/EshopDB.class.php";
?>
<html>
<head>
	<title>Каталог товарів</title>
</head>
<body>
<p>Товарів у <a href="basket.php">кошику</a>: <?= $eshop->count1?></p>
<table border="1" cellpadding="5" cellspacing="0" width="100%">
<tr>
	<th>Назва</th>
	<th>Автор</th>
	<th>Рік видання</th>
	<th>Ціна, грн.</th>
	<th><a href="add2basket.php?id=<?= $item['id']?>">У кошик</a></th>
</tr>
<?php
$goods = $eshop->selectAllItems();
if(!is_array($goods)){ // if($goods===false) 
	echo 'Виникла помилка при виведенні товарів';
	exit;
}
if(!$goods){
	echo 'На сьогодні товарів немає';
	exit;
}	
foreach($goods as $item){
?>
	<tr>
		<td><?= $item['title']?></td>
		<td><?= $item['author']?></td>
		<td><?= $item['pubyear']?></td>
		<td><?= $item['price']?></td>
		<td><a href="add2basket.php?id=<?= $item['id']?>">У кошик</a></td>
	</tr>
<?
}
?>
</table>
</body>
</html>