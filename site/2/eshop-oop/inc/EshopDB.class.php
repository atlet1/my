<?php
header('Content-Type: text/html; charset=utf-8');
include "IEshopDB.class.php";
class EshopDB implements IEshopDB{
const DB_HOST = "localhost";
const DB_USER = "root";
const DB_PASSWORD = "";
const DB_NAME = "eshop";
const ORDERS_LOG = "orders.log";
public $basket = array();//Кошик покупця
public $count1 = 0;//Кількість товарів в кошику
protected $_pdo;//private
	
public function __construct(){
$this->_pdo = new PDO('mysql:host=' .self::DB_HOST.';dbname='.self::DB_NAME, self::DB_USER, self::DB_PASSWORD);//or die ('Помилка при з\'єднанні з базою!');
$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//_WARNING, _SILENT
//$this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		
/*$params = parse_ini_file('db_conf.ini');
$this->_pdo = new PDO($params['db.conn'], $params['db.user'], $params['db.pass']);
$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);*/

$this->basketInit();	
}		
public function __destruct(){
		//unset ($this->_pdo);
		$this->_pdo = NULL;
}	
/*public function clearStr($data){
		return $this->_pdo->quote(trim(strip_tags($data)));// бо викорстовую підготовані запроси
}	*/
public function clearInt($data){
		return abs((int)$data);
}		
	
	// Функция addItemToCatalog (сохранить в базу новый товар)
public function addItemToCatalog($title, $author, $pubyear, $price){
		$sql = "INSERT INTO catalog(
												title,
												author,
												pubyear,
												price)
								VALUES (:title, :author, :pubyear, :price)";
	if (!$stmt = $this->_pdo->prepare($sql))
	return false;
	$stmt->bindParam(':title', $title, PDO::PARAM_STR);
	$stmt->bindParam(':author', $author, PDO::PARAM_STR);
	$stmt->bindParam(':pubyear', $pubyear, PDO::PARAM_INT);
	$stmt->bindParam(':price', $price, PDO::PARAM_INT);
	$stmt->execute();
	unset($stmt);
	return true;
}
	
// Вывод всего из каталога
public function selectAllItems() {
	$sql = 'SELECT id, title, author, pubyear, price
					FROM catalog';
	if(!$result = $this->_pdo->query($sql))//or die ('Помилка в запросі!');
	return false;
	$items = $result->fetchAll(PDO::FETCH_ASSOC);
	unset($result);	
	return $items;
}

public function saveBasket(){
	$this->basket = base64_encode(serialize($this->basket));
	setcookie('basket', $this->basket, 0x7FFFFFFF);
}

// Определение количества товаров в корзине пользователя
public function basketInit(){
	if(!isset($_COOKIE['basket'])){
	$this->basket = array('orderid' => uniqid());
	$this->saveBasket();
	}else{
	$this->basket = unserialize(base64_decode($_COOKIE['basket']));
	// print_r($this->basket); exit; перевірка чи ідентифікатор є, більше не треба її
	$this->count1 = count($this->basket) - 1;
	}
}
	
	// Добавление товаров в корзину
public function add2Basket($id, $q) {
		$this->basket[$id] = $q;  //basket[$id] = 1; якщо без параметра q
		$this->saveBasket();
}
	
	// Вывод корзины пользователя
public function myBasket() {
	$goods = array_keys($this->basket);
	array_shift($goods);
	if(!$goods) // if(!count($goods))
		return array();
	// if(count($goods))
	$ids = implode(",", $goods);
	//else
		//$ids = 0;
	$sql = "SELECT id, author, title, pubyear, price
						FROM catalog
						WHERE id IN ($ids)";
	if(!$result = $this->_pdo->query($sql))
	return false;
	$items = $this->result2Array($result);
	unset($result);
	return $items;
}

protected function result2Array($result){// $data
	$arr = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)){//$row = mysqli_fetch_assoc($data))
	$row['quantity'] = $this->basket[$row['id']];
	$arr[] = $row;
	}
	return $arr;
}
	// Удаление товара из корзины
public function deleteItemFromBasket($id){
		unset($this->basket[$id]);
		$this->saveBasket();
}
	
	// Пересохранение товаров из корзины в заказы
public function saveOrder($datetime) {
	$goods = $this->myBasket();
	$sql = 'INSERT INTO orders(
												title,
												author,
												pubyear,
												price,
												quantity,
												orderid,
												datetime)
									VALUES (?, ?, ?, ?, ?, ?, ?)';
	if (!$stmt = $this->_pdo->prepare($sql))
		return false;
	foreach($goods as $item){
	$stmt->bindParam(1, $item['title'], PDO::PARAM_STR);
	$stmt->bindParam(2, $item['author'], PDO::PARAM_STR);
	$stmt->bindParam(3, $item['pubyear'], PDO::PARAM_INT);
	$stmt->bindParam(4, $item['price'], PDO::PARAM_INT);
	$stmt->bindParam(5, $item['quantity'], PDO::PARAM_INT);
	$stmt->bindParam(6, $this->basket['orderid'], PDO::PARAM_STR);
	$stmt->bindParam(7, $datetime, PDO::PARAM_INT);
	$stmt->execute();
	}
	unset($stmt);
	setcookie('basket', '', time()-3600);
	return true;
}
	
	// Получение информации о заказах
public function getOrders() {
		// Получение заказчиков из log-файла 
			if(!is_file(EshopDB::ORDERS_LOG))
			return false;
/* Получаем в виде массива персональные данные пользователей из файла */
		$orders = file(EshopDB::ORDERS_LOG);
/* Массив, который будет возвращен методом */		
		$allorders = array();
		
		foreach ($orders as $order) {
			list($name, $email, $phone, $address, $orderid, $date) = explode('|', $order);
/* Промежуточный массив для хранения информации о конкретном заказе */			
			$orderinfo = array();
/* Сохранение информацию о конкретном пользователе */			
			$orderinfo["name"] = $name;	
			$orderinfo["email"] = $email;	
			$orderinfo["phone"] = $phone;	
			$orderinfo["address"] = $address;	
			$orderinfo["orderid"] = $orderid;	
			$orderinfo["date"] = $date;	
/* SQL-запрос на выборку из таблицы orders всех товаров для конкретного покупателя */
			$sql = "SELECT title, author, pubyear, price, quantity 
					FROM orders 
					WHERE orderid = '$orderid'";// AND datetime = $date";
/* Получение результата выборки */					
			if(!$result = $this->_pdo->query($sql))
			return false;
			$items = $result->fetchAll(PDO::FETCH_ASSOC);
			unset($result);
/* Сохранение результата в промежуточном массиве */
			$orderinfo["goods"] = $items;
/* Добавление промежуточного массива в возвращаемый массив */	
			$allorders[] = $orderinfo;
		}
		return $allorders;
	}
}	
$eshop = new EshopDB();
?>