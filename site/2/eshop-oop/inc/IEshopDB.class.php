<?php
/**
*	IEshopDB
*		содержит основные методы для работы с магазином
*/
interface IEshopDB{
	/**
	*	bool addItemToCatalog(string $title, string $author, int $pubyear, int $price)
	*		добавляет новую запись в Гостевую книгу
	*	Параметры:
	*		$author - имя автора
	*		$title - название книги
	*		$pubyear - год публикации
	*		$price - цена
	*/
	function addItemToCatalog($title, $author, $pubyear, $price);
	/**
	*	array selectAllItems(void)
	*		возвращает массив содержащий все записи из каталога
	*/
	function selectAllItems();
	/**
	*	array getOrders()
	*		Получение информации о заказах
	*	
	*/
	function getOrders();
}
?>