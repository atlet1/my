<?php
	// запуск сессии
	session_start();
	// подключение библиотек
require "EshopDB.class.php";
?>
<html>
<head>
	<title>Отримані замовлення</title>
</head>
<body>
<h2>Отримані замовлення:</h2>
<?php
	$result = $eshop->getOrders();
	foreach ($result as $order) {
?>
<hr>
<p><b>Замовник</b>: <?php echo $order["name"] ?></p>
<p><b>Email</b>: <?php echo $order["email"] ?></p>
<p><b>Телефон</b>: <?php echo $order["phone"] ?></p>
<p><b>Адреса доставки</b>: <?php echo $order["address"] ?></p>
<p><b>Дата розміщення замовлення</b>: <?php echo date("d.m.y H:i", $order["date"]) ?></p>
<h3>Куплені товари:</h3>
<table border="1" cellpadding="5" cellspacing="0" width="90%">
<tr>
	<th>N п/п</th>
	<th>Автор</th>
	<th>Назва</th>
	<th>Рік видання</th>
	<th>Ціна, грн.</th>
	<th>Кількість</th>
</tr>
<?php
	$i = 0;
	$sum = 0;
	while ($row = $order["goods"]->fetch(PDO::FETCH_ASSOC)){
	$sum += $row["price"] * $row["quantity"];
?>
	<tr>
		<td align="center"><?php echo ++$i ?></td>
		<td><?php echo $row["author"] ?></td>
		<td><?php echo $row["title"] ?></td>
		<td align="center"><?php echo $row["pubyear"] ?></td>
		<td align="center"><?php echo $row["price"] ?></td>
		<td align="center"><?php echo $row["quantity"] ?></td>
	</tr>
<?php
	}
?>
	</table>
	<p>Всього товарів у кошику на суму: <?php echo $sum ?> грн.
<?php
	}
?>
</body>
</html>