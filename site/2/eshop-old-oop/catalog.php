<?php
// запуск сессии
session_start();
header("Content-Type: text/html;charset=utf-8");
include "EshopDB.class.php";
//$eshop = new EshopDB();
?>
<html>
<head>
	<title>Каталог товарів</title>
</head>
<body>
<p>Товарів у <a href="basket.php">кошику</a>:
<?php
	echo $eshop->basketInit();
?>
</p>
<table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
	<th>Автор</th>
	<th>Назва</th>
	<th>Рік видання</th>
	<th>Ціна, грн.</th>
	<th>У кошик</th>
</tr>
<?php
	$result = $eshop->selectAll();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)){
?>
	<tr>
		<td><?php echo $row["author"] ?></td>
		<td><?php echo $row["title"] ?></td>
		<td align="center"><?php echo $row["pubyear"] ?></td>
		<td align="center"><?php echo $row["price"] ?></td>
		<td align="center">
		    <a href="add2basket.php?id=<?php echo $row["id"] ?>">
		    Додати</a></td>
	</tr>
<?php
	}
?>
</table>
</body>
</html>