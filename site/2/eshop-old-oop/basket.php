<?php
	// запуск сессии
	session_start();
	// подключение библиотек
	require "EshopDB.class.php";
?>
<html>
<head>
	<title>Кошик покупця</title>
</head>
<body>
<?php
	if ($eshop->basketInit() == 0) {
		echo "<p>Кошик порожній! Перейти в <a href=\"catalog.php\">каталог</a> товарів.</p>";
	} else {
?>
<table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
	<th>N п/п</th>
	<th>Автор</th>
	<th>Назва</th>
	<th>Рік видання</th>
	<th>Ціна, грн.</th>
	<th>Кількість</th>
	<th>Видалити</th>
</tr>
<?php
	$result = $eshop->myBasket();
	$i = 0;
	$sum = 0;
	while ($row = $result->fetch(PDO::FETCH_ASSOC)){
	$sum += $row["price"] * $row["quantity"];
?>
	<tr>
		<td align="center"><?php echo ++$i ?></td>
		<td><?php echo $row["author"] ?></td>
		<td><?php echo $row["title"] ?></td>
		<td align="center"><?php echo $row["pubyear"] ?></td>
		<td align="center"><?php echo $row["price"] ?></td>
		<td align="center"><?php echo $row["quantity"] ?></td>
		<td align="center">
		    <a href="delete_from_basket.php?id=<?php echo $row["id"] ?>">
		    Видалити</a></td>
	</tr>
<?php
	}
?>
</table>

<p>Всього товарів у кошику на суму:</p>
<?php echo $sum ?>
грн.

<div align="center">
	<input type="button" value="Оформити замовлення!"
                      onClick="location.href='orderform.php'">
</div>
<?php
}
?>
</body>
</html>