<?php
	// запуск сессии
	session_start();
	// подключение библиотек
	require "EshopDB.class.php";
	// ID удаляемого товара
	$id = (abs(int)($_GET['id']));
	
	// Удаление товара из корзины
	$eshop->basketDel($id);
	
	header('Location: basket.php');
?>