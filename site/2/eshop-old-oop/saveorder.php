<?php
	// запуск сессии
	session_start();
	// подключение библиотек
	require "EshopDB.class.php";
	// Получение данных о заказе
	$name = $eshop->clearLogData($_POST["name"]);
	$email = $eshop->clearLogData($_POST["email"]);
	$phone = $eshop->clearLogData($_POST["phone"]);
	$address = $eshop->clearLogData($_POST["address"]);
	$customer = session_id();
	$datetime = time();
	if(!empty ($name) and !empty ($email) and !empty ($phone) and !empty ($address)){
	//Создание строки из полученных данных
	$data = "$name|$email|$phone|$address|$customer|$datetime\r\n";
	//Сохранение данных в файл
	file_put_contents(EshopDB::ORDERS_LOG, $data, FILE_APPEND);	
	
	// Пересохранение купленных товаров из корзины в таблицу orders
	$eshop->resave($datetime);
	}
?>
	
<html>
<head>
	<title>Збереження даних замовлення</title>
</head>
<body>
	<p>Ваше замовлення прийняте.</p>
	<p><a href="catalog.php">Каталог товарів</a></p>
</body>
</html>