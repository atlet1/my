<?php
include "IEshopDB.class.php";
class EshopDB implements IEshopDB{
const DB_HOST = "localhost";
const DB_USER = "root";
const DB_PASSWORD = "";
const DB_NAME = "eshop1";
const ORDERS_LOG = "orders.log";
public $count1 = 0;
protected $_pdo;//private
	
public function __construct(){
$this->_pdo = new PDO('mysql:host=' .self::DB_HOST.';dbname='.self::DB_NAME, self::DB_USER, self::DB_PASSWORD);
$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//_WARNING, _SILENT
//$this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		
/*$params = parse_ini_file('db_conf.ini');
$this->_pdo = new PDO($params['db.conn'], $params['db.user'], $params['db.pass']);
$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);*/

$this->basketInit();	
}		
public function __destruct(){
		//unset ($this->_pdo);
		$this->_pdo = NULL;
}	
public function clearBdData($data){
		$data = stripslashes($data);
		$data = strip_tags($data);
		$data = trim($data);
		return $this->_pdo->quote($data);
		//return $this->_pdo->quote(trim(strip_tags($data)));
	}	
public	function clearLogData($data){
		$data = stripslashes($data);
		$data = strip_tags($data);
		$data = trim($data);
		return $data;
	}	
// Определение количества товаров в корзине пользователя
public function basketInit(){
try{
$sql = "SELECT count(*) FROM basket WHERE customer='".session_id()."'";
	
/*$sql = 'SELECT email FROM users WHERE id = ? AND name = ?';
$stmt = $db->prepare($sql);
$stmt->execute([1, 'John']);//execute([$_GET['id']]);
$john = $stmt->fetchAll();//Получаем массив массивов */

//while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT))//курсор
//$stmt = $db->query($sql);

$stmt = $this->_pdo->query($sql);//catch(PDOException $e){$e->getMessage();}
$row = $stmt->fetch(PDO::FETCH_NUM);
//$arr = $stmt->fetchAll(PDO::FETCH_COLUMN, 0)// Выбираем данные только из первого поля
$count1 = $row[0];
return $count1;
	}catch(PDOException $e){
		echo $e->getMessage();
		return false;
	}
}
	
	// Функция save (сохранить в базу новый товар)
public function save($author, $title, $pubyear, $price) {
		$sql = "INSERT INTO catalog(
						author,
						title,
						pubyear,
						price
					) VALUES(
						$author,
						$title,
						$pubyear,
						$price					
					)";
		$result = $this->_pdo->exec($sql);//or die ('Помилка в запросі!');
				if (!$result) 
				return false;
	return true;
}
	
// Вывод всего из каталога
public function selectAll() {
	$sql = "SELECT * FROM catalog";
	$result = $this->_pdo->query($sql);//or die ('Помилка в запросі!');
	return $result;
}
	
	// Добавление товаров в корзину
public function add2basket($customer, $goodsid, $quantity, $datetime) {
		$sql = "INSERT INTO basket(
					customer,
					goodsid,
					quantity,
					datetime
				) VALUES(
					'$customer',
					$goodsid,
					$quantity,
					$datetime				
				)";
		$result = $this->_pdo->exec($sql);
				if (!$result) 
				return false;
	return true;
}
	
	// Вывод корзины пользователя
public function myBasket() {
		$sql = "SELECT * FROM catalog, basket 
			WHERE customer ='".session_id()."' 
			and catalog.id=basket.goodsid";
		$result = $this->_pdo->query($sql);
	return $result;
}
	
	// Удаление товара из корзины
public function basketDel($id){
		$sql = "DELETE FROM basket WHERE id = $id";
		$result = $this->_pdo->exec($sql);
				if (!$result) 
				return false;
	return true;
}
	
	// Пересохранение товаров из корзины в заказы
public function resave($datetime) {
		$goods = $this->myBasket();
		while ($good = $goods->fetch(PDO::FETCH_ASSOC)) {
			$sql = "INSERT INTO orders(
						author,
						title,
						pubyear,
						price,
						customer,
						quantity,
						datetime
					) VALUES(
						'" . $good["author"] . "',
						'" . $good["title"] . "',
						" . $good["pubyear"] . ",
						" . $good["price"] . ",
						'".session_id()."',
						" . $good["quantity"] . ", $datetime)";
			$result = $this->_pdo->exec($sql);//" . $good["customer"] . ",
				if (!$result) 
				return false;
			//return true;// з цим тру не витирає з кошика
		}
// Удаление данных из таблицы basket
		$sql= "DELETE FROM basket WHERE customer='".session_id()."'";	
		$result = $this->_pdo->exec($sql);
				if (!$result) 
				return false;
	return true;
}
	
	// Получение информации о заказах
public function getOrders() {
		// Получение заказчиков из log-файла 
		$orders = file(EshopDB::ORDERS_LOG);
		
		$allorders = array();
		
		foreach ($orders as $order) {
			list($name, $email, $phone, $address, $customer, $date) = explode("|", $order);
			
			$orderinfo = array();
			
			$orderinfo["name"] = $name;	
			$orderinfo["email"] = $email;	
			$orderinfo["phone"] = $phone;	
			$orderinfo["address"] = $address;	
			$orderinfo["customer"] = $customer;	
			$orderinfo["date"] = $date;	
			// Товары из заказов:
			$sql = "SELECT * FROM orders 
							WHERE customer='".$orderinfo["customer"]."'";// AND datetime=".$orderinfo["date"];
			$result = $this->_pdo->query($sql);
			$orderinfo["goods"] = $result;
			$allorders[] = $orderinfo;
		}
		return $allorders;
	}
}	
$eshop = new EshopDB();
?>