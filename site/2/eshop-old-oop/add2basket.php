<?php
	// запуск сессии
	session_start();
	// подключение библиотек
	require "EshopDB.class.php";
	// Покупатель
	$customer = session_id();
	// Получить id товара, добавляемого в корзину
	$goodsid = (abs(int)($_GET["id"]));
	// Количество товара
	$quantity = 1;
	// Дата добавления товара в корзину
	$datetime = time();
	
	$eshop->add2basket($customer, $goodsid, $quantity, $datetime);
	
	header("Location: catalog.php");
?>