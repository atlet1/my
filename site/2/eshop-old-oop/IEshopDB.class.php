<?php
/**
*	IEshopDB
*		содержит основные методы для работы с магазином
*/
interface IEshopDB{
	/**
	*	bool save(string $author, string $title, int $pubyear, int $price)
	*		добавляет новую запись в Гостевую книгу
	*	Параметры:
	*		$author - имя автора
	*		$title - название книги
	*		$pubyear - год публикации
	*		$price - цена
	*/
	function save($author, $title, $pubyear, $price);
	/**
	*	array selectAll(void)
	*		возвращает массив содержащий все записи из каталога
	*/
	function selectAll();
	/**
	*	array getOrders()
	*		Получение информации о заказах
	*	
	*/
	function getOrders();
}
?>