<?php
// Создание структуры Базы Данных гостевой книги

define("DB_HOST", "localhost");
define("DB_LOGIN", "root");
define("DB_PASSWORD", "");

$link = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASSWORD) or die(mysqli_error($link));

$sql = 'CREATE DATABASE gbook';
mysqli_query($link, $sql) or die(mysqli_error($link));

mysqli_select_db($link, 'gbook') or die(mysqli_error($link));
$sql = "
CREATE TABLE msgs (
	id int(11) NOT NULL auto_increment,
	name varchar(50) NOT NULL default '',
	email varchar(50) NOT NULL default '',
	msg TEXT,
	datetime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ip varchar(50) NOT NULL default '',
	PRIMARY KEY (id)
)";//datetime int(11) NOT NULL default 0,
mysqli_query($link, $sql) or die(mysqli_error($link));

mysqli_close($link);

print '<p>Структура базы данных успешно создана!</p>';
?>