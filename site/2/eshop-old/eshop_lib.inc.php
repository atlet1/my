<?php
	// Функция save (сохранить в базу новый товар)
	function save($author, $title, $pubyear, $price) {
		global $link;
		$sql = "INSERT INTO catalog(
						author,
						title,
						pubyear,
						price
					) VALUES(
						'$author',
						'$title',
						$pubyear,
						$price					
					)";
		mysqli_query($link, $sql) or die(mysqli_error($link));
	}
	
	// Вывод всего из каталога
	function selectAll() {
		global $link;
		$sql = "SELECT * FROM catalog";
		$result = mysqli_query($link, $sql) or die(mysqli_error($link));
		return $result;
	}
	
	// Добавление товаров в корзину
	function add2basket($customer, $goodsid, $quantity, $datetime) {
		global $link;
		$sql = "INSERT INTO basket(
					customer,
					goodsid,
					quantity,
					datetime
				) VALUES(
					'$customer',
					$goodsid,
					$quantity,
					$datetime				
				)";
		$result = mysqli_query($link, $sql) or die(mysqli_error($link));
	}
	
	// Вывод корзины пользователя
	function myBasket() {
		global $link;
		$sql = "SELECT * FROM catalog, basket 
			WHERE customer ='".session_id()."' 
			and catalog.id=basket.goodsid";
		$result = mysqli_query($link, $sql) or die(mysqli_error($link));
		return $result;
	}
	
	// Удаление товара из корзины
	function basketDel($id){
		global $link;
		$sql = "DELETE FROM basket WHERE id = $id";
		$result = mysqli_query($link, $sql) or die(mysqli_error($link));
	}
	
	// Пересохранение товаров из корзины в заказы
	function resave($datetime) {
		global $link;
		$goods = myBasket();
		while ($good = mysqli_fetch_assoc($goods)) {
			$sql = "INSERT INTO orders(
						author,
						title,
						pubyear,
						price,
						customer,
						quantity,
						datetime
					) VALUES(
						'" . $good["author"] . "',
						'" . $good["title"] . "',
						" . $good["pubyear"] . ",
						" . $good["price"] . ",
						'".session_id()."',
						" . $good["quantity"] . ", $datetime)";
				mysqli_query($link, $sql) or die(mysqli_error($link));//" . $good["customer"] . ",
				
		}
		// Удаление данных из таблицы basket
		$sql= "DELETE FROM basket WHERE customer='".session_id()."'";	
		mysqli_query($link, $sql) or die(mysqli_error($link));
	}
	
	// Получение информации о заказах
	function getOrders() {
		global $link;
		// Получение заказчиков из log-файла 
				$orders = file(ORDERS_LOG);
		
		$allorders = array();
		
		foreach ($orders as $order) {
			list($name, $email, $phone, $address, $customer, $date) = explode("|", $order);
			
			$orderinfo = array();
			
			$orderinfo["name"] = $name;	
			$orderinfo["email"] = $email;	
			$orderinfo["phone"] = $phone;	
			$orderinfo["address"] = $address;	
			$orderinfo["customer"] = $customer;	
			$orderinfo["date"] = $date;	
			// Товары из заказов:
			$sql = "SELECT * FROM orders 
							WHERE customer='".$orderinfo["customer"]."'";// AND datetime=".$orderinfo["date"];
			$result = mysqli_query($link, $sql) or die(mysqli_error($link));
			$orderinfo["goods"] = $result;
			$allorders[] = $orderinfo;
		}
		return $allorders;
	}
?>