<?php
class Gbook {
	const DB_NAME = 'gbook.db';
	protected $_pdo;
	function __construct() {
	if(is_file(self::DB_NAME)){
	$this->_pdo = new PDO('sqlite:'.self::DB_NAME);
	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}else{
			try{
	$this->_pdo = new PDO('sqlite:'.self::DB_NAME);
	$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "CREATE TABLE msgs(
	id INTEGER PRIMARY KEY,
	name TEXT,
	email TEXT,
	msg TEXT,
	datetime INTEGER,
	ip TEXT)";
	if($this->_pdo->exec($sql))
	return true;
			}catch(PDOException $e){
				echo $e->getMessage();
				return false;
			}
		}
	}
	function __destruct(){
		unset ($this->_pdo);
	}
	function clearData($data){
		$data = trim(strip_tags($data));
		return $this->_pdo->quote($data); 
	}
	function save($name, $email, $msg){
		$dt = time();
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = "INSERT INTO msgs(name, email, msg, datetime, ip)
									VALUES ($name, $email, $msg, $dt, '$ip')";
		$result = $this->_pdo->exec($sql);
		if(!$result)
		return FALSE;
		return TRUE;							
	}
	function add(){
	$name = $this->clearData($_POST["name"]);
	$email = $this->clearData($_POST["email"]);
	$msg = $this->clearData($_POST["msg"]);
	if(!empty ($name) and !empty ($email) and !empty ($msg)){
		if($this->save($name, $email, $msg)){
			header("Location: gbook.php");
		}else{	
		echo "Виникла помилка при додаванні повідомлення";
		exit;
		}	
	}else{
	echo "Заповніть всі поля форми!";
	}
}
	function getAll(){
		$sql = "SELECT id, name, email, msg, datetime, ip FROM msgs ORDER BY id DESC";
		$result = $this->_pdo->query($sql);
		$arr = [];
		while ($stmt = $result->fetch(PDO::FETCH_ASSOC))
		$arr[] = $stmt;
		return $arr;
	}
	function showAll(){
	if($_SERVER["REQUEST_METHOD"]=="POST"){
		$this->add();
		}
	$id = abs((int)$_GET['del']);
	if($id){
	$this->delete($id);
	header("Location: gbook.php");
	}	
echo <<<FORM
		<!DOCTYPE HTML>
		<html>
		<head>
		<title>Гостьова книга</title>
		</head>
			<h1>Гостьова книга</h1><p>
			<form action="" method="post">
			Ваше ім'я:<br />
			<input type="text" name="name" /><br />
			Ваш e-mail:<br />
			<input type="text" name="email" /><br />
			Повідомлення:<br />
			<textarea name="msg" cols="50" rows="5"></textarea><br />
			<br />
			<input type="submit" value="Додати!" />
			</form>
FORM;
	if(!is_array($this->getAll())){
	$errMsg = "Виникла помилка при виведенні записів";
}else{
	echo "<p>Всього записів у Гостьовій книзі: ".count($this->getAll())."</p>";
	foreach($this->getAll() as $user){
		$id = $user['id'];
		$name = $user['name'];
		$email = $user['email'];
		$msg = nl2br($user['msg']);
		$ip = $user['ip'];
		$datetime = date("d-m-Y H:i:s",$user['datetime']*1);

echo <<<LABEL
		<hr>
		<p>
			<b><a href="mailto:$email">$name</a></b> from[$ip] @ $datetime
			<br />$msg
		</p>
		<p align="right">
			<a href="gbook.php?del=$id">Видалити</a>
		</p>
		</html>
LABEL;
	}
}
}
	function delete($id){
		try{
		$sql = "DELETE FROM msgs WHERE id = $id";
		$result = $this->_pdo->exec($sql);
		if ($result)
			return true;
		}catch(PDOException $e){
			echo $e->getMessage();
			return false;
		}
	}
}

$gb = new Gbook();
$gb->showAll();
?>
