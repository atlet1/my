<?php
$board = new MessageBoard();
$board->go();
class MessageBoard {
    protected $_db;
    protected $form_errors = array();
    protected $inTransaction = false;
    public function __construct() {
    	  set_exception_handler(array($this,'logAndDie'));
        $this->_db = new PDO('sqlite:message.db');//sqlite:/tmp/message.db
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    public function go() {
        // Значение $_REQUEST['cmd'] определяет выполняемую команду
        $cmd = isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : 'show';
        switch ($cmd) {
            case 'read':          // Чтение отдельного сообщения
              $this->read();
              break;
            case 'post':          // Отображение формы для ввода сообщения
              $this->post();
              break;
            case 'save':          // Сохранение отправленного сообщения
              if ($this->valid()) { // Если сообщение прошло проверку,
                  $this->save();    // сохранить его
                  $this->show();    // и вывести список сообщений
              } else {
                  $this->post();    // Иначе снова открыть форму отправки
              }
              break;
            case 'show':          // Вывести список сообщений по умолчанию
            default:
              $this->show();
              break;
        }
    }
    // save() сохраняет сообщение в базе данных
    protected function save() {
        $parent_id = isset($_REQUEST['parent_id']) ?
                     intval($_REQUEST['parent_id']) : 0;
        // Сообщение не должно изменяться в то время,
        // когда мы работаем с ним.
        $this->_db->beginTransaction();
        $this->inTransaction = true;
        // Сообщение является ответом?
        if ($parent_id) {
            // Получить обсуждение, уровень и thread_pos 
            // родительского сообщения
            $st = $this->_db->prepare("SELECT thread_id,level,thread_pos
                                FROM message WHERE id = ?");
            $st->execute(array($parent_id));
            $parent = $st->fetch();
            // Уровень ответа на 1 больше уровня родителя
            $level = $parent['level'] + 1;
            /* Чему равно максимальное значение  thread_pos среди всех
               сообщений данного обсуждения, имеющих того же родителя? */
            $st = $this->_db->prepare('SELECT MAX(thread_pos) FROM message
                    WHERE thread_id = ? AND parent_id = ?');
            $st->execute(array($parent['thread_id'], $parent_id));
            $thread_pos = $st->fetchColumn(0);
            // Имеются ли существующие ответы на то же
            // родительское сообщение?
            if ($thread_pos) {
                // Это значение thread_pos следует за максимальным
                // из существующих
                $thread_pos++;
            } else {
                // Это первый ответ, поместить его сразу после родителя
                $thread_pos = $parent['thread_pos'] + 1;
            }
            /* Увеличение thread_pos всех сообщений обсуждения, которые
            следуют за этим */
            $st = $this->_db->prepare('UPDATE message SET thread_pos = thread_pos
                                  + 1 WHERE thread_id = ? AND thread_pos >= ?');
            $st->execute(array($parent['thread_id'], $thread_pos));
            // Новое сообщение должно сохраняться со значением thread_id
            // своего родителя
            $thread_id = $parent['thread_id'];
        } else {
            // Сообщение не является ответом, а значит,
            // открывает новое обсуждение
            $thread_id = $this->_db->query('SELECT MAX(thread_id) + 1 FROM
                                          message')->fetchColumn(0);
            // Если строки данных еще не существуют,
            // начать со значения thread_id = 1
            if (! $thread_id) {
                $thread_id = 1;
            }
            $level = 0;
            $thread_pos = 0;
        }
       /* Вставка сообщения в базу данных. Использование prepare()
          и execute() обеспечивает необходимое экранирование всех полей */
       $st = $this->_db->prepare("INSERT INTO message (id,thread_id,parent_id,
                      thread_pos,posted_on,level,author,subject,body)
                      VALUES (?,?,?,?,?,?,?,?,?)");
       $st->execute(array(null,$thread_id,$parent_id,$thread_pos,
                          date('c'),$level,$_REQUEST['author'],
                          $_REQUEST['subject'],$_REQUEST['body']));
       // Закрепление всех операций
       $this->_db->commit();
       $this->inTransaction = false;
   }
   // show() выводит список всех сообщений
   protected function show() {
       print '<h2>Message List</h2><p>';
       /* Сообщения упорядочиваются по обсуждению (thread_id)
       и позиции в обсуждении (thread_pos) */
       $st = $this->_db->query("SELECT id,author,subject,LENGTH(body)
             AS body_length,posted_on,level FROM message
                      ORDER BY thread_id,thread_pos");
       while ($row = $st->fetch()) {
           // Сообщения с уровнем > 0 выводятся с отступами
           print str_repeat('&nbsp;',4 * $row['level']);
           // Вывод информации о сообщении со ссылкой для его чтения
           $when = date('Y-m-d H:i', strtotime($row['posted_on']));
           print "<a href='" . htmlentities($_SERVER['PHP_SELF']) .
           "?cmd=read&amp;id={$row['id']}'>" .
           htmlentities($row['subject']) . '</a> by ' .
           htmlentities($row['author']) . ' @ ' .
            htmlentities($when) .
            " ({$row['body_length']} bytes) <br/>";
        }
        // Для отправки сообщений, которые не являются ответами
        print "<hr/><a href='" .
              htmlentities($_SERVER['PHP_SELF']) .
              "?cmd=post'>Start a New Thread</a>";
    }
    // read() выводит отдельное сообщение
    public function read() {
        /* Убедиться в том, что переданный идентификатор является 
           целым числом и действительно соответствует сообщению */
        if (! isset($_REQUEST['id'])) {
            throw new Exception('No message ID supplied');
        }
        $id = intval($_REQUEST['id']);
        $st = $this->_db->prepare("SELECT author,subject,body,posted_on
                                 FROM message WHERE id = ?");
        $st->execute(array($id));
        $msg = $st->fetch();
        if (! $msg) {
            throw new Exception('Bad message ID');
        }
        /* Не отображать разметку HTML, введенную пользователем,
           но вывести символы новой строки как разрывы строк HTML */
        $body = nl2br(htmlentities($msg['body']));
        // Вывести сообщения со ссылками для ответа
        // и возврата к списку  сообщений
        $self = htmlentities($_SERVER['PHP_SELF']);
        $subject = htmlentities($msg['subject']);
        $author = htmlentities($msg['author']);
        print<<<_HTML_
<h2>$subject</h2>
<h3>by $author</h3>
<p>$body</p>
<hr/>
<a href="$self?cmd=post&parent_id=$id">Reply</a>
<br/>
<a href="$self?cmd=list">List Messages</a>
_HTML_;
    }
    // post() отображает форму для отправки сообщений
    public function post() {
        $safe =  array();
        foreach (array('author','subject','body') as $field) {
            // Экранирование в значениях полей по умолчанию
            if (isset($_POST[$field])) {
                $safe[$field] = htmlentities($_POST[$field]);
            } else {
                $safe[$field] = '';
            }
            // Сообщения об ошибках выводятся красным цветом
            if (isset($this->form_errors[$field])) {
                $this->form_errors[$field] = '<span style="color: red">' .
                   $this->form_errors[$field] . '</span><br/>';
            } else {
                $this->form_errors[$field] = '';
            }
        }
        // Сообщение является ответом?
        if (isset($_REQUEST['parent_id']) &&
        $parent_id = intval($_REQUEST['parent_id'])) {
        // Передать parent_id при отправке данных формы
        $parent_field =
            sprintf('<input type="hidden" name="parent_id" value="%d" />',
                    $parent_id);
        // Если тема не указана, использовать тему родительского сообщения
        if (! strlen($safe['subject'])) {
             $st = $this->_db->prepare('SELECT subject FROM message WHERE
                          id = ?');
             $st->execute(array($parent_id));
             $parent_subject = $st->fetchColumn(0);
            /* Вставить префикс 'Re: ' в тему родительского сообщения,
               если она существует и еще не имеет префикса 'Re:' */
            $safe['subject'] = htmlentities($parent_subject);
            if ($parent_subject && (! preg_match('/^re:/i',$parent_subject))) {
                $safe['subject'] = "Re: {$safe['subject']}";
            }
          }
        } else {
            $parent_field = '';
        }
    // Отображение формы с ошибками и значениями по умолчанию
    $self = htmlentities($_SERVER['PHP_SELF']);
    print<<<_HTML_
<form method="post" action="$self">
<table>
<tr>
 <td>Your Name:</td>
 <td>{$this->form_errors['author']}
     <input type="text" name="author" value="{$safe['author']}" />
</td>
<tr>
 <td>Subject:</td>
 <td>{$this->form_errors['subject']}
     <input type="text" name="subject" value="{$safe['subject']}" />
</td>
<tr>
 <td>Message:</td>
 <td>{$this->form_errors['body']}
     <textarea rows="4" cols="30" wrap="physical"
               name="body">{$safe['body']}</textarea>
</td>
<tr><td colspan="2"><input type="submit" value="Post Message" /></td></tr>
</table>
$parent_field
<input type="hidden" name="cmd" value="save" />
</form>
_HTML_;
    }
    // validate() проверяет, что каждое поле содержит данные
    public function valid() {
        $this->form_errors = array();
        if (! (isset($_POST['author']) && strlen(trim($_POST['author'])))) {
            $this->form_errors['author'] = 'Please enter your name.';
        }
        if (! (isset($_POST['subject']) && strlen(trim($_POST['subject'])))) {
            $this->form_errors['subject'] = 'Please enter a message subject.';
        }
        if (! (isset($_POST['body']) && strlen(trim($_POST['body'])))) {
            $this->form_errors['body'] = 'Please enter a message body.';
        }
        return (count($this->form_errors) == 0);
    }
    public function logAndDie(Exception $e) {
        print 'ERROR: ' . htmlentities($e->getMessage());
        if ($this->_db && $this->_db->inTransaction()) {
            $this->_db->rollback();
        }
        exit();
    }
}
/*
$sql = "CREATE TABLE message (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  posted_on DATETIME NOT NULL,
  author CHAR(255),
  subject CHAR(255),
  body MEDIUMTEXT,
  thread_id INT UNSIGNED NOT NULL,
  parent_id INT UNSIGNED NOT NULL,
  level INT UNSIGNED NOT NULL,
  thread_pos INT UNSIGNED NOT NULL)";
$this->_db->exec($sql);
*/

?>