<?php
include "IGbookDB.class.php";
class GbookDB implements IGbookDB{
	const DB_NAME = "gbook.db";
	protected $_pdo;//private
	
	public function __construct(){
		if(is_file(self::DB_NAME)){//if (file_exists(self::DB_NAME)) {
		$this->_pdo = new PDO("sqlite:".self::DB_NAME);
		$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//_WARNING, _SILENT
		//$this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		}else{
		$this->_pdo = new PDO("sqlite:".self::DB_NAME);
		$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$sql = "CREATE TABLE msgs(
							id INTEGER PRIMARY KEY,
							name TEXT,
							email TEXT,
							msg TEXT,
							datetime INTEGER,
							ip TEXT)";
				$this->_pdo->exec($sql); //or die ('Помилка в запросі!');//($this->_pdo->errorInfo());
		}
	}
	public function __destruct(){
		//unset ($this->_pdo);
		$this->_pdo = NULL;
	}
	function clearData($data){
		$data = stripslashes($data);
		$data = strip_tags($data);
		$data = trim($data);
		return $this->_pdo->quote($data);
	}	
	public function savePost($name, $email, $msg){
		
			$dt = time();
			$ip = $_SERVER["REMOTE_ADDR"];
			$sql = "INSERT INTO msgs(
							name,
							email,
							msg,
							datetime,
							ip
						) VALUES(
							$name,
							$email,
							$msg,
							$dt,
							'$ip')";
			//echo $sql;	
			$result = $this->_pdo->exec($sql);
				if (!$result) 
				return false;
	return true;
	}
	public function getAll(){//SELECT * ... 
		try{
			$sql = "SELECT id, name, email, msg, ip, datetime
							FROM msgs ORDER BY id DESC";
			if($stmt = $this->_pdo->query($sql))
			//throw new Exception($this->_db->errorInfo()); було if(!$stmt
				$items = $this->db2Arr($stmt);
				unset($stmt);
				return $items;
		}catch(PDOException $e){
			return false;
		}
	}
	protected function db2Arr($data){//$items
	$arr = [];
	while ($row = $data->fetch(PDO::FETCH_ASSOC))
	$arr[] = $row;
  return $arr;
	}	
	public function deletePost($id){
		try{
			$sql = "DELETE FROM msgs WHERE id=$id";
			$result = $this->_pdo->exec($sql);
			if ($result)
			return true;
		}catch(PDOException $e){
			echo $e->getMessage();
			return false;
		}
	}
}
//$gbook = new GbookDB();
?>