<?php
require 'INewsDB.class.php';
class NewsDB implements INewsDB{
	protected $_db;
	const DB_NAME = 'E:\Serv\home\my.loc\www\site\3\news\news.db';
	const RSS_NAME = "rss.xml";
	const RSS_TITLE = "Стрічка новин";
	const RSS_LINK = "http://www.my.loc/site/3/news/news.php";
	function __construct(){
		if(is_file(self::DB_NAME)){
		$this->_db = new SQLite3(self::DB_NAME);
		}else{
		$this->_db = new SQLite3(self::DB_NAME);
		$sql = "CREATE TABLE msgs(
								id INTEGER PRIMARY KEY AUTOINCREMENT,
								title TEXT,
								category INTEGER,
								description TEXT,
								source TEXT,
								datetime INTEGER)";
		$this->_db->exec($sql) or die ($this->_db->LastErrorMsg());
		$sql = "CREATE TABLE category(
								id INTEGER,
								name TEXT)";
		$this->_db->exec($sql) or die ($this->_db->LastErrorMsg());
		$sql = "INSERT INTO category(id, name)
								SELECT 1 as id, 'Политика' as name
								UNION SELECT 2 as id, 'Культура' as name
								UNION SELECT 3 as id, 'Спорт' as name ";
		$this->_db->exec($sql) or die ($this->_db->LastErrorMsg());
		}
	}	
	function __destruct(){
		unset($this->_db);
	}
	function clearStr($data){
		$data = trim(strip_tags($data));
		return $this->_db->escapeString($data);
	}	
	function clearInt($data){
	return abs((int)$data);
	}
	protected function db2Arr($data){
		$arr = array();
		while($row = $data->fetchArray(SQLITE3_ASSOC))
		$arr[] = $row;
	return $arr;
	}
	function saveNews($t, $c, $d, $s){//saveNews($title, $category, $description, $source)
			try{
			$dt = time();
			$sql = "INSERT INTO msgs(
														title, 
														category, 
														description,
														source, datetime)
											VALUES('$t', $c, '$d','$s', $dt)";
			//echo $sql;								
			$res = $this->_db->exec($sql);// or die ($this->_db->LastErrorMsg());
			if(!$res)
			throw new Exception($this->_db->LastErrorMsg());
			$this->createRss();
			return true;
		}catch(Exception $e){
			return false;
		}	
	}
	function getNews(){
		try{
			$sql = "SELECT msgs.id as id, title, category.name as category, description, source, datetime
							FROM msgs, category
							WHERE category.id=msgs.category
							ORDER BY msgs.id DESC";
/*Объединение таблиц		
SELECT m.id, m.title, c.name, m.description, m.source, m.datetime
							FROM msgs m
							INNER JOIN category c ON c.id = m.category
							ORDER BY m.id DESC";
					
SELECT t.name, t.code, l.course
FROM teachers t
INNER JOIN lessons l ON t.id = l.tid
////////////
SELECT t.name, t.code, l.course
FROM teachers t
LEFT OUTER JOIN lessons l ON t.id = l.tid
//RIGHT OUTER JOIN lessons l ON t.id = l.tid
////////////
 SELECT DISTINCT teachers.name
FROM teachers INNER JOIN
(lessons INNER JOIN courses
ON lessons.course = courses.id) 
ON teachers.id = lessons.teacher
WHERE courses.title LIKE 'Web%'
ORDER BY teachers.name*/							
			$res = $this->_db->query($sql); //or die ($this->_db->LastErrorMsg());
			if(!is_object($res))
				throw new Exception($this->_db->LastErrorMsg());
			return $this->db2Arr($res);
		}catch(Exception $e){
		//$e->getMessage();
		return false;	
		}
	}
	function deleteNews($id){
		try{
			$sql = "DELETE FROM msgs WHERE id=$id";
			$res = $this->_db->exec($sql);//or die ($this->_db->LastErrorMsg());
			if(!$res)
				throw new Exception($this->_db->LastErrorMsg());
			return true;
		}catch(Exception $e){
		//$e->getMessage();
		return false;
		}
	}
	function createRss(){
		$dom = new DomDocument('1.0', 'utf8');
		$dom->formatOutput = true;
		$dom->preserveWhiteSpase = false;
		$rss = $dom->createElement('rss');
		$dom->appendChild($rss);
		$channel = $dom->createElement('channel');
		$rss->appendChild($channel);
		$t = $dom->createElement('title', self::RSS_NAME);
		$l = $dom->createElement('link', self::RSS_LINK);
		$channel->appendChild($t);
		$channel->appendChild($l);
		$lenta = $this->getNews();
		if(!$lenta) return false;
		foreach($lenta as $news){
			$item = $dom->createElement('item');
			$title = $dom->createElement('title', $news['title']);
			$category = $dom->createElement('category', $news['category']);
			$description = $dom->createElement('description', $news['description']);
			$txt = Self::RSS_LINK.'?id='.$news['id'];
			$link = $dom->createElement('link', $txt);
			$dt = date('r', $news['datetime']);
			$pubdate = $dom->createElement('pubDate', $dt);
			$item->appendChild($title);
			$item->appendChild($link);
			$item->appendChild($description);
			$item->appendChild($pubdate);
			$item->appendChild($category);
			$channel->appendChild($item);
		}
		$dom->save(self::RSS_NAME);
	}
}
//$news = new NewsDB;

?>