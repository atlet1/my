<?php
$users = $news->getNews();
if(!is_array($users)){
	$errMsg = 'Виникла помилка при виводі стрічки';
}else{
	echo '<p>Всього останніх новин: ' .count($users);
	foreach($users as $item){
		$id = $item['id'];
		$title = $item['title'];
		$cat = $item['category'];
		$desc = nl2br($item['description']);
		$dt = date('d-m-Y H:i:s', $item['datetime']);
		echo <<<LABEL
		<hr>
		<h3>$title</h3>
		<p>$desc<br>[$cat] @ $dt</p>
		<p align='right'>
			<a href='news.php?del=$id'>Видалити</a>
		</p>
LABEL;
}	
}
?>