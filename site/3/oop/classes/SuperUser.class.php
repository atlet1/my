<?php
class SuperUser extends User implements ISuperUser{//наслідування класу, реалізація інтерфейсу
	public $role;
	public static $cntSU = 0;// Счетчик 
	function __construct($name, $login, $password, $role){
			parent::__construct($name, $login, $password);
			$this->role = $role;
			++self::$cntSU;//self::$cntSU++;
			--parent::$cntU;// --self::$cntSU;
	}
	function showInfo(){
		parent::showInfo();
			echo '<br>Роль: '.$this->role;//"<br>Роль: ", $this->role;	
	}
	function getInfo(){
		$arr = array();
		foreach($this as $key=>$val)
		$arr[$key] = $val;
		return $arr;
		//return (array)$this;
	}
}
?>