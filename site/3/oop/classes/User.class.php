<?php
// Информация о пользователях
class User extends AUser{
	//public $name;
	//public $login;
	//public $password;
	public static $cntU = 0;// Счетчик
	protected $objNum = 0;//
	const INFO_TITLE = "<h3>Дані користувача: ";
	
	function __construct($name="", $login="", $password=""){//($n, $l, $p)
	try{
				if(empty($name) or empty($login) or empty($password)) throw new Exception("Введены не все данные!");
		$this->name = $name;
		$this->login = $login;
		$this->password = $password;
		//++self::$cntU;
		$this->objNum += ++self::$cntU;//
		}catch(Exception $e){
				echo "Произошла ошибка ", $e->getMessage(),
					" в строке ", $e->getLine(),
					" файла ", $e->getFile();
				exit;	
		}
	}
	
	function __destruct(){
		echo '<br>Користувач '.$this->name.' видалений';
	}	
	
	public function showInfo(){
	echo '<p>Name: '.$this->name;//echo "<p>Name: {$this->name}"; //{$this}->name - вже не працює
	echo '<p>Login: '.$this->login;//echo "<br>Логин: ", $this->login;
	echo '<p>Password: '.$this->password;
	}
	
	function showTitle() {
			echo self::INFO_TITLE;	
	}
	
	function __clone(){
		$this->name = 'Guest';
		$this->login = 'guest';
		$this->password = '0000';
		++self::$cntU;
		$this->objNum++;//
	}
	function __toString() {//
			return "Объект #".$this->objNum.": ".$this->name;	
	}
}
/*
class Foo{
	function __construct($user){
		$user->showInfo();
	}
}*/
?>