<?php
// Функция автоматической подгрузки классов
function loadClass ($class_name) {
require_once "classes/$class_name.class.php";
}

spl_autoload_register('loadClass');
//var_dump(spl_autoload_functions());

//spl_autoload_register(function ($class_name) {
 //   include classes/$class_name . '.php';
//});

// Пользователь 1
$user1 = new User('John Smith', 'john', '1234');
echo User::INFO_TITLE;
$user1->showInfo();

$user2 = new User('Mike Dow', 'mike', '45678');
$user2->showTitle();
$user2->showInfo();

$user3 = new User('Ivan Petrov', 'ivan', '9632');
$user3->showInfo();
/*
$user3 = new User;
$user3->name = 'Ivan Petrov';
$user3->login = 'ivan';
$user3->password = '9632';
$user3->showInfo();*/
$user4 = clone $user3;
$user4->showInfo();

// Суперпользователь
$user = new SuperUser('Vasa Pupkin', 'vasa', 'root', 'admin');
$user0 = new SuperUser("Igor Borisov", "igor", "sdfrtRc34", "admin");
//$user->role = 'admin';
$user->showInfo();
echo "<hr><p>";
$props = $user->getInfo();
foreach($props as $prop=>$value){
	echo "$prop = $value<br>";
}
$user0->showInfo();
//echo '<br>Role: '.$user->role;
echo '<hr>';
echo 'Всього простих юзерів: '.User::$cntU.'<hr>';
echo 'Всього адмінів: '.SuperUser::$cntSU.'<hr>';

// Функция проверки типа пользователя
	function checkUser($obj){
		if ($obj instanceof User) {
			if ($obj instanceof SuperUser) {
				echo "<p>", $obj->name,
				        " - cуперпользователь</p>";	
			} else {
				echo "<p>", $obj->name,
				        " - обычный пользователь</p>";
			}
		}else{
			echo '<p>$obj - неизвестный пользователь</p>';
		}
	}
	
	// Проверка типа пользователя	
	checkUser($user2);
	checkUser($user);
	
	// Количество обычных пользователей
	echo "<p>Количество обычных пользователей - ",
	        User::$cntU;
	// Количество суперпользователей		
	echo "<p>Количество суперпользователей - ",
	        SuperUser::$cntSU;
	echo "<hr>", $user1;
	echo "<hr>", $user2;
	echo "<hr>", $user3;
	echo "<hr>", $user4;
	echo "<hr>", $user;
	echo "<hr>", $user0;
?>