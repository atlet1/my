<?php
header("Content-Type: text/html;charset=utf-8");
	// Создание объекта DOM
	$dom = new DOMDocument();
	
	// Загрузка XML-документа в объект
	$dom->load("catalog.xml");
	//$dom->loadXML($string);
	//$dom->loadHTMLfile($fileName);
	//$dom->loadHTML($string);
	
	
	// Получение корневого элемента
	$root = $dom->documentElement;
?>
<html>
	<head>
		<title>Каталог</title>
	</head>
	<body>
	<h1>Каталог книг</h1>
	<table border="1" width="100%">
		<tr>
			<th>Автор</th>
			<th>Название</th>
			<th>Год публикации</th>
			<th>Цена, руб</th>
		</tr>
<?php
	foreach ($root->childNodes as $element) {
		// Проверка на тип элемента
		if ($element->nodeType == XML_ELEMENT_NODE) {//nodeType == 1) {
			echo "\t<tr>\n";
			foreach ($element->childNodes as $book) {
				if ($book->nodeType == XML_ELEMENT_NODE)	{
					echo "<td>", $book->textContent, "</td>";	
				}	
			}
			echo "\t</tr>\n";
		}
	}
?>		
	</table>
</body>
</html>