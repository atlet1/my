<?php
class StockService{
    // Описание функции Web-сервиса
	function getStock($id) {	
		$stock = array(
			"1" => 100,
			"2" => 200,
			"3" => 300,
			"4" => 400,
			"5" => 500
		);
		if (isset($stock[$id])) {
			$quantity = $stock[$id];		
			return $quantity;
		} else {
			throw new SoapFault("Server", "Несуществующий id товара");
		}	
	}
}
//getStock(2);exit;// проверка сервера на ошибки
    // Отключение кэширования WSDL-документа (во время разработки нужно)
	ini_set("soap.wsdl_cache_enabled", "0");
	// Создание SOAP-сервер
	$server = new SoapServer("http://my.loc/site/3/soap/stock.wsdl");
	// Добавить класс к серверу
	$server->setClass("StockService");//класс
	//$server->addFunction("getStock");//функция
	// Запуск сервера
	$server->handle();
?>