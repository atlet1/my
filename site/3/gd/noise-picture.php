<?php
session_start();
$i = imageCreateFromJPEG('images/noise.jpg');
$color = imageColorAllocate($i, 64, 64, 64);
imageAntiAlias($i, true);// Включаем сглаживание
$nChars = 5;// Число символов
$randStr = substr(md5(uniqid()), 0, $nChars);// Случайная строка
$_SESSION['randStr'] = $randStr;

$font = 'fonts/bellb.ttf';
$x = 20; $y = 30; $deltaX = 40;// Координаты
for($j = 0; $j < $nChars; $j++){//$j < count($nChars);
	$size = rand(18, 30);
	$angle = -30 + rand(0, 60);
	imageTtfText($i, $size, $angle, $x, $y, $color, $font, $randStr{$j});//imageFtText
	$x += $deltaX;
}
header('Content-Type: image/jpeg');
imageJPEG($i, NULL, 50);
?>
